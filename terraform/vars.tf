variable "backend_key_file" {
  default = "./backend_ssh.pub"
}

variable "redis_key_file" {
  default = "./redis_ssh.pub"
}

variable "postgres_key_file" {
  default = "./redis_ssh.pub"
}