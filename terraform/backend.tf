resource "aws_key_pair" "backend" {
  key_name   = "kegi-frontend-ssh-key"
  public_key = file(var.backend_key_file)
}

resource "aws_security_group" "backend" {
  name        = "kegi-backend-sg"
  description = "Allow"
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "backend1" {
  ami                    = "ami-04e601abe3e1a910f"
  instance_type          = "t2.micro"
  availability_zone      = "eu-central-1a"
  key_name               = aws_key_pair.backend.key_name
  vpc_security_group_ids = [aws_security_group.backend.id]
  tags = {
    Name = "Kegi Backend 1"
  }
}