resource "aws_key_pair" "redis" {
  key_name   = "kegi-redis-ssh-key"
  public_key = file(var.redis_key_file)
}

resource "aws_security_group" "redis" {
  name        = "kegi-redis-sg"
  description = "Allow"
  ingress {
    from_port   = 6379
    to_port     = 6379
    protocol    = "tcp"
    cidr_blocks = [format("%s/32", aws_instance.backend1.public_ip)]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "redis1" {
  ami                    = "ami-04e601abe3e1a910f"
  instance_type          = "t2.micro"
  availability_zone      = "eu-central-1a"
  key_name               = aws_key_pair.redis.key_name
  vpc_security_group_ids = [aws_security_group.redis.id]
  tags = {
    Name = "Kegi Redis 1"
  }
}