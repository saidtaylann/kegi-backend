import nodemailer from "nodemailer";
require("dotenv").config();

export const receiver = async (event) => {
  for (const record of event.Records) {
    var transporter = nodemailer.createTransport({
      pool: true,
      host: process.env.EMAIL_HOST,
      port: +process.env.EMAIL_PORT,
      auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASSWORD,
      },
    });
    const qBody = JSON.parse(record.body);
    const mailType = record.messageAttributes["type"].stringValue;
    let htmlContent;
    if (mailType === "verification") {
      htmlContent = `<html><body>
      Merhaba ${qBody.templateData.name} ${qBody.templateData.lastname}</br>
      Doğrulama kodunuz aşağıdadır: ${qBody.templateData.verificationToken}</br>
      Aşağıdaki linke tıklayarak da hesabınızı doğrulayabilirsiniz:</br>
      <a href="${qBody.templateData.verificationLink}">${qBody.templateData.verificationLink}</a>
      </body></html>`;
    } else if (mailType === "re-verification") {
      htmlContent = `<html><body>
      Merhaba ${qBody.templateData.name} ${qBody.templateData.lastname}</br>
      Yeni Doğrulama kodunuz aşağıdadır: ${qBody.templateData.verificationToken}</br>
      Aşağıdaki linke tıklayarak hala hesabınızı doğrulayabilirsiniz:</br>
      <a href="${qBody.templateData.verificationLink}">${qBody.templateData.verificationLink}</a>
      </body></html>`;
    }
    const mailOptions = {
      from: process.env.EMAIL_USER,
      to: qBody.to,
      subject: qBody.subject,
      html: htmlContent,
    };
    try {
      for (i of qBody.to) {
        await transporter.sendMail({ ...mailOptions, to: i });
        console.log(
          `${mailType} mail was sent to ${i}\nName: ${qBody.templateData.name} ${qBody.templateData.lastname}`
        );
      }
    } catch (err) {
      console.log("err", err);
    }
  }
};

/*
import nodemailer from "nodemailer";

export const receiver = async (event) => {
  console.log("FONKSİYON BAŞLADI");
  for (const record of event.Records) {
    var transporter = nodemailer.createTransport({
      //pool: true,
      host: process.env.EMAIL_HOSTNAME,
      port: +process.env.EMAIL_PORT,
      secure: process.env.EMAIL_SECURE === "true" ? true : false,
      auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASSWORD,
      },
    });
    var transporter = nodemailer.createTransport({
      //pool: true,
      host: "smtp.gmail.com",
      port: 587,
      auth: {
        user: "saidimtaylan@gmail.com",
        pass: "otmcntxgnhfekvxx",
      },
    });
    const mailType = record.messageAttributes["type"].stringValue;
    let htmlContent;
    if (mailType === "verification") {
      console.log("TYPE IS VERIFICATION");
      htmlContent = await ejs.renderFile("./templates/verification.ejs", {
        appName,
        name: qBody.templateData.name,
        lastname: qBody.templateData.lastname,
        verificationLink: qBody.verificationLink,
      });
    }
    console.log("HTMLCONTENT", htmlContent);
    const qBody = JSON.parse(record.body);
    const mailOptions = {
      from: process.env.EMAIL_USER,
      subject: qBody.subject,
      html: htmlContent,
    };
    try {
      for (i of qBody.to) {
        console.log("MAIL ATMA DONGUSUNE GIRDI");
        await transporter.sendMail({ ...mailOptions, to: i });
        console.log(
          `verification mail was sent to ${i}\nName: ${qBody.templateData.name} ${qBody.templateData.lastname}`
        );
      }
    } catch (err) {
      console.log("err", err);
    }
  }
};


*/
