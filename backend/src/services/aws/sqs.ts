import { SQS, config } from "aws-sdk"
import { ISQSAttr } from "src/types/services/aws/sqs";
import { IMailDataToSend, IVerificationMailData } from "src/types/services/aws/sqs";

type mailTypes = "verification" | "re-verification" | "forgottenPassword"

const sendToQ = async (qUrl: string, messageBody: any, messageAttrs?: any) => {

    config.update({ region: 'REGION' });
    const sqs = new SQS({ apiVersion: '2012-11-05' });
    const params = {
        ...(messageAttrs && { MessageAttributes: messageAttrs }),
        //MessageDeduplicationId: "TheWhistler",  // Required for FIFO queues
        //MessageGroupId: "Group1",  // Required for FIFO queues
        MessageBody: JSON.stringify(messageBody),
        QueueUrl: qUrl
    };
    try {
        await sqs.sendMessage(params).promise()
    } catch (err) {
        console.log('err', err)
        return err
    }
}

export const sendEmail = async (type: mailTypes, data: IMailDataToSend) => {
    sendToQ(process.env.AWS_SQS_MAILQ_URL, data,
        {
            type: <ISQSAttr>{ DataType: "String", StringValue: type }
        })
}