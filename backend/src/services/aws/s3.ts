import { S3, config } from 'aws-sdk';
import { Error as Err } from 'src/types/error';

export const uploadS3File = async (file: Express.Multer.File, fileName: string, folder: string) => {
    config.update({
        region: 'eu-central-1'
    })
    const s3 = new S3({
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_ACCESS_SECRET_KEY
    });
    const params = {
        Bucket: process.env.AWS_S3_BUCKET_NAME,
        Key: folder + fileName,
        Body: file.buffer
    };
    try {
        return s3.upload(params).promise()

    } catch (err) {
        return new Err({ message: "an error occured while uploading the file", cause: err })
    }
};

export const updateS3File = async (file: Express.Multer.File, fileName: string, folder: string) => {
    config.update({
        region: 'eu-central-1'
    })
    const s3 = new S3({
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_ACCESS_SECRET_KEY
    });
    // Setting up S3 upload parameters
    const params = {
        Bucket: process.env.AWS_S3_BUCKET_NAME,
        Key: folder + fileName,
        Body: file.buffer
    };

    try {
        return s3.putObject(params).promise()
    } catch (err) {
        return new Err({ message: "an error occured while uploading the file", cause: err })
    }
};

export const deleteS3File = async (fileName: string, folder: string) => {
    config.update({
        region: 'eu-central-1'
    })
    const s3 = new S3({
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_ACCESS_SECRET_KEY
    });
    // Setting up S3 upload parameters
    const params = {
        Bucket: process.env.AWS_S3_BUCKET_NAME,
        Key: folder + fileName,
    };
    try {
        return s3.deleteObject(params).promise()
    } catch (err) {
        return new Err({ message: "an error occured while uploading the file", cause: err })
    }
};

// kullanılmayacak
export const getFile = async (fileName: string, folder: string) => {
    config.update({
        region: 'eu-central-1'
    })
    const s3 = new S3({
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_ACCESS_SECRET_KEY
    });
    // Setting up S3 upload parameters
    const params = {
        Bucket: process.env.AWS_S3_BUCKET_NAME,
        Key: folder + fileName,
    };

    try {
        return s3.getObject(params).promise()
    } catch (err) {
        return new Err({ message: "an error occured while uploading the file", cause: err })
    }
};