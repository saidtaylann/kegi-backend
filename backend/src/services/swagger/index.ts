import { INestApplication } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

export const swaggerInit = async (app: INestApplication) => {
    const config = new DocumentBuilder()
        .setTitle('Kegi API Documentation ')
        .setDescription('The Kegi API description')
        .setVersion('1.0')
        .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('api', app, document);
}