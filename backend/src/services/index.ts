//import { initRabbitMQ } from "./rabbitmq"
import Redis from "./redis"
import Database from "src/database"

export const initServices = async () => {
    try {
        // sqs is being used for queue, so rabbitmq was commented for now
        // await initRabbitMQ()
        new Redis()
        new Database()

    } catch (err) {
        console.log(err);
    }
}