import redis from "ioredis"
import { OnlineUser } from "./users";
import { RedisRoomSession } from "./session";

class Redis {
    static conn: redis = new redis(process.env.REDIS_ADDR, {
        username: "default", // needs Redis >= 6
        password: "my-top-secret",
        db: 0
    });

    static users: OnlineUser = new OnlineUser(this.conn);

    static sessions: RedisRoomSession = new RedisRoomSession(this.conn)

    static getUserModule(): OnlineUser {
        return this.users
    }

    static getSessionModule(): RedisRoomSession {
        return this.sessions
    }

    static multi() {
        return this.conn.multi()
    }
}
export default Redis