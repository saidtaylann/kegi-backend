import type Redis from "ioredis"
import { Error as Err } from "src/types/error"

export class OnlineUser {
    private readonly groupName: string = "user"
    constructor(private readonly redis: Redis) {
        this.redis = redis
    }
    async set(websocket: string, uid: "" | number) {
        try {
            return await this.redis.set(`${this.groupName}:${websocket}`, uid)
        } catch (err) {
            return new Err({ message: 'an error occured while setting online user ', cause: err })
        }
    }
    async get(websocket: string) {
        try {
            return this.redis.get(`${this.groupName}:${websocket}`)
        } catch (err) {
            return new Err({ message: 'an error occured while getting online user ', cause: err })
        }
    }

    async del(websocket: string) {
        try {
            return this.redis.del(`${this.groupName}:${websocket}`,)

        } catch (err) {
            return new Err({ message: 'an error occured while deleting online user ', cause: err })
        }
    }
}