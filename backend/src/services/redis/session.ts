import Redis from "ioredis"
import type { JoinSessionDto, StartSessionDto } from "src/ws/dto/room.dto"

export class RedisRoomSession {

    constructor(private readonly conn: Redis) { }

    async setParticipant(body: JoinSessionDto) {
        return this.conn.hset(`session:${body.sessionId}:participantStreamID:${body.websocketId}`, body.participant)
    }

    async deleteParticipant(sessionId: number, participantWebsocketId: string) {
        return this.conn.del(`session:${sessionId}:participantStreamID:${participantWebsocketId}`)
    }

    async getParticipant(sessionId: number, participantWebsocketId: string) {
        return this.conn.hgetall(`session:${sessionId}:participantStreamID:${participantWebsocketId}`)
    }

    async updateParticipant(sessionId: number, participantWebsocketId: string, field: string, value: any) {
        return this.conn.hmset(`session:${sessionId}:participantStreamID:${participantWebsocketId}`, [field, value])
    }

    async deleteParticipantField(sessionId: number, participantWebsocketId: string, field: string) {
        return this.conn.hdel(`session:${sessionId}:participantStreamID:${participantWebsocketId}`, field)
    }

    async setSession(sessionId: number, session: any) {
        await this.conn.hset(`session:${sessionId}`, session)
    }

    async deleteSession(sessionId: number) {
        await this.conn.del(`session:${sessionId}`)
    }

    async getSession(sessionId: number) {
        return this.conn.hgetall(`session:${sessionId}`)
    }

    async updateSession(sessionId: number, field: string, value: any) {
        return this.conn.hmset(`session:${sessionId}`, [field, value])
    }
}
