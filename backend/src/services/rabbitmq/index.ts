import amqp from "amqplib"
import { mailConfig } from "./mailer";

export const initRabbitMQ = async () => {
    const connection: amqp.Connection = await amqp.connect(process.env.RABBITMQ_ADDR)
    await mailConfig(connection)
}