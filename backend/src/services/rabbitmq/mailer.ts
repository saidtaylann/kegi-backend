import amqp from "amqplib"
import { IMailDataToSend } from "src/types/services/rabbitmq";

const QInfo = <{ channel: amqp.Channel, queues: string[], qPrefix: string, exchange: string, exchangeType: string }>{
    channel: null,
    queues: ["verification"],
    qPrefix: "mail",
    exchange: "mailer",
    exchangeType: "direct"
}
export const mailConfig = async (connection: amqp.Connection) => {
    QInfo.channel = await connection.createChannel()

    QInfo.channel.assertExchange(QInfo.exchange, QInfo.exchangeType, {
        durable: true,
        autoDelete: false,
        internal: false,
    })

    QInfo.queues.forEach(async (q: string) => {
        await QInfo.channel.assertQueue(`${QInfo.qPrefix}.${q}`, {
            durable: true,
            autoDelete: false,
            exclusive: false,
        })
        await QInfo.channel.bindQueue(`${QInfo.qPrefix}.${q}`, QInfo.exchange, q)
    })
}

export const sendMail = async (mailType: string, data: IMailDataToSend) => {
    const pubOk = QInfo.channel.publish(QInfo.exchange, mailType, Buffer.from(JSON.stringify(data)), {
        persistent: true,
        contentType: "text/plain",
    });
    if (!pubOk) {
        throw "verification mail could not send"
    }
}
