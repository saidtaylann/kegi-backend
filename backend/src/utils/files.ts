import fs from "fs"

export const deleteFile = (path: string): boolean => {
    try {
        fs.rm(path, () => {
        })
        return true
    } catch (err) {
        throw err
    }
}

export const createIncomingFile = (file: any, dir: string, fileName: string) => {
    return file.mv(
        dir + fileName,
        async () => { }
    );
}