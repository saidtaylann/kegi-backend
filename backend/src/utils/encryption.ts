import bc from "bcrypt"
import config from "src/config.json"
import { Error as Err } from "src/types/error"

export const hashPassword = async (password: string) => {
    try {
        const round = config.ENCRYPTION.BCRYPT_SALT_ROUND
        return bc.hash(password, round)
    }
    catch (err) {
        return new Err({ message: "an error ocured while hashing password", cause: err })
    }
}

export const comparePasswords = (password: string, hash: string) => {
    try {
        return bc.compare(password, hash)
    }
    catch (err) {
        return new Err({ message: "an error occured while comparing password", cause: err })
    }
}