import cluster, { Worker } from "cluster"
import { cpus } from "os"

const cCPUs = cpus().length;

const clusterify = (appFunc: Function, startPort: number) => {
    if (cluster.isPrimary) {
        // Create a worker for each CPU
        for (let i = 0; i < cCPUs; i++) {
            cluster.fork();
        }
        cluster.on('online', function (worker: Worker) {
        });
        cluster.on('exit', function (worker: Worker, code, signal) {
        });
    } else {
        appFunc(startPort + cluster.worker.id - 1)
    }
}

export default clusterify