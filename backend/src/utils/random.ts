import { v4 as uuidv4 } from 'uuid';

export const genRandomString = (size: number) => {
        return uuidv4()
}

export const genRandomDashedString = (length: number) => {
        let str: string = "";
        const chars: string = "abcdefghijklmoprstuvyzwxq"
        const charLen: number = chars.length
        for (let i = 1; i <= length; i++) {
                if (i === 5 || i === 10) str += "-"
                else str += chars.charAt(Math.floor(Math.random() * charLen));
        }
        return str
}

export const genRandomShortCode = (length: number) => {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        const charactersLength = characters.length;
        let counter = 0;
        while (counter < length) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
                counter += 1;
        }
        return result;
}