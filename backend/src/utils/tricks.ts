export const erhand = (promise, improved) => {
    return promise
      .then((data: any) => [data, null])
      .catch((err: any) => {
        if (improved) {
          Object.assign(err, improved);
        }
  
        return [err, null]; // which is same as [err, undefined];
      });
  }