import { CronJob } from "cron"

export const cronJob = (job: Function, minute: string) => {

    return new CronJob(`*/${minute} * * * *`, () => job, null, true, "Europe/Istanbul")
}