import { Response } from "express"
import config from "src/config.json"

// time is allowed as minute
export const createHttpOnlyStrictCookie = async (res: Response, key: string, value: any, maxAge?: number, expires?: Date) => {
    res.cookie(key, value, { httpOnly: true, sameSite: "strict", domain: '', secure: true, ...(maxAge && { maxAge: maxAge * 1000 * 60 }), ...(expires && { expires: expires }) })
}