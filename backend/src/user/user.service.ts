import { Injectable, forwardRef, Inject, HttpException, HttpStatus } from "@nestjs/common";
import { hashPassword } from "src/utils/encryption";
import { genRandomString } from "src/utils/random";
import { CreateUserDto, GetUsersDto, UpdateUsersDto } from "./dto/users.dto";
import { UserModel } from "./user.model"
import path from "path";
import { AuthService } from "src/auth/auth.service";
import { OnlineUser } from "src/services/redis/users";
import Redis from "src/services/redis";
import type { Knex } from "knex";
import { IUser, IUserDevice, IUserLogin } from "src/types/user";
import { IMailDataToSend, IVerificationMailData } from 'src/types/services/aws/sqs';
import config from "src/config.json"
import { updateS3File, uploadS3File, deleteS3File } from "src/services/aws/s3";
import { sendEmail } from "src/services/aws/sqs";
import { Error as Err } from "src/types/error";
import Database from "src/database";
import type { Socket } from "socket.io"
import { RoomService } from "src/room/room.service";

@Injectable()
export class UserService {
  private defaultAvatar = "default.png";
  private avatarDir = `${config.DIRS.images}/avatar/`;
  private readonly onlineUsers: OnlineUser;
  private readonly knex = Database.connection

  constructor(
    private readonly UserModel: UserModel,
    @Inject(forwardRef(() => AuthService)) private readonly authService: AuthService,
    @Inject(forwardRef(() => RoomService)) private readonly roomService: RoomService,
  ) {
    this.onlineUsers = Redis.getUserModule();
  }
  async getUsers(trx: Knex.Transaction, cond: GetUsersDto): Promise<IUser[] | Err> {
    const users: IUser[] | Err = await this.UserModel.selectUsersBy(trx, cond)
    if (users instanceof Err) {
      users.code = 500
      return users
    }
    return users
  }

  async getUserById(trx: Knex.Transaction, id: number): Promise<IUser | Err> {
    const user: IUser | Err = await this.UserModel.selectUserBy(trx, { id })
    if (user instanceof Err) {
      user.code = 500
      return user
    }
    return user
  }

  async getUserByEmail(trx: Knex.Transaction, email: string): Promise<IUser | Err> {
    const user: IUser | Err = await this.UserModel.selectUserBy(trx, { email })
    if (user instanceof Err) {
      user.code = 500
      return user
    }
    return user
  }

  async getUserByVerificationToken(trx: Knex.Transaction, token: string) {
    const user: IUser | Err = await this.UserModel.selectUserByVerificationtoken(trx, token)
    if (user instanceof Err) {
      user.code = 500
      return user
    }
    return user

  }

  async createUser(trx: Knex.Transaction, body: CreateUserDto, platform: string, browser: string)
    : Promise<{
      DID: string,
      LID: any,
      accessToken: string, UID: number
    } | Err> {
    let isUserExist: IUser | Err = await this.UserModel.selectUserBy(trx, { email: body.email })
    if (isUserExist instanceof Err) {
      isUserExist.cause = isUserExist.message
      isUserExist.message = "an error occured while querying whether user is exist"
      isUserExist.code = 500
      return isUserExist
    }
    else if (isUserExist) {
      return new Err({ message: "exist such a user with this email", code: 409 })
    }
    let { password } = body;
    const hashedPassword = await hashPassword(password)
    if (hashedPassword instanceof Error) {
      return new Err({ message: "an error occured while hashing password in phase creating user", code: 409 })
    }
    const newUser: IUser | Err = await this.UserModel.create(trx, {
      ...body,
      password: hashedPassword as string,
    }, this.defaultAvatar)
    if (newUser instanceof Err) {
      newUser.code = 500
      return newUser
    }
    else if (newUser) {
      const verificationToken = await this.authService.addEmailVerificationCode(trx, newUser.id)
      if (verificationToken instanceof Err) {
        verificationToken.code = 500
        return verificationToken
      }
      if (verificationToken) {
        const tmplData: IVerificationMailData = { name: body.name, lastname: body.lastname, verificationToken, verificationLink: `${config.SERVER.allowedDomain}/verify/${verificationToken}` }
        const data: IMailDataToSend = { to: [body.email], subject: "Kegi Mail Doğrulama! Son Bir Adım Kaldı.", templateData: tmplData }
        sendEmail("verification", data)
      }
      const loginData = await this.authService.login(trx, { email: body.email, password: body.password }, body.websocket, undefined, browser, platform)
      if (loginData) {
        return { ...loginData, UID: newUser.id }
      }
    }
    return new Err({ message: 'could not user created', code: 500 })
  }

  async updateUser(trx: Knex.Transaction, id: number, body: UpdateUsersDto): Promise<IUser | Err> {
    const updatedUser: IUser | Err = await this.UserModel.update(trx, id, body)
    if (updatedUser instanceof Err) {
      updatedUser.code = 500
      return updatedUser
    }
    return updatedUser
  }

  async deleteUser(trx: Knex.Transaction, id: number): Promise<IUser | Err> {
    const deletedUser: IUser | Err = await this.UserModel.delete(trx, id)
    if (deletedUser instanceof Err) {
      deletedUser.code = 500
      return deletedUser
    }
    return deletedUser
  }

  async updateAvatar(
    trx: Knex.Transaction,
    uid: number,
    avatar: Express.Multer.File,
  ): Promise<{ success: boolean } | Err> {
    const user: IUser | Err = await this.getUserById(trx, uid)
    if (user instanceof Err) {
      user.cause = user.message
      user.message = "an error occured while updating avatar"
      user.code = 500
      return user
    }
    const avatarName = this.setAvatarName(uid, avatar.originalname)
    if (user.avatar === this.defaultAvatar) {
      // createIncomingFile(avatar, this.avatarDir, this.setAvatarName(uid, avatar.originalname))
      const isUploaded = await uploadS3File(avatar, avatarName, this.avatarDir)
      if (isUploaded instanceof Err) {
        isUploaded.code = 500
        return isUploaded
      }
      const isUpdated = await this.UserModel.update(trx, uid, { avatar: avatarName })
      if (isUpdated instanceof Err) {
        isUpdated.cause = isUpdated.message
        isUpdated.message = "could not update avatar"
        isUpdated.code = 500
        return isUpdated
      }
    } else {
      const isUpdated = await updateS3File(avatar, avatarName, this.avatarDir)
      if (isUpdated instanceof Err) {
        isUpdated.cause = isUpdated.message
        isUpdated.message = "could not update avatar"
        isUpdated.code = 500
        return isUpdated
      }
    }
    return { success: true }
  }

  async deleteAvatar(trx: Knex.Transaction, uid: number): Promise<string | Err> {
    const user = await this.getUserById(trx, uid)
    if (user instanceof Err) {
      user.cause = user.message
      user.message = "could not delete user avatar"
      user.code = 500
      return user
    }
    const isUpdated: IUser | Err = await this.UserModel.update(trx, uid, { avatar: this.defaultAvatar })
    if (isUpdated instanceof Err) {
      isUpdated.cause = isUpdated.message
      isUpdated.message = "could not update user avatar"
      isUpdated.code = 500
      return isUpdated
    }
    const isDeleted = await deleteS3File(user.avatar, this.avatarDir)
    if (isDeleted instanceof Err) {
      isDeleted.cause = isDeleted.message
      isDeleted.message = "could not delete user avatar"
      isDeleted.code = 500
      return isDeleted
    }
    else if (isDeleted) return isUpdated.avatar
    return ""
  }

  setAvatarName(uid: number, avatarname: any): string {
    return `${uid}${path.extname(avatarname)}`;
  }

  async startSession(socket: Socket, registeredUser?: { id: number, LID: string }) {
    this.knex.transaction(async trx => {
      if (registeredUser) {
        const user: IUser | Err = await this.UserModel.selectUserBy(trx, { id: registeredUser.id })
        if (user instanceof Err) {
          user.cause = user.message

          user.message = "an error occured while getting user info"
          return user
        }
        else if (user) {
          const login: IUserLogin | Err = await this.UserModel.selectUserLoginBy(trx, { id: registeredUser.LID })
          if (login instanceof Err) {
            login.cause = login.message
            login.message = "an error occured while getting login info of the user"
            login.code = 500
            return login
          }
          if (!login) {
            return new Err({ message: 'invalid LID', code: 404 })
          }
          this.onlineUsers.set(socket.id, user.id)
          const rooms: any[] | Err = await this.roomService.getRoomsUserMembered(trx, registeredUser.id)
          if (rooms instanceof Err) {
            rooms.code = 500
            return rooms
          }
          if (rooms.length > 0) {
            rooms.forEach((r) => {
              socket.join(r.code)
            })
          }
          socket.emit("userSessionStarted", registeredUser.id)
          this.onlineUsers.set(socket.id, user.id)
        }
      }
      else {
        socket.emit("userSessionStarted", -1)
      }
    })
  }
}