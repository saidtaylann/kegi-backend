import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { AuthModule } from 'src/auth/auth.module';
import { UserController } from './user.controller';
import { UserModel } from './user.model';
import { UserService } from './user.service';
import { forwardRef } from '@nestjs/common/utils';
import { RoomModule } from 'src/room/room.module';

@Module({
  controllers: [UserController],
  providers: [UserService, UserModel],
  imports: [forwardRef(() => AuthModule), forwardRef(() => RoomModule)],
  exports: [UserService, UserModel]
})
export class UserModule {
}