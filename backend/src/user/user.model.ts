import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/users.dto';
import type { Knex } from "knex"
import { IUserLogin, IUser, IUserDevice } from 'src/types/user';
import { Error as Err } from "src/types/error"
import Database from 'src/database';
import moment from "moment"

@Injectable()
export class UserModel {
    private knex = Database.connection
    private userReturningFields: string[] = ['u.id', 'u.name', 'u.lastname', 'u.avatar', 'u.username', 'u.email', 'u.role', 'u.verified', 'u.type']
    private userLoginsReturningFields: string[] = ['ul.id', 'ul.refresh_token', 'ul.user_id', 'ul.updated_at', 'ul.created_at']
    private userDeviceReturningFields: string[] = ['ud.id', 'ud.platform', 'ud.user_id']
    constructor() {
    }

    async selectUsersBy(trx: Knex.Transaction, data: {}): Promise<IUser[] | Err> {
        try {
            const selectedUsers: IUser[] = await this.knex("users as u").where(data).whereNull("deleted_at").select(this.userReturningFields).transacting(trx)
            return selectedUsers
        }
        catch (err) {
            return new Err({ message: "could not get users", cause: err })
        }
    }

    async selectUserBy(trx: Knex.Transaction, data: {}) {
        try {
            const selectedUser: IUser = await this.knex("users as u").where(data).whereNull("deleted_at").first().select(this.userReturningFields).transacting(trx)
            return selectedUser
        }
        catch (err) {
            return new Err({ message: "could not get user", cause: err })
        }
    }

    async selectUserWithPassword(trx: Knex.Transaction, email: string) {
        try {
            const user: IUser = await this.knex("users as u").where('email', email).whereNull('deleted_at').first().select([...this.userReturningFields, 'u.password']).transacting(trx)
            return user
        } catch (err) {
            return new Err({ message: "could not get user", cause: err })
        }
    }

    async selectUserByVerificationtoken(trx: Knex.Transaction, token: string): Promise<IUser | Err> {
        try {
            return this.knex("email_verification_token as vt").where({ token }).first().transacting(trx)
                .innerJoin("users as u", "vt.token", '=', "u.id")
                .select(this.userReturningFields).first()
        } catch (err) {
            return new Err({ message: 'an error occured while getting user', cause: err })
        }
    }


    async create(trx: Knex.Transaction, dataToInsert: CreateUserDto, avatar: string) {
        try {
            const { websocket, ...user } = dataToInsert
            //const newUser: IUser = await this.knex("users as u").insert({ ...dataToInsert, avatar }).transacting(trx).returning(this.userReturningFields)
            const newUser = await this.knex("users").insert({ ...user, avatar }).transacting(trx).returning("id")
            return newUser ? newUser[0] : undefined
        }
        catch (err) {
            return new Err({ message: "could not create user", cause: err })
        }
    }
    async update(trx: Knex.Transaction, id: number, dataToUpdate: {}): Promise<IUser | Err> {
        try {
            const updatedUser = await this.knex("users as u").where({ "id": id }).whereNull('deleted_at').update(dataToUpdate).transacting(trx).returning(this.userReturningFields)
            return updatedUser ? updatedUser[0] : undefined
        }
        catch (err) {
            return new Err({ message: "could not update user", cause: err })
        }
    }
    async delete(trx: Knex.Transaction, id: number) {
        try {
            const deletedUser = await this.knex("users as u").where("id", id).whereNull('deleted_at').update({}).returning(this.userReturningFields).transacting(trx)
            return deletedUser ? deletedUser[0] : undefined
        }
        catch (err) {
            return new Err({ message: "could not delete user", cause: err })
        }
    }

    async createUserLogin(trx: Knex.Transaction, data: Omit<IUserLogin, "updated_at">) {
        try {
            const newLogin: IUserLogin[] = await this.knex("user_logins as ul").insert(data).transacting(trx).returning(this.userLoginsReturningFields)
            return newLogin ? newLogin[0] : undefined
        } catch (err) {
            return new Err({ message: "could not create user login", cause: err })
        }
    }

    async deleteUserLogin(trx: Knex.Transaction, lid: string) {
        try {
            const login = await this.knex("user_logins as ul").where({ 'id': lid }).delete().transacting(trx).returning("*")
            return login ? login[0] : undefined
        } catch (err) {
            return new Err({ message: "could not delete user login", cause: err })
        }
    }

    // by uid or lid or did
    async markUserLoginAsLogedOutBy(trx: Knex.Transaction, by: {}) {
        try {
            const login = await this.knex("user_logins as ul").where(by).update({ 'deleted_at': moment().format() }).transacting(trx).returning(this.userLoginsReturningFields)
            return login
        } catch (err) {
            return new Err({ message: "could not logout", cause: err })
        }
    }

    // by uid or did
    async deleteUserLoginsBy(trx: Knex.Transaction, by: {}) {
        try {
            const login = await this.knex("user_logins as ul").where(by).delete().transacting(trx).returning("*")
            return login
        }
        catch (err: any) {
            return new Err({ message: "could not delete user logins", cause: err })
        }
    }

    async updateUserLoginBy(trx: Knex.Transaction, lid: string, by: {}) {
        try {
            const login = await this.knex("user_logins as ul").where({ 'id': lid }).update(by).transacting(trx).returning("*")
            return login ? login[0] : undefined
        } catch (err) {
            return new Err({ message: "could not update user login", cause: err })
        }
    }

    // bu uid or lid or did
    async selectUserLoginBy(trx: Knex.Transaction, by: {}) {
        try {
            const login: IUserLogin = await this.knex("user_logins as ul").where(by).whereNull('ul.deleted_at').select(this.userLoginsReturningFields).first().transacting(trx)
            return login
        }
        catch (err: any) {
            return new Err({ message: "could not get user login", cause: err })
        }
    }

    async selectUsersLoginsBy(trx: Knex.Transaction, by: {}) {
        try {
            const logins: IUserLogin[] = await this.knex("user_logins as ul").where(by).whereNull('deleted_at').select(this.userLoginsReturningFields).transacting(trx)
            return logins
        }
        catch (err: any) {
            return new Err({ message: "could not get user logins", cause: err })
        }
    }

    async createUserDevice(trx: Knex.Transaction, data: IUserDevice) {
        try {
            const newDevice = await this.knex("user_devices as ud").insert(data).transacting(trx).returning("*")
            return newDevice ? newDevice[0] : undefined
        } catch (err) {
            return new Err({ message: "could not get users", cause: err })
        }
    }

    async deleteUserDevice(trx: Knex.Transaction, did: string) {
        try {
            const deletedDevice = await this.knex("user_devices as ud").where({ 'id': did }).del().transacting(trx).returning("*")
            return deletedDevice ? deletedDevice[0] : undefined
        } catch (err) {
            return new Err({ message: "could not delete user device", cause: err })
        }
    }

    async updateUserDevice(trx: Knex.Transaction, did: string, updateData?: { platform?: string }) {
        try {
            const updatedDevice = await this.knex("user_devices as ud").where({ 'id': did, }).update({ ...updateData }).transacting(trx).returning("*")
            return updatedDevice ? updatedDevice[0] : undefined
        } catch (err) {
            return new Err({ message: "could not update user device", cause: err })
        }
    }

    // by platform or browser or user id or id
    async selectUserDevicesBy(trx: Knex.Transaction, by: {}) {
        try {
            const userDevices: IUserDevice[] = await this.knex("user_devices as ud").where({}).select(this.userDeviceReturningFields).transacting(trx)
            return userDevices
        }
        catch (err) {
            return new Err({ message: "could not get user devices", cause: err })
        }
    }

    // by platform or browser or user id or id
    async selectUserDeviceBy(trx: Knex.Transaction, by: {}) {
        try {
            const userDevices: IUserDevice = await this.knex("user_devices as ud").where({}).first().select(this.userDeviceReturningFields).transacting(trx)
            return userDevices
        }
        catch (err) {
            return new Err({ message: "could not get user device", cause: err })
        }
    }
}
