import { Body, Controller, Get, Post, Put, Delete, Res, ParseIntPipe, ParseFilePipe, FileTypeValidator } from '@nestjs/common';
import { Param, Req, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common/decorators';
import { HttpException } from '@nestjs/common/exceptions';
import { CreateUserDto, GetUsersDto, UpdateUsersDto } from './dto/users.dto';
import { UserService } from './user.service';
import Database from 'src/database';
import { IResponseCreatedUser } from 'src/types/responses';
import { ExtReq } from 'src/types/common';
import { Response } from "express"
import { Roles } from 'src/auth/decorators/roles.decorator';
import { ERole } from 'src/enums/roles.enum';
import { FileInterceptor } from '@nestjs/platform-express';
import { Error as Err } from 'src/types/error';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { OnlineUser } from 'src/services/redis/users';
import Redis from 'src/services/redis';

@Controller('users')
export class UserController {
    private readonly knex = Database.connection
    private onlineUsers: OnlineUser
    constructor(private readonly UserService: UserService) {
        this.onlineUsers = Redis.getUserModule();
    }
    @Post()
    async handleCreateUser(@Body() body: CreateUserDto, @Req() req: ExtReq, @Res({ passthrough: true }) res: Response) {
        return this.knex.transaction(async trx => {
            const resp: { UID: number, LID: string, DID: string, accessToken: string } | Err = await
                this.UserService.createUser(trx, body, req.useragent.platform, req.useragent.browser)
            if (resp instanceof Err) {
                throw new HttpException(resp.message, resp.code)
            } else {
                // login phase
                //createHttpOnlyStrictCookie(res, "DID", resp.DID, 60 * 24 * 180)
                //createHttpOnlyStrictCookie(res, "LID", resp.LID, 60 * 24 * 60)
                //createHttpOnlyStrictCookie(res, "A_T", resp.accessToken, +process.env.JWT_EXPR_DURATION)
                return <IResponseCreatedUser>{ UID: resp.UID, accessToken: resp.accessToken, LID: resp.LID, DID: resp.DID }
            }
        })
    }

    @Put()
    async handleUpdateUser(
        @Body() body: UpdateUsersDto, @Req() req: ExtReq) {
        return this.knex.transaction(async trx => {
            const updatedUser = await this.UserService.updateUser(trx, req.user.id, body)
            if (updatedUser instanceof Err) {
                throw new HttpException(updatedUser.message, updatedUser.code)
            }
            return updatedUser
        })
    }

    @Post("avatar")
    @UseInterceptors(FileInterceptor('avatar'))
    async handleUpdateAvatar(@Req() req: ExtReq,
        @UploadedFile(
            new ParseFilePipe({
                validators: [
                    //new MaxFileSizeValidator({ maxSize: 100000 }),
                    new FileTypeValidator({ fileType: 'image/jpeg' || "image/png" }),
                ],
            }),
        )
        avatar: Express.Multer.File) {
        return this.knex.transaction(async trx => {
            const updated = await this.UserService.updateAvatar(trx, req.user.id, avatar)
            if (updated instanceof Err) {
                throw new HttpException(updated.message, updated.code)
            }
            return { avatar: updated }
        })
    }

    @Delete("avatar")
    async handleDeleteAvatar(@Req() req: ExtReq) {
        try {
            return this.knex.transaction(async trx => {
                await this.UserService.deleteAvatar(trx, req.user.id)
                return true
            })
        } catch (err) {
            throw new HttpException(err, 500)
        }
    }

    /*     @Post("filter")
        async handleGetUsers(@Body() body: GetUsersDto) {
            return this.knex.transaction(async trx => {
                const users = await this.UserService.getUsers(trx, body)
                if (users instanceof Err) {
                    throw new HttpException(users.message, users.code)
                }
                return users
            })
        } */

    @Get(":id")
    async handleGetUserById(@Req() req: ExtReq, @Param('id', new ParseIntPipe()) id: number) {
        if (req.user.id === id) {
            return this.knex.transaction(async trx => {
                const user = await this.UserService.getUserById(trx, id)
                if (user instanceof Err) {
                    throw new HttpException(user.message, user.code)
                }
            })
        }
        throw new HttpException("you can just only get your info", 401)

    }

    @Get()
    @UseGuards(RolesGuard)
    @Roles(ERole.USER, ERole.ADMIN, ERole.SUPERADMIN)
    async handleGetUser(@Req() req: ExtReq) {
        return this.knex.transaction(async trx => {
            const user = await this.UserService.getUserById(trx, req.user.id)
            if (user instanceof Err) {
                throw new HttpException(user.message, user.code)
            }
            return user
        })
    }

    @Delete(":id")
    async handleDeleteUser(@Req() req: ExtReq, @Param('id', new ParseIntPipe()) id: number) {
        return this.knex.transaction(async trx => {
            const user = await this.UserService.deleteUser(trx, req.user.id)
            if (user instanceof Err) {
                throw new HttpException(user.message, user.code)
            }
            return user
        })
    }
}
