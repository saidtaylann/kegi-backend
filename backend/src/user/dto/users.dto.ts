import { IsBoolean, IsEmail, IsInt, IsNotEmpty, IsNumberString, IsOptional, IsString, MaxLength, MinLength } from 'class-validator';

export class CreateUserDto {
    @IsNotEmpty()
    @IsString()
    @MinLength(2)
    @MaxLength(120)
    name: string;

    @IsNotEmpty()
    @IsString()
    @MinLength(2)
    lastname: string;

    @IsNotEmpty()
    @IsEmail()
    @IsEmail()
    email: string;

    @IsNotEmpty()
    @IsString()
    @MinLength(8)
    @MaxLength(24)
    password: string;

    @IsNotEmpty()
    @IsString()
    @MinLength(4)
    @MaxLength(32)
    username: string

    @IsNotEmpty()
    @IsString()
    type: string

    @IsNotEmpty()
    @IsInt()
    role: number

    @IsNotEmpty()
    @IsString()
    websocket: string
}

export class GetUsersDto {

    @IsOptional()
    @IsString()
    @MinLength(2)
    @MaxLength(100)
    name?: string

    @IsOptional()
    @IsString()
    @MinLength(2)
    @MaxLength(50)
    lastName?: string

    @IsOptional()
    @IsString()
    @MinLength(4)
    @MaxLength(32)
    username?: string

    @IsOptional()
    @IsBoolean()
    verified?: boolean

    @IsOptional()
    @IsString()
    type?: string

    @IsOptional()
    @IsInt()
    role?: number
}

export class UpdateUsersDto {
    @IsOptional()
    @IsString()
    @MinLength(2)
    @MaxLength(100)
    name?: string

    @IsOptional()
    @IsString()
    @MinLength(2)
    @MaxLength(50)
    lastname?: string

    @IsNotEmpty()
    @IsEmail()
    @IsEmail()
    email?: string;

    @IsOptional()
    @IsString()
    @MinLength(8)
    @MaxLength(24)
    password?: string

    @IsOptional()
    @IsString()
    @MinLength(4)
    @MaxLength(32)
    username?: string

    @IsOptional()
    @IsBoolean()
    type?: string

    @IsOptional()
    @IsString()
    role?: number
}

export class LoginDto {
    @IsNotEmpty()
    @IsEmail()
    @IsEmail()
    email: string;

    @IsNotEmpty()
    @IsString()
    @MinLength(8)
    @MaxLength(24)
    password: string;
}