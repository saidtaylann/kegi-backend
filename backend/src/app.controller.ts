import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { IMailDataToSend, IVerificationMailData } from 'src/types/services/rabbitmq';
import { sendMail } from './services/rabbitmq/mailer';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
