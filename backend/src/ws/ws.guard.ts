import { ExecutionContext, Injectable } from '@nestjs/common';
import jwt from "jsonwebtoken"
import { WsService } from './ws.service';

@Injectable()
export class WsJwtAuthGuard {
    constructor(private readonly wsService: WsService) { }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const socket = context.switchToWs()
        const body = socket.getData()
        const client = socket.getClient()
        const token = body.accessToken
        if (token) {
            const payload = jwt.verify(token, process.env.JWT_SECRET);
            if (!payload) {
                return false;
            }
            client.request.user = payload;
            return true;
        }
        return false;
    }
}


@Injectable()
export class WsOptionalJWTAuthGuard {
    constructor(private readonly wsService: WsService) { }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const socket = context.switchToWs()
        const body = socket.getData()
        const client = socket.getClient()
        const token = body.accessToken
        if (!token) return true


        const payload = jwt.verify(token, process.env.JWT_SECRET);

        if (!payload) {
            return false;
        }
        client.request.user = payload;
        return true;
    }
}
