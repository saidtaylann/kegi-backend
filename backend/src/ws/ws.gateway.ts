import { ConnectedSocket, MessageBody, SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';
import { WsService } from './ws.service';
import config from "../config.json"
import { Body, UseGuards } from '@nestjs/common';
import { WsJwtAuthGuard, WsOptionalJWTAuthGuard } from './ws.guard';
import { ExtSocket } from 'src/types/common';
import { RoomService } from 'src/room/room.service';
import { JoinSessionDto, RequestToJoinSessionDto, StartSessionDto, } from './dto/room.dto';
import { UserService } from 'src/user/user.service';
import { Error as Err } from 'src/types/error';

@WebSocketGateway({
  cors: {
    allowedHeaders: ['content-type', 'lid', 'accessToken', 'withcredentials'],
    origin: config.SERVER.allowedDomain,
    credentials: true,
    methods: ['GET', 'OPTIONS', 'PUT', 'POST', 'DELETE'],
  },
  /*   cookie: {
      httpOnyl: true,
      sameSite: "strict"
    } */
})

export class WsGateway {

  @WebSocketServer()
  server: Server;

  constructor(private readonly wsService: WsService, private readonly roomService: RoomService, private readonly userService: UserService) { }

  @UseGuards(WsOptionalJWTAuthGuard)
  @SubscribeMessage('startUserSession')
  async listenToStartSession(@Body() body, @ConnectedSocket() socket: ExtSocket) {
    const LID = body.LID
    if (socket.request.user && LID) {
      await this.userService.startSession(socket, { id: socket.request.user.id, LID })
    } else {
      await this.userService.startSession(socket)
    }
  }


  @UseGuards(WsJwtAuthGuard)
  @SubscribeMessage('openRoom')
  async listenToOpenRoom(@MessageBody() body: { roomCode: string }, @ConnectedSocket() socket: ExtSocket) {
    const err: Err = await this.roomService.openRoom(socket, body.roomCode, socket.request.user.id)
    if (err) {
      socket.emit("room_error", { message: err.message, ...(err.code && { code: err.code }) })
    }
  }

  @UseGuards(WsJwtAuthGuard)
  @SubscribeMessage('createRoomSession')
  async listenToCreateSession(@MessageBody() body: { roomCode: string, sessionPreferences: StartSessionDto }, @ConnectedSocket() socket: ExtSocket) {

    const err: any = await this.roomService.createSession(socket, body.roomCode, socket.request.user.id, body.sessionPreferences)
    if (err instanceof Err) {
      socket.emit("room_error", { message: err.message, ...(err.code && { code: err.code }) })
    }
  }


  @UseGuards(WsJwtAuthGuard)
  @SubscribeMessage('closeRoom')
  listenCloseRoomOrClass(@MessageBody() body: { userSessionId: string, roomCode: string }, @ConnectedSocket() socket: ExtSocket) {
  }

  @UseGuards(WsOptionalJWTAuthGuard)
  @SubscribeMessage('requestToJoinSession')
  async listenToRequestToJoinSession(@MessageBody() body: RequestToJoinSessionDto, @ConnectedSocket() socket: ExtSocket) {
    let err: Err;
    if (socket.request.user?.id) {
      err = await this.roomService.requestToJoinSession(socket, body.roomCode, body.passcode, socket.request.user.id)
    } else {
      err = await this.roomService.requestToJoinSession(socket, body.roomCode, body.passcode, undefined, body.displayName)
    }
    if (err) {
      socket.emit("room_error", { message: err.message, ...(err.code && { code: err.code }) })
    }
  }

  @UseGuards(WsOptionalJWTAuthGuard)
  @SubscribeMessage('joinSession')
  async listenToJoinSession(@MessageBody() body: JoinSessionDto, @ConnectedSocket() socket: ExtSocket) {
    let err;
    if (socket.request.user?.id) {
      err = await this.roomService.joinSession(socket, body)
    } else {
      err = await this.roomService.joinSession(socket, body)
    }
    if (err) {
      socket.emit("room_error", { message: err.message, ...(err.code && { code: err.code }) })
    }
  }

  @UseGuards(WsOptionalJWTAuthGuard)
  @SubscribeMessage('leaveSession')
  async listenToLeaveSession(@MessageBody() body, @ConnectedSocket() socket: ExtSocket) {
    let err;
    if (socket.request.user?.id) {
      err = await this.roomService.leaveSession(this.server, socket, body, socket.request.user.id)
    } else {
      err = await this.roomService.leaveSession(this.server, socket, body)
    }
    if (err) {
      socket.emit("room_error", { message: err.message, ...(err.code && { code: err.code }) })
    }
  }

  @UseGuards(WsOptionalJWTAuthGuard)
  @SubscribeMessage('mute')
  listenToMuteParticipant(@MessageBody() body) {
    this.roomService.mute(this.server, body)
  }

  @UseGuards(WsOptionalJWTAuthGuard)
  @SubscribeMessage('unmute')
  listenToUnmuteParticipant(@MessageBody() body) {
    this.roomService.unmute(this.server, body)
  }

  @UseGuards(WsOptionalJWTAuthGuard)
  @SubscribeMessage('turnOffCam')
  listenToCamTurnOffParticipant(@MessageBody() body) {
    this.roomService.closeCam(this.server, body)
  }

  @UseGuards(WsOptionalJWTAuthGuard)
  @SubscribeMessage('turnOnCam')
  async listenToCamTurOnParticipant(@MessageBody() body) {
    await this.roomService.openCam(this.server, body)
  }

  @UseGuards(WsOptionalJWTAuthGuard)
  @SubscribeMessage('shareScreen')
  async listenToShareScreen(@MessageBody() body) {
    await this.roomService.shareScreen(this.server, body)
  }

  @SubscribeMessage("stopSharingScreen")
  async listenToStopSharingScreen(@MessageBody() body) {
    await this.roomService.stopSharingScreen(this.server, body)
  }

  async listenToEndSession(@MessageBody() body) {
    await this.roomService.endSession(this.server, body.roomSessionId)
  }

  @SubscribeMessage("switchMicAuthorization")
  async listenToSwitchMicAuthorization(@MessageBody() body) {
    await this.roomService.switchMicAuthorization(this.server, body)
  }

  @SubscribeMessage("switchCamAuthorization")
  async listenToSwitchCamAuthorization(@MessageBody() body) {
    await this.roomService.switchCamAuthorization(this.server, body)
  }

  @SubscribeMessage("switchSharingScreenAuthorization")
  async listenToSwitchSharingScreenAuthorization(@MessageBody() body) {
    await this.roomService.switchSharingScreenAuthorization(this.server, body)
  }

  @SubscribeMessage("changeParticipantRole")
  async listenToChangeSessionRole(@MessageBody() body) {
    await this.roomService.changeParticipantRole(this.server, body)
  }
}
