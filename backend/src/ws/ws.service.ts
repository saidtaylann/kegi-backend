import { Injectable } from '@nestjs/common';
import { RoomModel } from 'src/room/room.model';
import Redis from "src/services/redis";
import Database from 'src/database';
import { Socket } from 'socket.io';
import type { IRoomSessionDetail } from 'src/types/room';
import { Error as Err } from 'src/types/error';

@Injectable()
export class WsService {
    private readonly knex = Database.connection
    private onlineUsers = Redis.getUserModule()
    constructor(private readonly roomModel: RoomModel) { }

    /*     async openRoomOrClass(socket: Socket, body: { userSessionId: string, roomCode: string }) {
            this.knex.transaction(async trx => {
                const userId = await this.onlineUsers.get(body.userSessionId, "id")
                const roomSessionId = await this.roomModel.createRoomSession(trx, body.roomCode)
                if (roomSessionId instanceof Err) {
                    return roomSessionId
                }
                if (roomSessionId) {
                    const updated = await this.roomModel.updateRoom(trx, body.roomCode, { is_open: true })
                    if (updated instanceof Err) {
                        return updated
                    }
                    const sessionDetail: IRoomSessionDetail[] | Err = await this.roomModel.getRoomSessionDetail(trx, roomSessionId)
                    if (sessionDetail instanceof Err) {
                        return sessionDetail
                    }
                    socket.join(roomSessionId)
                    socket.in(body.roomCode).emit("openedRoomOrClass", sessionDetail)
                }
            })
    
        } */

    getCookie(client: any, name: string) {
        if ("cookie" in client.handshake.headers)
            return client.handshake.headers.cookie.split('; ').find((c) => c.startsWith(name))?.split('=')[1]
    }
}
