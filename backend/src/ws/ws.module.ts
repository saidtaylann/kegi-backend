import { Module } from '@nestjs/common';
import { WsGateway } from './ws.gateway';
import { UserModule } from 'src/user/user.module';
import { RoomModule } from 'src/room/room.module';
import { WsService } from './ws.service';

@Module({
    providers: [WsGateway, WsService],
    imports: [RoomModule, UserModule]
})
export class WsModule { }
