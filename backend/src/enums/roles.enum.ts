export enum ERole {
    SUPERADMIN = 2,
    ADMIN = 1,
    USER = 0
}

export enum ERoomRole {
    OWNER = 3,
    SUPERHOST = 2,
    HOST = 1,
    PARTICIPANT = 0
}