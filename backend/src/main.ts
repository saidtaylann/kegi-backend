import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { swaggerInit } from './services/swagger';
import { initServices } from './services';
import cookieParser from 'cookie-parser';
import config from "src/config.json"
import userAgent from "express-useragent"
import clusterify from './utils/cluster';

async function bootstrap(APP_PORT: string | number) {
  /*   const httpsOptions = {
      key: fs.readFileSync('./ssl/key.pem'),
      cert: fs.readFileSync('./ssl/cert.pem')
    }; */
  console.log('APP_PORT', APP_PORT)
  const app = await NestFactory.create(AppModule);

  swaggerInit(app)
  app.enableCors({
    origin: config.SERVER.allowedDomain,
    methods: ['GET', 'OPTIONS', 'PUT', 'POST', 'DELETE'],
    credentials: true,
    allowedHeaders: ['content-type', 'lid', 'Authorization', 'authorization', 'withcredentials']
  })
  //app.setGlobalPrefix("api")
  app.use(cookieParser());
  app.use(userAgent.express());
  //  app.use(csurf())

  await initServices()
  await app.listen(APP_PORT);
}
bootstrap(3000)
//clusterify(bootstrap, 3000)