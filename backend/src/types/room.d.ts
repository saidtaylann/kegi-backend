import { StringParameter } from "aws-sdk/clients/quicksight"
import { IUser } from "./user"

export interface IRoom {
    title: string
    image: string
    passcode: string
    code: string
    is_edu: boolean
    is_temp: boolean
    is_open: boolean
    created_at: Date
    updated_at: Date

    owner_id: number
    owner_username: string
    owner_name: string
    owner_lastname: string
}

export interface IRoomDetail {
    room_title: string
    passcode: string
    code: string
    image: string
    is_edu: boolean
    is_temp: boolean
    room_created_at: Date
    is_open: boolean

    owner_id: number
    owner_username: string
    owner_name: string
    owner_lastname: string

    member_id: number
    member_name: string
    member_lastname: string
    member_username: string
    member_email: string
    member_avatar: string,
    member_role: number
    member_joined_at: Date

}

export interface IParticipant {
    id: number
    user_id: number
    room_session_id: string
    display_name: string
    joined_at: Date
    left_at: Date
    role: number
    is_member: boolean
    role: boolean
    websocket_id: string
}

export interface IParticipantMove {
    participant_id: number
    loged_in_at: Date
    loged_out_at: Date
}

export interface IRoomSession {
    room_code: string
    id: number
    started_at: Date
    ended_at: Date
}

export interface IRoomSessionDetail {
    title: string
    passcode: string
    code: string
    image: string
    is_edu: boolean
    is_temp: boolean
    room_created_at: Date
    is_open: boolean

    owner_id: number
    owner_username: string
    owner_name: string
    owner_lastname: string

    participant_name: string
    participant_lastname: string
    participant_email: string
    participant_avatar: string
    participant_username: string
    participant_id: number

    participant_user_id: number
    participant_avatar: string
    participant_display_name: string
    participant_joined_at: Date
    participant_left_at: Date
    participant_role: number
    participant_is_member: boolean
    participant_websocket_id: string
    participant_stream_id: string

    room_session_id: number
    started_at: Date
    ended_at: Date
}

export interface IMember {
    room_code: string
    id: number
    user_id: number
    joined_at: Date
    left_at: Date
    role: number
}

export interface SessionDetailOfUser {
    isMicOpen: boolean
    isCamOpen: boolean
    streamId: string
}