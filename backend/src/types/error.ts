export class Error {
    name?: string
    message: string
    cause?: any
    code?: number
    constructor(params: { message: string, cause?: any, code?: number, name?: string }) {
        this.name = params.name
        this.message = params.message
        this.cause = params.cause
        this.code = params.code

    }
}