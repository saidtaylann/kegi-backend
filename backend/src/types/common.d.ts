import { Request } from "express"
import { IJWT } from "./auth"
import type { Socket } from "socket.io"

type ExtReq = Request & {
    user: IJWT,
    useragent: any,
    files: Express.Multer.File
}

type ExtSocket = Socket & {
    request: Request & {
        user: IJWT
    }
}