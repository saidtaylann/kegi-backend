export interface IJWT {
    id: number
    role: number
}

export interface IRole {
    role: number
    role_desc: string
}

export interface IEmailVerificationToken {
    user_id: number
    token: string
    created_at: Date
}