export interface IUser {
	id: number
	name: string
	lastname: string
	username: string
	password?: string
	email: string
	avatar: string
	type: string
	role: number
	verified: boolean
	created_at?: Date;
	updated_at?: Date;
	deleted_at?: Date;
}

export interface IUserDevice {
	user_id: number
	platform: string
	id: string
	browser: string
}

export interface IUserLogin {
	id: string
	user_id: number
	device_id: string
	refresh_token: string
	updated_at: Date
}

export interface IOnlineUser {
	id?: number
	role?: number
	websocket: string
}

export interface IUserRoomRole {
	id: number
	created_at: Date;
	updated_at: Date;
	deleted_at: Date
}