export interface IResponseLoginedUser {
    status: boolean
}

export interface IResponseCreatedUser {
    UID: number,
    accessToken: string
    DID: string
    LID: string
}

export interface IResponseError {
    error: string
    code: code
}