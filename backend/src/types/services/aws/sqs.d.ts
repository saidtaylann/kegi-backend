interface ISQSAttr {
    DataType: string
    StringValue: number | string | Buffer
}

export interface IVerificationMailData {
    name: string
    lastname: string
    verificationToken: string
    verificationLink: string
}

export interface IMailDataToSend {
    to: string[]
    subject: string
    templateData: IVerificationMailData
}