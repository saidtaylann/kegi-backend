export interface IVerificationMailData {
    Name: string
    Lastname:string
    VerificationLink: string
}
export interface IMailDataToSend {
    to: string[]
    subject: string
    templateData: IVerificationMailData }