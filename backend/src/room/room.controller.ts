import { Body, Controller, Delete, FileTypeValidator, Get, HttpException, MaxFileSizeValidator, Param, ParseFilePipe, Post, Query, Req, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { ExtReq } from 'src/types/common';
import { RoomService } from './room.service';
import Database from 'src/database';
import { CreateRoomDto } from './dto/room.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { Error as Err } from 'src/types/error';
import { RoomModel } from './room.model';
import { ERoomRole } from 'src/enums/roles.enum';

@Controller('rooms')
export class RoomController {
    private readonly knex = Database.connection
    constructor(private readonly roomService: RoomService, private readonly roomModel: RoomModel) { }

    @Get("")
    async handleGetRoomsYouMembered(@Req() req: ExtReq) {
        return this.knex.transaction(async trx => {
            const rooms = await this.roomService.getRoomsUserMembered(trx, req.user.id)
            if (rooms instanceof Err) {
                throw new HttpException(rooms.message, rooms.code)
            }
            return rooms
        })
    }

    @Get("is-member/:code")
    async handleIsMemberOfRoom(@Param('code') code: string, @Req() req: ExtReq) {
        return this.knex.transaction(async trx => {
            const room = await this.roomModel.getRoomMember(trx, code, req.user.id)
            if (room instanceof Err) {
                room.cause = room.message
                throw new HttpException(room.message, 500)
            }
            return room ? { status: true } : { status: false }
        })
    }

    @Get(":code")
    async handleGetRoom(@Req() req: ExtReq, @Param('code') code: string) {
        return this.knex.transaction(async trx => {
            const room = await this.roomService.getRoomByCode(trx, code)
            if (room instanceof Err) {
                throw new HttpException(room.message, room.code)
            }
            return room
        })
    }

    @Post()
    async handleCreateRoom(@Req() req: ExtReq, @Body() body: CreateRoomDto) {
        return this.knex.transaction(async trx => {
            const newRoom = await this.roomService.createRoom(trx, { ...body, owner: req.user?.id })
            if (newRoom instanceof Err) {
                throw new HttpException(newRoom.message, newRoom.code)
            }
            return { room_code: newRoom }
        })
    }

    @Delete(":code")
    async handleDleteRoom(@Req() req: ExtReq, @Param('code') code: string) {
        return this.knex.transaction(async trx => {
            const hasPermission = await this.roomService.hasPermission(trx, code, req.user.id, [ERoomRole.OWNER], true)
            if (hasPermission instanceof Err) {
                throw new HttpException(hasPermission.message, hasPermission.code)
            }
            const deleted = this.roomService.deleteRoom(trx, code)
            if (deleted instanceof Err) {
                throw new HttpException(deleted.message, deleted.code)
            }
            return { success: true }
        })
    }

    @Post(":code/image")
    @UseInterceptors(FileInterceptor('image'))
    async handleUpdateRoomImage(@Req() req: ExtReq, @Query('status') status: "new" | "update", @Param('code') room_code: string,
        @UploadedFile(
            new ParseFilePipe({
                validators: [
                    // new MaxFileSizeValidator({ maxSize: 100000 }),
                    new FileTypeValidator({ fileType: 'image/jpeg' || "image/png" }),
                ],
            }),
        ) image: Express.Multer.File) {
        try {
            return this.knex.transaction(async trx => {
                const hasPermission = await this.roomService.hasPermission(trx, room_code, req.user.id, [ERoomRole.OWNER], true)
                if (hasPermission instanceof Err) {
                    throw new HttpException(hasPermission.message, hasPermission.code)
                }
                const updatedAvatar = await this.roomService.updateImage(trx, room_code, image)
                if (updatedAvatar instanceof Err) {
                    throw new HttpException(updatedAvatar.message, updatedAvatar.code)
                }
                if (updatedAvatar) return { success: true }
                return { success: false }
            })
        }
        catch (err) {
            throw new HttpException(err, 500)
        }
    }
}
