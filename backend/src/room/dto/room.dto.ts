import { IsBoolean, IsDate, IsIn, IsInt, IsNotEmpty, IsNumberString, IsOptional, IsString, Length, Max, MaxLength, MinLength } from 'class-validator';

export class CreateRoomDto {

    // owner bilgisi token'den alınacak
    // image bilgisi zaten buradan gelmiyor
    @IsOptional()
    @IsNumberString()
    @Length(8, 8)
    passcode?: string

    @IsNotEmpty()
    @IsBoolean()
    is_edu: boolean

    @IsOptional()
    @IsString()
    @Length(8, 120)
    title?: string

    @IsNotEmpty()
    @IsBoolean()
    is_temp: boolean
}

export class UpdateRoomDto {
    @IsOptional()
    @IsString()
    @Length(8, 8)
    passcode?: string

    @IsOptional()
    @IsBoolean()
    isEdu?: boolean

    @IsOptional()
    @IsString()
    @Length(8, 120)
    title?: string

    @IsOptional()
    @IsBoolean()
    is_temp: boolean
}

export class AddParticipantDto {
    @IsOptional()
    @IsInt()
    user_id?: number

    @IsOptional()
    @IsString()
    @Length(3, 32)
    display_name?: string

    @IsOptional()
    @IsInt()
    room_session_id: number

    @IsNotEmpty()
    @IsInt()
    @Max(3)
    role: number

    @IsNotEmpty()
    @IsBoolean()
    is_member?: boolean

    @IsOptional()
    @IsString()
    @Length(128, 512)
    websocket_id?: string
}

export class UpdateParticipantDto {
    @IsOptional()
    @IsInt()
    @Max(3)
    role: number

    @IsOptional()
    @IsBoolean()
    is_member?: boolean
}

export class CreateMemberDto {
    @IsNotEmpty()
    @IsInt()
    user_id: number


    @IsNotEmpty()
    @IsInt()
    role: number

    @IsNotEmpty()
    @IsString()
    @Length(14, 14)
    room_code: string
}

export class UpdateMemberDto {
    @IsOptional()
    @IsInt()
    role?: number

}