import { Module, forwardRef } from '@nestjs/common';
import { RoomService } from './room.service';
import { RoomController } from './room.controller';
import { RoomModel } from './room.model';
import { AuthModule } from 'src/auth/auth.module';
import { UserModule } from 'src/user/user.module';

@Module({
  providers: [RoomService, RoomModel],
  controllers: [RoomController],
  imports: [AuthModule, forwardRef(() => UserModule)],
  exports: [RoomModel, RoomService]
})
export class RoomModule { }
