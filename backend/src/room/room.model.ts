import { Injectable } from '@nestjs/common';
import './dto/room.dto';
import type { Knex } from "knex"
//import { IRoom} from 'src/interfaces/room';
import Database from 'src/database';
import moment from "moment"
import { IRoom, IParticipant, IRoomSessionDetail, IMember } from 'src/types/room';
import { CreateRoomDto, AddParticipantDto, UpdateRoomDto, UpdateParticipantDto, CreateMemberDto } from './dto/room.dto';
import { genRandomDashedString } from 'src/utils/random';
import { Error as Err } from 'src/types/error';

@Injectable()
export class RoomModel {
    private knex = Database.connection
    constructor() {
    }

    async getRoomsBy(trx: Knex.Transaction, by: {}) {
        try {
            const rooms = await this.knex("rooms as r").where(by).whereNull("deleted_at")
                .join("users as u", "r.owner", "=", "u.id")
                .select("u.name as owner_name", "u.lastname as owner_lastname", "u.username as owner_username", "u.id as owner_id", "r.is_open",
                    "r.image", "r.passcode", "r.created_at", "r.updated_at", "r.is_edu", "r.title", "r.is_temp", "r.code")
                .transacting(trx)
            return rooms
        }
        catch (err) {
            return new Err({ message: 'could not get the rooms', cause: err })
        }
    }

    async getRoomBy(trx: Knex.Transaction, by: {}) {
        try {
            const room = await this.knex("rooms as r").where(by).whereNull("r.deleted_at")
                .join("users as u", "r.owner", "=", "u.id").first()
                .select("u.name as owner_name", "u.lastname as owner_lastname", "u.id as owner_id", "u.username as owner_username", "r.is_open",
                    "r.image", "r.passcode", "r.created_at", "r.updated_at", "r.is_edu", "r.title", "r.is_temp", "r.code")
                .transacting(trx)
            return room
        }
        catch (err) {
            return new Err({ message: 'could not get the room', cause: err })
        }
    }

    async getRoomOnly(trx: Knex.Transaction, room_code: string) {
        try {
            const room = await this.knex("rooms").where({ code: room_code }).whereNull("deleted_at").select('*').transacting(trx).first()
            return room
        }
        catch (err) {
            return new Err({ message: 'could not get the room', cause: err })
        }
    }

    async getRoomDetail(trx: Knex.Transaction, by: {}) {
        try {
            const members = await this.knex("rooms as r").where(by).whereNull("deleted_at")
                .join("members as m", "r.code", "=", "m.room_code")
                .join("users as u", "m.user_id", "=", "u.id")
                .join("users as owner", "r.owner", "=", "owner.id")
                .select(["u.name as member_name", "u.lastname as member_lastname", "u.username as member_username",
                    "u.email as member_email", "u.avatar as member_avatar",
                    "owner.username as owner_username", "owner.name as owner_name", "owner.lastname as owner_lastname", "owner.id as owner_id",
                    "r.passcode", "r.is_edu", "r.image", "r.created_at as room_created_at", "r.title", "r.is_temp",
                    "r.code", "r.is_open",
                    "m.role as member_role", "m.joined_at as member_joined_at", "m.id as member_id"
                ])
                .transacting(trx)
            return members
        }
        catch (err) {
            return new Err({ message: 'could not get the room detail', cause: err })
        }
    }

    async getParticipantById(trx: Knex.Transaction, participant_id: number) {
        try {
            const participant = await this.knex("participants as p").whereNull("deleted_at").where({ id: participant_id })
                .innerJoin("users as u", "u.id", "=", "p.user_id")
                .select(["u.username", "u.name", "u.lastname", "u.email", "u.avatar", "u.id",
                    "p.websocket_id", "p.display_name", "p.role", "p.is_member"])
                .transacting(trx)
                .first()
            return participant
        } catch (err) {
            return new Err({ message: 'could not get participant', cause: err })
        }
    }

    async getParticipant(trx: Knex.Transaction, room_session_id: number) {
        try {
            const participant = await this.knex("participants as p").whereNull("p.deleted_at")
                .innerJoin("users as u", "u.id", "=", "p.user_id")
                .select(["u.username", "u.name", "u.lastname", "u.email", "u.avatar", "u.id as user_id",
                    "p.websocket_id", "p.display_name", "p.role", "p.is_member", "p.room_session_id", "p.id"])
                .where({ 'p.room_session_id': room_session_id })
                .first()
                .transacting(trx)
            return participant
        } catch (err) {
            return new Err({ message: 'could not get participant', cause: err })
        }
    }

    async getParticipantsByOrder(trx: Knex.Transaction, room_session_id: number, orderBy: string, order: 'asc' | 'desc', count: number) {
        try {
            const participants = await this.knex("participants as p").whereNull("p.deleted_at")
                .where({ 'p.room_session_id': room_session_id })
                .select(["p.websocket_id", "p.display_name", "p.role", "p.is_member", "p.room_session_id", "p.id"])
                .transacting(trx)
                .orderBy(orderBy, order)
            return participants
        } catch (err) {
            return new Err({ message: 'could not get participants', cause: err })
        }
    }

    async getParticipants(trx: Knex.Transaction, by: {}) {
        try {
            const participants = await this.knex("participants as p").whereNull("p.deleted_at")
                .innerJoin("users as u", "u.id", "=", "p.user_id")
                .select(["u.username", "u.name", "u.lastname", "u.email", "u.avatar", "u.id as user_id",
                    "p.websocket_id", "p.display_name", "p.role", "p.is_member", "p.id", "p.room_session_id"])
                .where(by)
                .transacting(trx)
            return participants
        } catch (err) {
            return new Err({ message: 'could not get participants', cause: err })
        }
    }

    async getParticipantsByRange(trx: Knex.Transaction, by: {}, field: string, range: Array<any>) {
        try {
            const participants = await this.knex("participants as p").whereNull("p.deleted_at")
                .innerJoin("users as u", "u.id", "=", "p.user_id")
                .select(["u.username", "u.name", "u.lastname", "u.email", "u.avatar", "u.id as user_id",
                    "p.websocket_id", "p.display_name", "p.role", "p.is_member", "p.id", "p.room_session_id"])
                .where(by)
                .whereIn(field, range)
                .transacting(trx)
            return participants
        } catch (err) {
            return new Err({ message: 'could not get participant', cause: err })
        }
    }

    // returns row count affected
    // if an error is not exist and rowCount is 0 then room code conflicted
    async createRoom(trx: Knex.Transaction, body: CreateRoomDto): Promise<string | Err> {
        try {
            const code = genRandomDashedString(14)
            const newRoom: any = await this.knex("rooms").insert({ ...body, code, is_open: body.is_temp ? true : false }).transacting(trx).onConflict("code").ignore()
            if (newRoom.rowCount === 0) await this.createRoom(trx, body)
            else return code
        }
        catch (err) {
            return new Err({ message: 'could not create room', cause: err })
        }
    }

    async createParticipant(trx: Knex.Transaction, data: AddParticipantDto) {
        try {
            const newMember = await this.knex("participants as p").insert(data).transacting(trx).returning("p.id")
            return newMember ? newMember[0] : undefined
        }
        catch (err) {
            return new Err({ message: 'could not create the room participant', cause: err })
        }
    }

    async updateRoom(trx: Knex.Transaction, code: string, dataToUpdate: {}) {
        try {
            const updatedRoom = await this.knex("rooms").where({ code }).update({ ...dataToUpdate }).transacting(trx).returning("code").transacting(trx)
            return updatedRoom ? updatedRoom[0] : undefined
        }
        catch (err) {
            return new Err({ message: 'could not update the room', cause: err })
        }
    }

    async updateParticipant(trx: Knex.Transaction, participant_id: number, dataToUpdate: UpdateParticipantDto) {
        try {
            const updatedUser = await this.knex("participants as p").where({ "p.id": participant_id }).update(dataToUpdate).transacting(trx).returning(["p.role", "p.id", "p.websocket_id"])
            return updatedUser ? updatedUser[0] : undefined
        }
        catch (err) {
            return new Err({ message: 'could not update the room participant', cause: err })
        }
    }

    async deleteRoom(trx: Knex.Transaction, code: string) {
        try {
            const deletedUser = await this.knex("rooms as r").where("code", code).update({ 'deleted_at': moment().format() }).returning(["r.code", "r.deleted_at"]).transacting(trx)
            return deletedUser ? deletedUser[0] : undefined
        }
        catch (err) {
            return new Err({ message: 'could not delete the room', cause: err })
        }
    }

    async deleteParticipant(trx: Knex.Transaction, participant_id: number) {
        try {
            const deletedUser = await this.knex("participants").where({ "id": participant_id }).update({ 'deleted_at': moment().format() }).returning(["id", "deleted_at"]).transacting(trx)
            return deletedUser ? deletedUser[0] : undefined
        }
        catch (err) {
            return new Err({ message: 'could not delete the room participant', cause: err })
        }
    }

    async createRoomSession(trx: Knex.Transaction, room_code: string) {
        try {
            const session = await this.knex("room_sessions").insert({ room_code, started_at: moment().format() }).transacting(trx).returning("id")
            return session ? session[0].id : undefined
        } catch (err: any) {
            return new Err({ message: 'could not create the room session', cause: err })
        }
    }

    async updateRoomSession(trx: Knex.Transaction, room_session_id: number, dataToUpdate: {}) {
        try {
            const session = await this.knex("room_sessions").where({ "id": room_session_id }).update(dataToUpdate).transacting(trx)
            return session ? session[0] : undefined
        } catch (err: any) {
            return new Err({ message: 'could not update the room session', cause: err })
        }
    }

    async getLastGoingSession(trx: Knex.Transaction, by: {}) {
        try {
            const session = await this.knex("room_sessions as rs").where(by).transacting(trx)
                .whereNull('ended_at')
                .select(["rs.id as id", "rs.started_at", "rs.room_code"])
                .orderBy("started_at", "desc").first()
            return session
        } catch (err: any) {
            return new Err({ message: 'could not get the room session', cause: err })
        }
    }

    async getRoomSession(trx: Knex.Transaction, room_session_id: number) {
        try {
            const session = await this.knex("room_sessions as rs").where({ id: room_session_id }).transacting(trx)
                .select(["rs.id as id", "rs.started_at", "rs.ended_at", "rs.room_code"]).first()
            return session
        } catch (err: any) {
            return new Err({ message: 'could not get the room session', cause: err })
        }
    }

    async getRoomSessionDetail(trx: Knex.Transaction, room_session_id: number): Promise<IRoomSessionDetail[] | Err> {
        try {
            const sessions = await this.knex("room_sessions as rs").where({ 'rs.id': room_session_id }).transacting(trx)
                .join("rooms as r", "rs.room_code", "=", "r.code")
                .join("participants as p", "p.room_session_id", "=", "rs.id")
                .leftJoin("users as u", "p.user_id", "=", "u.id")
                .join("users as owner", "r.owner", "=", "owner.id")
                .select(["u.name as participant_name", "u.lastname as participant_lastname", "u.username as participant_username",
                    "u.avatar as participant_avatar", "u.email as participant_email",
                    "owner.username as owner_username", "owner.name as owner_name", "owner.lastname as owner_lastname", "owner.id as owner_id",
                    "r.passcode", "r.is_edu", "r.image", "r.created_at as room_created_at", "r.title", "r.is_temp",
                    "r.code", "r.is_open",
                    "p.role as participant_role", "p.websocket_id as participant_websocket_id", "p.id as participant_id", "p.room_session_id",
                    "p.created_at as participant_joined_at", "p.deleted_at as participant_left_at", "p.display_name as participant_display_name", "p.user_id as participant_user_id",
                    "rs.id as room_session_id", "rs.started_at", "rs.ended_at"])
            return sessions
        } catch (err: any) {
            return new Err({ message: 'could not get the room session detail', cause: err })
        }
    }

    async getRoomMember(trx: Knex.Transaction, room_code: string, uid: number) {
        try {
            return await this.knex("members").where({ room_code, user_id: uid }).first().transacting(trx).select("*")
        } catch (error) {
            return new Err({ message: 'could not get room that user is membered', cause: error })
        }
    }

    async getRoomSessions(trx: Knex.Transaction, room_code: string, limit: number, order: "asc" | "desc") {
        try {
            const session = await this.knex("room_sessions as rs").where({ "code": room_code }).transacting(trx)
                .select(["rs.id", "rs.started_at", "rs.ended_at", "rs.room_code"]).orderBy("rs.started_at", order).limit(limit)
            return session
        } catch (err: any) {
            return new Err({ message: 'could not get the room sessions', cause: err })
        }
    }

    async getParticipantLoginMoves(trx: Knex.Transaction, participant_id: number, order: "asc" | "desc") {
        try {
            const moves = await this.knex("participant_login_moves as mv").where({ participant_id }).transacting(trx)
                .orderBy("mv.loged_in_at", order).returning(["mv.loged_in_at", "mv.loged_out_at"])
        } catch (err: any) {
            return new Err({ message: 'could not get participant logins', cause: err })
        }
    }

    async getParticipantLastLoginMovesByLimit(trx: Knex.Transaction, participant_id: number, order: "asc" | "desc", limit: number) {
        try {
            const moves = await this.knex("participant_login_moves as mv").where({ participant_id }).transacting(trx)
                .orderBy("mv.loged_in_at", order).returning(["mv.loged_in_at", "mv.loged_out_at"]).limit(limit)
        } catch (err: any) {
            return new Err({ message: 'could not get participant logins', cause: err })
        }
    }

    async getRoomsUserParticipated(trx: Knex.Transaction, uid: number) {
        try {
            const rooms: any[] = await this.knex("users as u").transacting(trx)
                .innerJoin("participants as p", "u.id", "=", 'p.user_id')
                .innerJoin("rooms as r", "p.room_code", "=", 'r.code')
                .where({ 'p.user_id': uid })
                .select("r.image", "r.created_at", "r.updated_at", "r.is_edu", "r.title", "r.is_temp", "r.code")
            return rooms
        } catch (err) {
            return new Err({ message: 'could not get rooms', cause: err })
        }
    }

    async getRoomsUserMembered(trx: Knex.Transaction, uid: number) {
        try {
            const rooms: any[] = await this.knex("users as u").transacting(trx)
                .innerJoin("members as m", "u.id", "=", 'm.user_id')
                .innerJoin("rooms as r", "m.room_code", "=", 'r.code')
                .where({ 'm.user_id': uid })
                .select("r.image", "r.created_at", "r.updated_at", "r.is_edu", "r.title", "r.is_temp", "r.code")
            return rooms
        } catch (err) {
            return new Err({ message: 'could not get rooms', cause: err })
        }
    }

    async createParticipantLoginMove(trx: Knex.Transaction, participant_id: number, data: { loged_in_at?: string, loged_out_at?: string }) {
        try {
            const move = await this.knex("participant_login_moves").insert({ participant_id, ...data }).transacting(trx).returning("*")
            return move ? move[0] : undefined
        } catch (err: any) {
            return new Err({ message: 'could not create participant login', cause: err })
        }
    }
    /* 
        async selectHostsAndOwner(trx: Knex.Transaction, room_code: string, uid: number) {
            try {
                const rooms = await this.knex("rooms as r").whereNull("deleted_at")
                    .join("participants as p", "r.code", "=", "p.room_code")
                    .join("users as u", "u.id", "=", "p.user_id")
                    .select("u.name", "u.lastname", "u.username", "u.email", "u.avatar", "u.id")
                    .where({ "r.code": room_code, "p.user_id": uid })
                    .transacting(trx)
                return rooms
            }
            catch (err) {
                return new Err({ message: 'could not get the hosts and owner', cause: err })
            }
        } */

    async deleteParticipantLoginMoves(trx: Knex.Transaction, participant_id: number) {
        try {
            const deledMove = await this.knex("participant_login_moves").where({ participant_id }).del().transacting(trx).returning("*")
            return deledMove
        } catch (err: any) {
            return new Err({ message: 'could not delete participant login', cause: err })
        }
    }

    async createMember(trx: Knex.Transaction, body: CreateMemberDto) {
        try {
            const member = await this.knex("members").insert(body).transacting(trx).returning("*")
            return member ? member[0] : undefined
        } catch (err) {
            return new Err({ message: 'could not create member', cause: err })
        }
    }

    async deleteMember(trx: Knex.Transaction, uid: number, room_code: string) {
        try {
            const member = await this.knex("members").where({ user_id: uid, room_code }).update({ left_at: moment().format() }).transacting(trx)
                .returning("*")
            return member ? member[0] : undefined
        } catch (err) {
            return new Err({ message: 'could not delete member', cause: err })
        }
    }

    async updateMember(trx: Knex.Transaction, uid: number, room_code: string, role: number) {
        try {
            const member = await this.knex("members").where({ user_id: uid, room_code }).update({ role }).transacting(trx)
                .returning("*")
            return member ? member[0] : undefined
        } catch (err) {
            return new Err({ message: 'could not update member', cause: err })
        }
    }

    async getMember(trx: Knex.Transaction, room_code: string, uid: number) {
        try {
            const member: IMember = await this.knex("members as m").where({ user_id: uid, room_code })
                .where({ room_code, user_id: uid })
                .innerJoin("users as u", "u.id", "=", "m.user_id")
                .select(["m.role as room_role", "u.name", "u.lastname", "u.username", "u.email", "u.avatar"])
                .first()
                .transacting(trx)
            return member
        } catch (err) {
            return new Err({ message: 'could not get the member', cause: err })

        }
    }
}