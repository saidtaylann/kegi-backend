import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { RoomModel } from './room.model';
import Database from 'src/database';
import type { Knex } from 'knex';
import { IParticipant, IRoom, IRoomDetail, IRoomSession, IRoomSessionDetail, SessionDetailOfUser } from 'src/types/room';
import { CreateRoomDto, UpdateRoomDto } from "src/room/dto/room.dto"
import config from "src/config.json"
import path from 'path';
//import { createIncomingFile, deleteFile } from 'src/utils/files';
import { UserService } from 'src/user/user.service';
import { Error as Err } from 'src/types/error';
import { IUser } from 'src/types/user';
import { deleteS3File, updateS3File, uploadS3File } from 'src/services/aws/s3';
import { ERole, ERoomRole } from 'src/enums/roles.enum';
import { Socket, Server } from 'socket.io';
import moment from 'moment';
import Redis from "../services/redis"
import { RedisRoomSession } from 'src/services/redis/session';
import type { JoinSessionDto, StartSessionDto } from 'src/ws/dto/room.dto';
import { ExtSocket } from 'src/types/common';

@Injectable()
export class RoomService {
  private redisSessions: RedisRoomSession
  private readonly knex = Database.connection
  private readonly redis = Redis
  private readonly roomImageDir = `${config.DIRS.store}/${config.DIRS.images}/room/`;
  constructor(private readonly roomModel: RoomModel, @Inject(forwardRef(() => UserService)) private readonly userService: UserService) {
    this.redisSessions = this.redis.getSessionModule()
  }

  async getRoomsBy(trx: Knex.Transaction, by: {}): Promise<IRoom[] | Err> {
    const rooms: IRoom[] | Err = await this.roomModel.getRoomsBy(trx, by)
    if (rooms instanceof Err) {
      rooms.code = 500
      return rooms
    }
    return rooms
  }

  async getRoomsUserMembered(trx: Knex.Transaction, uid: number) {
    const rooms: IRoom[] | Err = await this.roomModel.getRoomsUserMembered(trx, uid)
    if (rooms instanceof Err) {
      rooms.code = 500
    }
    return rooms
  }

  async getRoomByCode(trx: Knex.Transaction, code: string) {
    const room: IRoom | Err = await this.roomModel.getRoomBy(trx, { code })
    if (room instanceof Err) {
      room.code = 500
      return room
    }
    return room
  }

  async getRoomDetail(trx: Knex.Transaction, code: string) {
    const room: IRoomDetail[] | Err = await this.roomModel.getRoomDetail(trx, { code: code })
    if (room instanceof Err) {
      room.code = 500
      return room
    }
    if (room) {
      return this.parseRoomDetail(room)
    }
  }

  async createRoom(trx: Knex.Transaction, data: CreateRoomDto & { owner: number }) {
    const user: IUser | Err = await this.userService.getUserById(trx, data.owner)
    if (user instanceof Err) {
      user.cause = user.message
      user.message = "an error occured while creating room"
      user.code = 500
      return user
    }
    else if (user) {
      if (!user.verified) {
        return new Err({ message: 'user is not verified', code: 401 })
      }
      const roomCode: string | Err = await this.roomModel.createRoom(trx, data)
      if (roomCode instanceof Err) {
        roomCode.code = 500
        return roomCode
      }
      if (data.is_temp === false) {
        const newMember = await this.roomModel.createMember(trx, { user_id: user.id, room_code: roomCode, role: ERoomRole.OWNER })
        if (newMember instanceof Err) {
          newMember.code = 500
          return newMember
        }
      }
      return roomCode
    }
  }

  async updateRoom(trx: Knex.Transaction, code: string, dataToUpdate: UpdateRoomDto) {
    const room: IRoom | Err = await this.roomModel.updateRoom(trx, code, dataToUpdate)
    if (room instanceof Err) {
      room.code = 500
      return room
    }
    return room
  }

  async updateImage(
    trx: Knex.Transaction,
    room_code: string,
    image: any,
  ) {
    const room: IRoom | Err = await this.roomModel.getRoomBy(trx, { id: room_code })
    if (room instanceof Err) {
      room.cause = room.message
      room.message = "an error occured while updating room image"
      room.code = 500
      return room
    }
    const imageName = this.setRoomImageName(room_code, image.name)
    if (!room.image) {
      const isUploaded = await uploadS3File(image, this.setRoomImageName(room_code, image.name), this.roomImageDir)
      if (isUploaded instanceof Err) {
        isUploaded.cause = isUploaded.message
        isUploaded.message = "an error occured while uploading the image"
        isUploaded.code = 500
        return isUploaded
      }
      const isUpdated = await this.roomModel.updateRoom(trx, room_code, { avatar: imageName })
      if (isUpdated instanceof Err) {
        isUpdated.cause = isUpdated.message
        isUpdated.message = "an error occured while updating the image"
        isUpdated.code = 500
        return isUpdated
      }
    } else {
      const isUploaded = await updateS3File(image, this.setRoomImageName(room_code, image.name), this.roomImageDir)
      if (isUploaded instanceof Err) {
        isUploaded.cause = isUploaded.message
        isUploaded.message = "an error occured while uploading the image"
        isUploaded.code = 500
        return isUploaded
      }
      const isUpdated = await this.roomModel.updateRoom(trx, room_code, { image: imageName })
      if (isUpdated instanceof Err) {
        isUpdated.cause = isUpdated.message
        isUpdated.message = "an error occured while updating the image"
        isUpdated.code = 500
        return isUpdated
      }

    }
    return true
  }

  async deleteRoom(trx: Knex.Transaction, room_code: string) {
    const room: IRoom | Err = await this.roomModel.deleteRoom(trx, room_code)
    if (room instanceof Err) {
      room.code = 500
      return room
    }
    return room
  }

  async getRoomSessions(trx: Knex.Transaction, room_code: string) {
    const sessions: IRoomSession[] | Err = await this.roomModel.getRoomSessions(trx, room_code, 50, "desc")
    if (sessions instanceof Err) {
      sessions.code = 500
      return sessions
    }
    return sessions
  }

  async getRoomSessionDetail(trx: Knex.Transaction, session_id: number) {
    const session: IRoomSessionDetail[] | Err = await this.roomModel.getRoomSessionDetail(trx, session_id)
    if (session instanceof Err) {
      session.code = 500
      return session
    }
    return this.parseRoomSessionDetail(session)
  }

  setRoomImageName(room_code: string, imageName: string): string {
    return `${room_code}${path.extname(imageName)}`;
  }

  parseRoomDetail(room: Array<IRoomDetail>) {
    const parsedParticipants: any = {}
    parsedParticipants.code = room[0].code
    parsedParticipants.room_title = room[0].room_title && room[0].room_title
    parsedParticipants.passcode = room[0].passcode && room[0].passcode
    parsedParticipants.image = room[0].image && room[0].image
    parsedParticipants.is_edu = room[0].is_edu
    parsedParticipants.is_temp = room[0].is_temp
    parsedParticipants.room_created_at = room[0].room_created_at
    parsedParticipants.owner_username = room[0].owner_username
    parsedParticipants.owner_name = room[0].owner_name
    parsedParticipants.owner_lastname = room[0].owner_lastname
    parsedParticipants.owner_id = room[0].owner_id
    parsedParticipants.is_open = room[0].is_open
    parsedParticipants.members = []
    room.forEach((r: IRoomDetail) => {
      parsedParticipants.members.push({
        id: r.member_id,
        name: r.member_name,
        lastname: r.member_lastname,
        username: r.member_username,
        email: r.member_email,
        avatar: r.member_avatar,
        joined_at: r.member_joined_at,
        participant_role: r.member_role,
      })

    })
    return parsedParticipants
  }
  parseRoomSessionDetail(room: Array<IRoomSessionDetail>) {
    const parsedParticipants: any = {}
    parsedParticipants.code = room[0].code
    parsedParticipants.title = room[0].title && room[0].title
    parsedParticipants.passcode = room[0].passcode && room[0].passcode
    parsedParticipants.image = room[0].image && room[0].image
    parsedParticipants.is_edu = room[0].is_edu
    parsedParticipants.is_temp = room[0].is_temp
    parsedParticipants.room_created_at = room[0].room_created_at
    parsedParticipants.owner_username = room[0].owner_username
    parsedParticipants.owner_name = room[0].owner_name
    parsedParticipants.owner_id = room[0].owner_id
    parsedParticipants.owner_lastname = room[0].owner_lastname
    parsedParticipants.room_session_id = room[0].room_session_id
    parsedParticipants.started_at = room[0].started_at
    parsedParticipants.ended_at = room[0].ended_at
    parsedParticipants.participants = []
    room.forEach((rs: IRoomSessionDetail) => {
      parsedParticipants.participants.push({
        id: rs.participant_id,
        ...(rs.participant_user_id && {
          user_id: rs.participant_user_id,
          name: rs.participant_name,
          lastname: rs.participant_lastname,
          username: rs.participant_username,
          email: rs.participant_email,
          avatar: rs.participant_avatar,
          is_member: rs.participant_is_member
        }),
        ...(rs.participant_display_name && {
          display_name: rs.participant_display_name,
        }),
        websocket_id: rs.participant_websocket_id,
        joined_at: rs.participant_joined_at,
        left_at: rs.participant_left_at,
        role: rs.participant_role
      })

    })
    return parsedParticipants
  }

  async hasPermission(trx: Knex.Transaction, roomCodeOrSessionId: string | number, uid: number, roles: number[], forMember: boolean): Promise<Err | { role: number }> {
    let isHost;
    if (forMember) {
      isHost = await this.roomModel.getMember(trx, roomCodeOrSessionId as string, uid)
    } else {
      isHost = await this.roomModel.getParticipant(trx, roomCodeOrSessionId as number)
    }
    if (isHost instanceof Err) {
      isHost.code = 500
      return isHost
    }
    if (roles.includes(isHost.role)) return { role: isHost.role }
    return new Err({ message: 'not authorized', code: 401 })
  }

  async createSession(socket: Socket, room_code: string, uid: number, sessionPreferences: StartSessionDto) {
    return this.knex.transaction(async trx => {
      const isOwner: any = await this.roomModel.getRoomOnly(trx, room_code)
      if (isOwner instanceof Err) {
        isOwner.cause = isOwner.message
        isOwner.message = "createSession"
        isOwner.code = 500
        return isOwner
      }
      if (isOwner.owner !== uid) return new Err({ message: 'createSession', code: 401 })
      const room = await this.roomModel.getRoomBy(trx, { code: room_code })
      if (room instanceof Err) {
        room.code = 500
        room.cause = room.message
        room.message = "roomCreateSession"
        return room
      }
      const newSession = await this.roomModel.createRoomSession(trx, room_code)
      if (newSession instanceof Err) {
        newSession.code = 500
        return newSession
      }
      const participant = await this.addParticipant(trx, newSession, ERoomRole.OWNER, false, socket.id, uid)
      if (participant instanceof Err) return participant
      const sessionDetail = await this.getRoomSessionDetail(trx, newSession)
      if (sessionDetail instanceof Err) {
        sessionDetail.code = 500
        sessionDetail.cause = sessionDetail.message
        sessionDetail.message = "createRoomSession"
        return sessionDetail
      }
      Object.assign(sessionDetail, {
        is_mic_authorized: sessionPreferences.isMicAuthorized,
        is_cam_authorized: sessionPreferences.isCamAuthorized,
        is_sharing_screen_authorized: sessionPreferences.isSharingScreenAuthorized
      })
      await this.redisSessions.setSession(newSession, sessionPreferences)
      if (participant) {
        socket.join(`${newSession}`)
        socket.emit("roomSessionCreated", sessionDetail)
      }
    })
  }

  async requestToJoinSession(socket: Socket, room_code: string, passcode?: string, uid?: number, display_name?: string): Promise<Err> {
    const eventName: string = "requestToJoinSession"
    return this.knex.transaction(async trx => {

      const room = await this.roomModel.getRoomBy(trx, { code: room_code })
      if (room instanceof Err) {
        room.cause = room.message
        // code==500 bir server hatası var demektir. 500 hariç herhangi bir ws hata kodu kullanılmamıştır.
        room.code = 500
        room.message = eventName
        return room
      }
      else if (room) {
        // is_open özelliğinin açık ya da kapalı olması kalıcı oda olduğunu gösterir. Geçici odalar kapandığı anda deleted_at olur çünkü
        const roomSession: IRoomSession | Err = await this.roomModel.getLastGoingSession(trx, { room_code })
        if (roomSession instanceof Err) {
          roomSession.cause = roomSession.message
          roomSession.message = eventName
          roomSession.code = 500
          return room
        }
        if (room.is_open) {
          let participant: any;
          if (room.is_temp === false) {
            if (roomSession) {
              if (uid && !display_name) {
                const isMember = await this.roomModel.getMember(trx, room.code, uid)
                if (isMember instanceof Err) {
                  isMember.cause = isMember.message
                  isMember.code = 500
                  isMember.message = eventName
                  return isMember
                }
                else if (!isMember) {
                  if (passcode !== room.passcode) return new Err({ message: 'invalidPasscode' })
                  participant = await this.addParticipant(trx, roomSession.id, ERoomRole.PARTICIPANT, false, socket.id, uid)
                  if (participant instanceof Err) {
                    participant.message = eventName
                    return participant
                  }
                } else {
                  participant = await this.addParticipant(trx, roomSession.id, isMember.role, true, socket.id)
                  if (participant instanceof Err) {
                    participant.message = eventName
                    return participant
                  }
                }
              } else {
                if (passcode !== room.passcode) return new Err({ message: 'invalidPasscode' })
                participant = await this.addParticipant(trx, roomSession.id, 0, false, socket.id, undefined, display_name)
                if (participant instanceof Err) {
                  participant.message = eventName
                  return participant
                }
              }

              socket.emit("joinSessionRequestAccepted", { room_session_id: roomSession.id, code: room.code })
            }
          }
          else if (room.is_temp === true) {
            if (uid && !display_name) {
              if (passcode !== room.passcode) return new Err({ message: 'invalidPasscode' })
              participant = await this.addParticipant(trx, roomSession.id, ERoomRole.PARTICIPANT, false, socket.id, uid)
              if (participant instanceof Err) {
                participant.message = eventName
                return participant
              }
            } else {
              if (room.passcode && passcode !== room.passcode) return new Err({ message: 'invalidPasscode' })

              participant = await this.addParticipant(trx, roomSession.id, ERoomRole.PARTICIPANT, false, socket.id, undefined, display_name)
              if (participant instanceof Err) {
                participant.message = eventName
                return participant
              }
            }
            socket.emit("joinSessionRequestAccepted", { room_session_id: roomSession.id, code: room.code })
          }
        }
        return new Err({ message: 'the room is closed', code: 401 })
      }
    })
  }

  async joinSession(socket: Socket, incomingData: JoinSessionDto) {
    const eventName = "joinSession"
    const sessionPreferences = await this.redisSessions.getSession(incomingData.sessionId)
    await this.redisSessions.setParticipant(incomingData)
    return this.knex.transaction(async trx => {
      const sessionDetail = await this.getRoomSessionDetail(trx, incomingData.sessionId)
      if (sessionDetail instanceof Err) {
        sessionDetail.code = 500
        sessionDetail.cause = sessionDetail.message
        sessionDetail.message = eventName
        return sessionDetail
      }
      sessionDetail.is_mic_authorized = sessionPreferences.isMicAuthorized == "true" ? true : false
      sessionDetail.is_cam_authorized = sessionPreferences.isCamAuthorized == "true" ? true : false
      sessionDetail.is_sharing_screen_authorized = sessionPreferences.isSharingScreenAuthorized ? true : false
      let newParticipant;
      for (const p of sessionDetail.participants) {
        const participant = await this.redisSessions.getParticipant(incomingData.sessionId, p.websocket_id)
        const parsedParticipant: {
          isMicOpen: boolean,
          isCamOpen: boolean
          sharingScreen?: string
          isMicAuthorized: boolean,
          isCamAuthorized: boolean
          isSharingScreenAuthorized: boolean
        } = {
          isMicOpen: true,
          isCamOpen: true,
          isMicAuthorized: true,
          isCamAuthorized: true,
          isSharingScreenAuthorized: true,
        }
        parsedParticipant.isMicOpen = participant.isMicOpen == "true" ? true : false
        parsedParticipant.isCamOpen = participant.isCamOpen == "true" ? true : false
        parsedParticipant.sharingScreen = participant.sharingScreen ?? ''
        parsedParticipant.isMicAuthorized = participant.isMicAuthorized == "true" ? true : false
        parsedParticipant.isCamAuthorized = participant.isCamAuthorized == "true" ? true : false
        parsedParticipant.isSharingScreenAuthorized = participant.isSharingScreenAuthorized == "true" ? true : false

        p.stream_id = participant.streamId
        if (p.role === 0) {
          if (parsedParticipant.isMicAuthorized) {
            p.is_mic_open = parsedParticipant.isMicOpen
          }
          else {
            p.is_mic_open = false
          }
          if (parsedParticipant.isCamAuthorized) {

            p.is_cam_open = parsedParticipant.isCamOpen
          }

          else {
            p.is_cam_open = false
          }
          if (parsedParticipant.isSharingScreenAuthorized) {
            p.sharing_screen = parsedParticipant.sharingScreen
          }
          else {
            p.sharing_screen = ''
          }
        }
        else {
          p.is_mic_open = parsedParticipant.isMicOpen
          p.is_cam_open = parsedParticipant.isCamOpen
          p.sharing_screen = parsedParticipant.sharingScreen
        }
        p.is_mic_authorized = parsedParticipant.isMicAuthorized && sessionDetail.is_mic_authorized
        p.is_cam_authorized = parsedParticipant.isCamAuthorized && sessionDetail.is_cam_authorized
        p.is_sharing_screen_authorized = parsedParticipant.isSharingScreenAuthorized && sessionDetail.is_sharing_screen_authorized
        if (p.websocket_id === incomingData.websocketId) { newParticipant = p }
      }
      socket.join(`${incomingData.sessionId}`)
      socket.emit("joinedSession", sessionDetail)
      socket.in(`${incomingData.sessionId}`).emit("someoneJoinedSession", newParticipant)
    })

  }

  async addParticipant(trx: Knex.Transaction, room_session_id: number, role: number, is_member?: boolean,
    websocket_id?: string, uid?: number, display_name?: string) {

    const participant: IParticipant | Err = await this.roomModel.createParticipant(trx, {
      user_id: uid, room_session_id, role, is_member, display_name, websocket_id
    })
    if (participant instanceof Err) {
      participant.cause = participant.message
      participant.code = 500
      return participant
    }
    else if (participant) {
      const loginMove = await this.roomModel.createParticipantLoginMove(trx, participant.id, { loged_in_at: moment().format() })
      if (loginMove instanceof Err) {
        loginMove.cause = loginMove.message
        loginMove.code = 500
        return loginMove
      }
      return participant
    }
  }

  async removeParticipant(trx: Knex.Transaction, participant_id: number, sessionId: number, socketId: string, eventName: string) {
    const deletedParticipant = await this.roomModel.deleteParticipant(trx, participant_id)
    if (deletedParticipant instanceof Err) {
      deletedParticipant.cause = deletedParticipant.message
      deletedParticipant.message = eventName
      deletedParticipant.code = 500
      return deletedParticipant
    }
    else if (deletedParticipant) {
      const loginMove = await this.roomModel.createParticipantLoginMove(trx, participant_id, { loged_out_at: moment().format() })
      if (loginMove instanceof Err) {
        loginMove.cause = loginMove.message
        deletedParticipant.message = eventName
        loginMove.code = 500
        return loginMove
      }
    }
    const streamIdOfParticipant = (await this.redisSessions.getParticipant(sessionId, socketId)).streamId
    await this.redisSessions.deleteParticipant(sessionId, socketId)
    return streamIdOfParticipant
  }

  async openRoom(socket: Socket, room_code: string, uid: number) {
    return this.knex.transaction(async trx => {
      const memberPermission = await this.hasPermission(trx, room_code, uid, [2, 3], true)
      if (memberPermission instanceof Err) {
        memberPermission.cause = memberPermission.message
        memberPermission.message = "openRoom"
        return memberPermission
      }
      else if (memberPermission) {
        const session = await this.roomModel.createRoomSession(trx, room_code)
        if (session instanceof Err) {
          session.code = 500
          session.cause = session.message
          session.message = "openRoom"
          return session
        }
        else if (session) {
          const room = await this.roomModel.updateRoom(trx, room_code, { is_open: true })
          if (room instanceof Err) {
            room.code = 500
            room.cause = room.message
            room.message = "openRoom"
            return room
          }
          else if (room) {
            const participant = await this.addParticipant(trx, session.id, memberPermission.role, true, socket.id, uid)
            if (participant instanceof Err) {
              participant.message = "openRoom"
              return participant
            }
            const sessionDetail = await this.roomModel.getRoomSessionDetail(trx, session.id)
            if (sessionDetail instanceof Err) {
              session.code = 500
              session.cause = session.message
              session.message = "openRoom"
              return session
            }
            if (sessionDetail) {
              socket.join(session)
              socket.emit("roomOpened", sessionDetail)
            }
          }
        }
      }

    })
  }

  async _endSession(trx: Knex.Transaction, sessionId: number, roomCode: string, eventName: string) {
    const closedSession = this.roomModel.updateRoomSession(trx, sessionId, { ended_at: moment().format() })
    if (closedSession instanceof Err) {
      closedSession.cause = closedSession.message
      closedSession.message = eventName
      closedSession.code = 500
      return closedSession
    }
    const room = await this.roomModel.updateRoom(trx, roomCode, { is_open: true })
    if (room instanceof Err) {
      room.cause = room.message
      room.message = eventName
      room.code = 500
      return room
    }
    await this.redisSessions.deleteSession(sessionId)
  }

  async mute(server: Server, body: { socketId: string[], roomSessionId: number }) {
    for (const socket in body.socketId) {
      await this.redisSessions.updateParticipant(body.roomSessionId, socket, "isMicOn", false)
    }
    server.in(`${body.roomSessionId}`).emit("muted", body.socketId)
  }

  async unmute(server: Server, body: { socketId: string[], roomSessionId: number }) {
    for (const socket in body.socketId) {
      await this.redisSessions.updateParticipant(body.roomSessionId, socket, "isMicOn", true)
    }
    server.in(`${body.roomSessionId}`).emit("unmuted", body.socketId)
  }

  async openCam(server: Server, body: { socketId: string[], roomSessionId: number }) {
    for (const socket in body.socketId) {
      await this.redisSessions.updateParticipant(body.roomSessionId, socket, "isCamOpen", true)
    }
    server.to(`${body.roomSessionId}`).emit("turnedOnCam", body.socketId)
  }

  async closeCam(server: Server, body: { socketId: string[], roomSessionId: number }) {
    for (const socket in body.socketId) {
      await this.redisSessions.updateParticipant(body.roomSessionId, socket, "isCamOpen", false)
    }
    server.in(`${body.roomSessionId}`).emit("turnedOffCam", body.socketId)
  }

  async shareScreen(server: Server, body: { socketId: string, roomSessionId: number, streamId: string }) {
    await this.redisSessions.updateParticipant(body.roomSessionId, body.socketId, "sharingScreen", body.streamId)
    server.in(`${body.roomSessionId}`).emit("sharedScreen", { socketId: body.socketId, streamId: body.streamId })
  }

  async stopSharingScreen(server: Server, body: { socketId: string, roomSessionId: number }) {
    await this.redisSessions.deleteParticipantField(body.roomSessionId, body.socketId, "sharingScreen")
    server.in(`${body.roomSessionId}`).emit("stoppedSharingScreen", body.socketId)
  }

  async leaveSession(server: Server, socket: ExtSocket, body: { participantId: number, socketId: string, roomSessionId: number, isClosingSession: boolean }, userId?: number) {
    const eventName = "leaveSession"
    return this.knex.transaction(async trx => {
      const roomSession = await this.roomModel.getRoomSession(trx, body.roomSessionId)
      if (roomSession instanceof Err) {
        roomSession.cause = roomSession.message
        roomSession.message = eventName
        roomSession.code = 500
      }

      if (userId) {
        if (body.isClosingSession) {
          const canUserCloseSession = await this.hasPermission(trx, body.roomSessionId, userId, [1, 2, 3], false)
          if (canUserCloseSession instanceof Err) {
            canUserCloseSession.cause = canUserCloseSession.message
            canUserCloseSession.message = eventName
            return canUserCloseSession
          }
          const participantsOfRoom = await this.roomModel.getParticipants(trx, { room_session_id: body.roomSessionId })
          if (participantsOfRoom instanceof Err) {
            participantsOfRoom.cause = participantsOfRoom.message
            participantsOfRoom.message = eventName
            participantsOfRoom.code = 500
            return participantsOfRoom
          }
          for (const p of participantsOfRoom) {
            const deletedParticipant = await this.removeParticipant(trx, p.id, body.roomSessionId, body.socketId, eventName)
            if (deletedParticipant instanceof Err) {
              deletedParticipant.cause = deletedParticipant.message
              deletedParticipant.message = eventName
              deletedParticipant.code = 500
              return deletedParticipant
            }
            server.in(`${body.roomSessionId}`).emit("sessionEnded")
            server.sockets.sockets.get(`${p.websocket_id}`).leave(`${body.roomSessionId}`)
          }
          const endedSession = await this._endSession(trx, body.roomSessionId, roomSession.room_code, eventName)
          if (endedSession instanceof Err) return endedSession
          socket.leave(`${body.roomSessionId}`)
        }

        // if the participant is a user but the room won't be closed
        else {
          const participant = await this.roomModel.getParticipant(trx, body.roomSessionId)
          if (participant instanceof Err) {
            participant.cause = participant.message
            participant.message = eventName
            participant.code = 500
            return participant
          }
          if (!participant) return new Err({ message: 'participant not found', code: 404 })
          const deletedParticipant = await this.removeParticipant(trx, body.participantId, body.roomSessionId, body.socketId, eventName)
          if (deletedParticipant instanceof Err) {
            deletedParticipant.cause = deletedParticipant.message
            deletedParticipant.message = eventName
            deletedParticipant.code = 500
            return deletedParticipant
          }
          if (server.sockets.adapter.rooms.get(`${body.roomSessionId}`).size === 1) {
            const endedSession = await this._endSession(trx, body.roomSessionId, roomSession.room_code, eventName)
            if (endedSession instanceof Err) {
              return endedSession
            }
            return server.in(`${body.roomSessionId}`).emit("sessionEnded")
          }
          if ([1, 2, 3].includes(participant.role)) {
            const authorizedParticipants = await this.roomModel.getParticipantsByRange(trx, { room_session_id: body.roomSessionId }, 'p.role', [1, 2, 3])
            if (authorizedParticipants instanceof Err) {
              authorizedParticipants.cause = authorizedParticipants.message
              authorizedParticipants.message = eventName
              authorizedParticipants.code = 500
              return authorizedParticipants
            }
            if (authorizedParticipants.length === 0) {
              const participantToAuthorize = await this.roomModel.getParticipantsByOrder(trx, body.roomSessionId, "p.created_at", "asc", 1)
              if (participantToAuthorize instanceof Err) {
                participantToAuthorize.cause = participantToAuthorize.message
                participantToAuthorize.message = eventName
                participantToAuthorize.code = 500
                return participantToAuthorize
              }
              const newAuthorizedParticipant = await this.roomModel.updateParticipant(trx, participantToAuthorize[0].id, { role: 1 })
              if (newAuthorizedParticipant instanceof Err) {
                newAuthorizedParticipant.cause = newAuthorizedParticipant.message
                newAuthorizedParticipant.message = eventName
                newAuthorizedParticipant.code = 500
                return newAuthorizedParticipant
              }
              socket.emit("leftSession")
              socket.in(`${body.roomSessionId}`).emit("someoneLeftSession", { streamId: deletedParticipant, websocketId: body.socketId })
              server.in(`${body.roomSessionId}`).emit("changedParticipantRole", { participantId: [participantToAuthorize[0].id], newRole: newAuthorizedParticipant.role })
            }
          }
          socket.leave(`${body.roomSessionId}`)
        }
      }

      else {
        const deletedParticipant = await this.removeParticipant(trx, body.participantId, body.roomSessionId, body.socketId, eventName)
        if (deletedParticipant instanceof Err) {
          deletedParticipant.cause = deletedParticipant.message
          deletedParticipant.message = eventName
          deletedParticipant.code = 500
          return deletedParticipant
        }
        socket.emit("leftSession")
        socket.in(`${body.roomSessionId}`).emit("someoneLeftSession", body.socketId)
        if (server.sockets.adapter.rooms.get(`${body.roomSessionId}`).size === 1) {
          const endedSession = await this._endSession(trx, body.roomSessionId, roomSession.room_code, eventName)
          if (endedSession instanceof Err) {
            return endedSession
          }
          server.in(`${body.roomSessionId}`).emit("sessionEnded")
        }
        socket.leave(`${body.roomSessionId}`)
      }

    })
  }

  async endSession(server: Server, sessionId: number) {
    const eventName = "endSession"
    return this.knex.transaction(async trx => {
      const session = await this.roomModel.getRoomSession(trx, sessionId)
      const participants = await this.roomModel.getParticipants(trx, { room_session_id: sessionId })
      if (participants instanceof Err) {
        participants.cause = participants.message
        participants.message = eventName
        participants.code = 500
        return participants
      }
      for (const p of participants) {
        const removedParticipant = await this.removeParticipant(trx, p.id, sessionId, p.websocket_id, eventName)
        if (removedParticipant instanceof Err) return removedParticipant

      }
      const endedSession = await this._endSession(trx, sessionId, session.room_code, eventName)
      if (endedSession instanceof Err) return endedSession
      server.in(`${sessionId}`).emit("sessionEnded")

    })
  }

  async switchMicAuthorization(server: Server, body: { roomSessionId: number, socketId: string[], isMicAllowed: boolean, all: boolean }) {
    for (const socket of body.socketId) {
      await this.redisSessions.updateParticipant(body.roomSessionId, socket, "isMicAuthorized", body.isMicAllowed)
      if (!body.isMicAllowed) {
        await this.redisSessions.updateParticipant(body.roomSessionId, socket, "isMicOpened", false)
      }
    }
    if (body.all) {
      await this.redisSessions.updateSession(body.roomSessionId, "isMicAuthorized", body.isMicAllowed)
    }
    server.in(`${body.roomSessionId}`).emit("switchedMicAuthorization", { isMicAllowed: body.isMicAllowed, sockets: body.socketId, all: body.all })
  }

  async switchCamAuthorization(server: Server, body: { roomSessionId: number, socketId: string[], isCamAllowed: boolean, all: boolean }) {
    for (const socket of body.socketId) {
      await this.redisSessions.updateParticipant(body.roomSessionId, socket, "isCamAuthorized", body.isCamAllowed)
      if (!body.isCamAllowed) {
        await this.redisSessions.updateParticipant(body.roomSessionId, socket, "isCamOpen", false)
      }
    }
    if (body.all) {
      await this.redisSessions.updateSession(body.roomSessionId, "isCamAuthorized", body.isCamAllowed)
    }
    server.in(`${body.roomSessionId}`).emit("switchedCamAuthorization", { isCamAllowed: body.isCamAllowed, sockets: body.socketId })
  }

  async switchSharingScreenAuthorization(server: Server, body: { roomSessionId: number, socketId: string[], isSharingScreenAllowed: boolean, all: boolean }) {
    for (const socket of body.socketId) {
      await this.redisSessions.updateParticipant(body.roomSessionId, socket, "isSharingScreenAuthorized", body.isSharingScreenAllowed)
      if (!body.isSharingScreenAllowed) {
        await this.redisSessions.updateParticipant(body.roomSessionId, socket, "sharingScreen", "")
      }
    }
    if (body.all) {
      await this.redisSessions.updateSession(body.roomSessionId, "isSharingScreenAuthorized", body.isSharingScreenAllowed)
    }
    await this.redisSessions.updateSession(body.roomSessionId, "isSharingScreenAuthorized", body.isSharingScreenAllowed)
    server.in(`${body.roomSessionId}`).emit("switchedSharingScreenAuthorization", { isSharingScreenAllowed: body.isSharingScreenAllowed, sockets: body.socketId })
  }
  async changeParticipantRole(server: Server, body: { roomSessionId: number, participantId: number[], newRole: number }) {
    const eventName = "changeParticipantRole"
    await this.knex.transaction(async trx => {
      for (const pId of body.participantId) {
        const updatedParticipant = await this.roomModel.updateParticipant(trx, pId, { role: body.newRole })
        if (updatedParticipant instanceof Err) {
          updatedParticipant.cause = updatedParticipant.message
          updatedParticipant.message = eventName
          updatedParticipant.code = 500
          return updatedParticipant
        }
      }
    })
    server.in(`${body.roomSessionId}`).emit("changedParticipantRole", { participantId: body.participantId, newRole: body.newRole })
  }
}