import { Module, forwardRef, } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { UserModule } from 'src/user/user.module';
import { AuthModel } from './auth.model';
import { RolesGuard } from './guards/roles.guard';

@Module({
  controllers: [AuthController],
  imports: [forwardRef(() => UserModule)],
  providers: [AuthService, AuthModel],
  exports: [AuthService]
})
export class AuthModule { }
