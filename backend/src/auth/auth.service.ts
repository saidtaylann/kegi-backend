import { Injectable, forwardRef, Inject } from '@nestjs/common';
import { v4 as uuidv4 } from 'uuid';
import { IEmailVerificationToken, IJWT } from 'src/types/auth';
import { comparePasswords } from "src/utils/encryption"
import jwt from "jsonwebtoken"
import { Knex } from 'knex';
import Redis from "src/services/redis";
import { UserModel } from 'src/user/user.model';
import { genRandomString } from 'src/utils/random';
import { IUser, IUserDevice, IUserLogin } from 'src/types/user';
import config from "../config.json"
import { AuthModel } from './auth.model';
import { OnlineUser } from 'src/services/redis/users';
import { Error as Err } from 'src/types/error';

@Injectable()
export class AuthService {
    private readonly onlineUsers: OnlineUser;
    constructor(@Inject(forwardRef(() => UserModel)) private readonly UserModel: UserModel, private readonly authModel: AuthModel) {
        this.onlineUsers = Redis.getUserModule();
    }

    async comparePasswords(password: string, hash: string): Promise<boolean | Err> {
        const isSame: boolean | Err = await comparePasswords(password, hash)
        return isSame
    }

    createRefreshToken(): string | Err {
        try {
            return uuidv4()
        } catch (err) {
            return new Err({ message: 'could not create refresh token', cause: err })
        }
    }

    signJWT(payload: IJWT, expire: string): string | Err {
        try {
            return jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: expire + "m" })
        }
        catch (err) {
            return new Err({ message: 'could not sign jwt', cause: err })
        }
    }

    decodeJWT(token: string) {
        try {
            return jwt.decode(token)
        }
        catch (err) {
            return new Err({ message: 'could not decode jwt', cause: err })

        }
    }

    verifyJWT(token: string, config?: { ignoreExpiration?: boolean }): IJWT | Err {
        try {
            const decoded = jwt.verify(token, process.env.JWT_SECRET, config)
            return decoded as IJWT
        }
        catch (err) {
            if (err.name === "TokenExpiredError")
                return new Err({ message: err.message, cause: err })
            else if (err.name === "JsonWebTokenError")
                return new Err({ message: err.message, cause: err })

        }
    }

    async login(trx: Knex.Transaction, data: { email: string; password: string }, websocket: string, did: string, browser: string, platform: string) {
        let device: IUserDevice | Err;
        const user: IUser | Err = await this.UserModel.selectUserWithPassword(trx, data.email)
        if (user instanceof Err) {
            user.cause = user.message
            user.message = "an error ocured while logging in"
            user.code = 500
            return user

        }
        if (user) {
            const arePasswordsSame: boolean | Err = await this.comparePasswords(
                data.password,
                user.password,
            )
            if (arePasswordsSame instanceof Err) {
                arePasswordsSame.cause = arePasswordsSame.message
                arePasswordsSame.message = "an error occured while logging in"
                arePasswordsSame.code = 500
                return arePasswordsSame
            }
            delete user.password
            if (arePasswordsSame) {
                if (did) {
                    device = await this.UserModel.selectUserDeviceBy(trx, { id: did })
                    if (device instanceof Err) {
                        device.code = 500
                        return device
                    }
                    if (!device) {
                        device = await this.UserModel.createUserDevice(trx, { id: genRandomString(128), user_id: user.id, platform, browser })
                        if (device instanceof Err) {
                            device.code = 500
                            return device
                        }
                    }
                }
                else {
                    device = await this.UserModel.createUserDevice(trx, { id: genRandomString(128), user_id: user.id, platform, browser })
                    if (device instanceof Err) {
                        device.code = 500
                        return device
                    }
                }
                const signPayload: IJWT = {
                    id: user.id,
                    role: user.role
                }
                const accessToken = this.signJWT(signPayload, config.AUTH.accessTokenExpirationMinutes)
                if (accessToken instanceof Err) {
                    accessToken.cause = accessToken.message
                    accessToken.message = "an error occured creating access token"
                    accessToken.code = 500
                    return accessToken
                }
                const refreshToken: string | Err = this.createRefreshToken();
                if (refreshToken instanceof Err) {
                    refreshToken.code = 500
                    return refreshToken
                }

                const delLogin = await this.UserModel.markUserLoginAsLogedOutBy(trx, { user_id: user.id, device_id: device.id })
                if (delLogin instanceof Err) {
                    delLogin.cause = delLogin.message
                    delLogin.message = "an error occured while logging out other logs"
                    delLogin.code = 500
                    return delLogin
                }
                const loginedUser = await this.UserModel.createUserLogin(trx, {
                    id: genRandomString(128),
                    user_id: user.id,
                    refresh_token: refreshToken,
                    device_id: device.id,
                })
                if (loginedUser instanceof Err) {
                    loginedUser.code = 500
                    return loginedUser
                }
                if (loginedUser) {
                    this.onlineUsers.set(websocket, user.id)
                    return {
                        user,
                        DID: device.id,
                        LID: loginedUser.id,
                        accessToken
                    }
                }
            }
        }
        return new Err({ message: 'username and password are wrong', code: 401 })
    }

    async logout(trx: Knex.Transaction, lid: string, websocket: string) {
        const logout = await this.UserModel.markUserLoginAsLogedOutBy(trx, { id: lid })
        if (logout instanceof Err) {
            logout.code = 500
            return logout
        }
        this.onlineUsers.del(websocket)
        return logout.length !== 0 ? { success: true } : { success: false }
    }

    async logoutFromAllDevices(trx: Knex.Transaction, uid: number, websocket: string): Promise<{ success: boolean } | Err> {
        const logsout = await this.UserModel.markUserLoginAsLogedOutBy(trx, { user_id: uid })
        if (logsout instanceof Err) {
            logsout.code = 500
            return logsout
        }
        this.onlineUsers.del(websocket)
        return logsout.length !== 0 ? { success: true } : { success: false }
    }

    async refresh(trx: Knex.Transaction, uid: number, urole: number, lid: string): Promise<string | Err> {
        const login: IUserLogin | Err = (await this.UserModel.markUserLoginAsLogedOutBy(trx, { id: lid }))[0]
        if (login instanceof Err) {
            login.cause = login.message
            login.message = "an error occured while getting new token"
            login.code = 500
            return login
        }
        if (login) {
            const currentDate = new Date()
            // refresh token has expired
            if (currentDate.setDate(login.updated_at.getDate() + config.AUTH.refreshTokenExirationDays) < new Date().getTime()) {

                // o device'ın aktif olan tek login'i silinir.
                const deleted = await this.UserModel.deleteUserLogin(trx, lid)
                if (deleted instanceof Err) {
                    deleted.cause = deleted.message
                    deleted.message = "an error occured while getting new token"
                    deleted.code = 500
                    return deleted
                }
            }
            // refresh token has not expired
            else {
                // regen new access and refresh tokens
                const signPayload: IJWT = {
                    id: uid,
                    role: urole
                }
                const accessToken = this.signJWT(signPayload, config.AUTH.accessTokenExpirationMinutes)
                if (accessToken instanceof Err) {
                    accessToken.cause = accessToken.message
                    accessToken.message = "an error occured while getting new token"
                    accessToken.code = 500
                    return accessToken
                }
                const newRefreshToken: string | Err = this.createRefreshToken();
                if (newRefreshToken instanceof Err) {
                    newRefreshToken.cause = newRefreshToken.message
                    newRefreshToken.message = "an error occured while getting new token"
                    newRefreshToken.code = 500
                    return newRefreshToken
                }
                const updatedLogin: IUserLogin | Err = await this.UserModel.updateUserLoginBy(trx, login.id, { refresh_token: newRefreshToken })
                if (updatedLogin instanceof Err) {
                    updatedLogin.cause = updatedLogin.message
                    updatedLogin.message = "an error occured while getting new token"
                    updatedLogin.code = 500
                    return updatedLogin
                }
                return accessToken
            }
        }
    }

    async verifyEmail(trx: Knex.Transaction, token: string): Promise<boolean | Err> {
        const isTokenExist: IEmailVerificationToken | Err = await this.authModel.selectVerificationTokenBy(trx, { token })
        if (isTokenExist instanceof Err) {
            isTokenExist.cause = isTokenExist.message
            isTokenExist.message = "an error occured while verifying email"
            isTokenExist.code = 500
            return isTokenExist
        }
        if (isTokenExist) {
            const currentDate = new Date()
            if (currentDate.setDate(isTokenExist.created_at.getHours() + config.AUTH.verificationTokenExpirationHours) < new Date().getTime()) {
                return new Err({ message: 'TokenExpired', code: 401 })
            }
            const verified: IUser | Err = await this.UserModel.update(trx, isTokenExist.user_id, { verified: true })
            if (verified instanceof Err) {
                verified.cause = verified.message
                verified.message = "an error occured while verifying email"
                verified.code = 500
                return verified
            }
            const deleted = await this.authModel.deleteVerificationToken(trx, isTokenExist.user_id)
            if (deleted instanceof Err) {
                deleted.cause = deleted.message
                deleted.message = "an error occured while verifying email"
                deleted.code = 500
                return deleted
            }
            return deleted ? true : false
        }
        return new Err({ message: 'InvalidToken', code: 404 })
    }

    async addEmailVerificationCode(trx: Knex.Transaction, uid: number) {
        const createdToken = await this.authModel.createVerificationToken(trx, uid)
        if (createdToken instanceof Err) {
            createdToken.code = 500
            return createdToken
        }
        else if (createdToken) return createdToken
        return new Err({ message: 'verification code could not created', code: 500 })
    }
    async resendVerificationToken(trx: Knex.Transaction, uid: number) {
        const newToken = await this.authModel.updateVerificationToken(trx, uid)
        if (newToken instanceof Err) {
            newToken.cause = newToken.message
            newToken.message = "an error occured while resending new verification token"
            newToken.code = 500
        }
        return newToken
    }

}
