import { Body, Controller, Get, HttpException, Inject, Param, Post, Req, Res, forwardRef } from '@nestjs/common';
import { AuthService } from './auth.service';
import Database from 'src/database';
import { ExtReq } from 'src/types/common';
import { createHttpOnlyStrictCookie } from 'src/utils/cookie';
import { Response } from "express"
import { Error as Err } from 'src/types/error';
import { IMailDataToSend, IVerificationMailData } from 'src/types/services/aws/sqs';
import { sendEmail } from 'src/services/aws/sqs';
import { UserService } from 'src/user/user.service';
import config from "../config.json"

@Controller('auth')
export class AuthController {
    private knex = Database.connection
    constructor(private readonly authService: AuthService, @Inject(forwardRef(() => UserService)) private readonly userService: UserService,) { }

    @Post("login")
    async handleLogin(@Res({ passthrough: true }) res: Response, @Req() req: ExtReq, @Body() body: { email: string, password: string, websocket: string }) {
        const DID: string = req.get("DID")
        return this.knex.transaction(async trx => {
            const loginedUser: any = await this.authService.login(trx, { ...body }, body.websocket, DID, req.useragent.browser, req.useragent.platform)
            if (loginedUser instanceof Err) {
                throw new HttpException(loginedUser.message, loginedUser.code)
            }
            return { user: loginedUser.user, DID: loginedUser.DID, LID: loginedUser.LID, accessToken: loginedUser.accessToken }
            //createHttpOnlyStrictCookie(res, "DID", loginedUser.DID, 60 * 24 * 180)
            //createHttpOnlyStrictCookie(res, "LID", loginedUser.LID, 60 * 24 * 60)
            //createHttpOnlyStrictCookie(res, "A_T", loginedUser.accessToken, +config.AUTH.accessTokenExpirationMinutes)
            //return { ...loginedUser.user }
        })
    }

    @Post("logout")
    async handleLogout(@Body() body: { websocket: string }, @Req() req: ExtReq, @Res({ passthrough: true }) res: Response): Promise<{ success: boolean }> {
        const LID = req.get("LID")
        return this.knex.transaction(async trx => {
            const login = await this.authService.logout(trx, LID, body.websocket)
            if (login instanceof Err) {
                throw new HttpException(login.message, login.code)
            }

            //res.clearCookie("A_T")
            //res.clearCookie("LID")
            return login ? { success: true } : { success: false }
        })
    }

    @Post("logout-all")
    async handleLogoutFromAllDevices(@Body() body: { websocket: string }, @Req() req: ExtReq, @Res({ passthrough: true }) res: Response): Promise<{ success: boolean }> {
        return this.knex.transaction(async trx => {
            const login = await this.authService.logoutFromAllDevices(trx, req.user.id, body.websocket)
            if (login instanceof Err) {
                throw new HttpException(login.message, login.code)
            }
            //res.clearCookie('LID')
            return login ? { success: true } : { success: false }
        })
    }

    @Post("refresh")
    async handleRefreshToken(@Req() req: ExtReq, @Res({ passthrough: true }) res: Response) {
        const LID = req.get("LID")
        const accessToken = req.get('authorization')?.split("Bearer ")
        let payload: any;
        if (LID && accessToken.length === 2) {
            payload = this.authService.verifyJWT(accessToken[1], { ignoreExpiration: true })
            return this.knex.transaction(async trx => {
                const newAccessToken = await this.authService.refresh(trx, payload.id, payload.role, LID)
                if (newAccessToken instanceof Err) {
                    throw new HttpException(newAccessToken.message, newAccessToken.code)
                }
                //createHttpOnlyStrictCookie(res, "A_T", newAccessToken, +process.env.JWT_EXPR_DURATION)
                return newAccessToken ? { accessToken: newAccessToken } : { accessToken: '' }
            })
        }
        if (payload instanceof Err) {
            throw new HttpException(payload.message, payload.code)
        }
    }

    @Post("verify-email/:token")
    async handleVerifyEmail(@Param('token') token: string): Promise<{ success: boolean }> {
        return this.knex.transaction(async trx => {
            const verified = await this.authService.verifyEmail(trx, token)
            if (verified instanceof Err) {
                throw new HttpException(verified.message, verified.code)
            }
            return verified ? { success: true } : { success: false }
        })
    }

    @Post("refresh-verification")
    async handleRefreshVerificationToken(@Req() req: ExtReq): Promise<{ success: boolean }> {
        return this.knex.transaction(async trx => {
            const user = await this.userService.getUserById(trx, req.user.id)
            if (user instanceof Err) {
                throw new HttpException(user.message, 500)
            }
            else if (user) {
                const verified = await this.authService.resendVerificationToken(trx, req.user.id)
                if (verified instanceof Err) {
                    throw new HttpException(verified.message, verified.code)
                }
                else if (verified) {
                    const tmplData: IVerificationMailData = { name: user.name, lastname: user.lastname, verificationToken: verified, verificationLink: `${config.SERVER.allowedDomain}/verify/${verified}` }
                    const data: IMailDataToSend = { to: [user.email], subject: "Kegi Mail Doğrulama", templateData: tmplData }
                    sendEmail("re-verification", data)
                    return { success: true }
                }
                return { success: false }
            }
        })
    }

}
