import { Injectable } from '@nestjs/common';
import type { Knex } from "knex"
import moment from 'moment';
import Database from 'src/database';
import { Error as Err } from 'src/types/error';
import { IUser } from 'src/types/user';
import { genRandomShortCode } from 'src/utils/random';

@Injectable()
export class AuthModel {
    private knex = Database.connection
    private verificationTokenFields: string[] = ['vt.user_id', 'vt.token', 'vt.created_at']
    constructor() {
    }

    async createVerificationToken(trx: Knex.Transaction, uid: number) {
        try {
            const verificationToken = genRandomShortCode(6)
            const createdRows: any = await this.knex('email_verification_tokens as vt').where({ 'user_id': uid })
                .insert({ token: verificationToken, 'user_id': uid }).transacting(trx).onConflict("token").ignore()
            if (createdRows.rowCount === 0) await this.createVerificationToken(trx, uid)
            else return verificationToken
            return createdRows ? createdRows[0] : undefined
        } catch (err) {
            return new Err({ message: 'verification token could not updated', cause: err })
        }
    }

    async selectVerificationTokenBy(trx: Knex.Transaction, by: { user_id?: number, token?: string }) {
        try {
            const incomingToken = await this.knex('email_verification_tokens as vt').where(by).first().transacting(trx).select(this.verificationTokenFields)
            return incomingToken
        } catch (err) {

            return new Err({ message: 'verification token could not get', cause: err })
        }
    }

    async deleteVerificationToken(trx: Knex.Transaction, user_id: number) {
        try {
            const deletedToken = await this.knex('email_verification_tokens as vt').where({ user_id }).del().transacting(trx).returning("*")
            return deletedToken ? deletedToken[0] : undefined
        } catch (err) {
            return new Err({ message: 'verification token could not delete', cause: err })
        }
    }

    async updateVerificationToken(trx: Knex.Transaction, uid: number) {
        try {
            const verificationToken = genRandomShortCode(6)
            const updatedToken: any = await this.knex('email_verification_tokens as vt').where({ 'vt.user_id': uid })
                .update({ token: verificationToken, created_at: moment().format() }).transacting(trx).onConflict("token").ignore()
            if (updatedToken.rowCount === 0) await this.updateVerificationToken(trx, uid)
            else return verificationToken
            return updatedToken ? updatedToken[0] : undefined
        } catch (err) {
            return new Err({ message: 'verification token could not updated', cause: err })
        }
    }
}
