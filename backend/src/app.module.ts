import { MiddlewareConsumer, Module, NestModule, Req, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from '@nestjs/config';
import { RoomModule } from './room/room.module';
import { JWTMiddleware, OptionalJWTMiddleware } from './middlewares/auth.middleware';
import { WsModule } from './ws/ws.module';
import { jwtMiddlewareRoutes, optionalJwtMiddlewareRoutes } from './middlewares/routeList';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    UserModule,
    AuthModule,
    RoomModule,
    WsModule
  ],
  controllers: [AppController],
  providers: [AppService],
})

export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(JWTMiddleware)
      .forRoutes(
        ...jwtMiddlewareRoutes
      )
    consumer
      .apply(OptionalJWTMiddleware)
      .forRoutes(...optionalJwtMiddlewareRoutes)
    consumer
      .apply(OptionalJWTMiddleware)
      .forRoutes(
        { path: '/users/start', method: RequestMethod.POST },
      )
  }
}