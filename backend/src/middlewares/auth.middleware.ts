import { HttpException, Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import jwt from "jsonwebtoken"

@Injectable()
export class JWTMiddleware implements NestMiddleware {
    use(req: Request, res: Response, next: NextFunction) {
        let accessToken: string[];
        accessToken = req.get("authorization")?.split("Bearer ")
        if (accessToken.length === 2) {
            return jwt.verify(accessToken[1], process.env.JWT_SECRET, async (err: any, user: any) => {
                if (err) {
                    if (err.name === 'TokenExpiredError') {
                        throw new HttpException('TokenExpired', 401)
                    }
                    throw new HttpException("invalid token", 401)

                }
                req.user = user
                next()
            })
        }
        throw new HttpException('Invalid token', 401)
    }
}

@Injectable()
export class OptionalJWTMiddleware implements NestMiddleware {
    use(req: Request, res: Response, next: NextFunction) {
        let accessToken: string[];
        accessToken = req.get("authorization")?.split("Bearer ")
        if (accessToken.length !== 2) next()
        else {
            return jwt.verify(accessToken[1], process.env.JWT_SECRET, async (err: any, user: any) => {
                if (err) {
                    if (err.name === 'TokenExpiredError') {
                        throw new HttpException('TokenExpired', 401)
                    }
                    throw new HttpException('Invalid token', 401)
                }
                req.user = user
                next()
            })
        }
    }
}