import { RequestMethod } from "@nestjs/common";

export const jwtMiddlewareRoutes = [
    { path: '/users', method: RequestMethod.GET },
    { path: '/users', method: RequestMethod.PUT },
    { path: '/users/avatar', method: RequestMethod.POST },
    { path: '/users/avatar', method: RequestMethod.DELETE },
    { path: '/users/filter', method: RequestMethod.POST },
    { path: '/users/:id', method: RequestMethod.GET },
    { path: '/users/:id', method: RequestMethod.DELETE },

    { path: '/auth/logout', method: RequestMethod.POST },
    { path: '/auth/logout-all', method: RequestMethod.POST },
    { path: '/auth/verify-email/:token', method: RequestMethod.POST },
    { path: '/auth/refresh-verification', method: RequestMethod.POST },

    { path: '/rooms', method: RequestMethod.GET },
    { path: '/rooms/is-member/:code', method: RequestMethod.GET },
    { path: '/rooms', method: RequestMethod.POST },
    { path: '/rooms/:code/image', method: RequestMethod.POST },

]

export const optionalJwtMiddlewareRoutes = [
    { path: '/rooms/:code', method: RequestMethod.GET },

]