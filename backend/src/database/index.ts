import knex from "knex"

class Database {
  static readonly connection = knex({
    client: 'pg',
    connection: {
      host: "localhost",
      port: 5432,
      user: "admin",
      password: "password",
      database: "kegiNestDB",
      timezone: "+00:00"
    },
    searchPath: ['public', 'knex'],
  });
}

export default Database