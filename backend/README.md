# Kegi | Egitim Platformu

### **_Geliştirme aşamasındadır_**

<p style="margin-top:10px;font-weight:bold">Kegi; Canlı derslere girilip ödevlerin verilebildiği, puanlamaların yapılabildiği kapsamlı bir eğitim platformu
Bunun yanı sıra sıradan kullanıcılar için de canlı toplantı seçenekleri de mevcuttur.</p>

## Neler Yapıldı?

- [x] Canlı toplantılar oluşturma (sadece kayıtlı kullanıcılar)
- [x] Ekran paylaşımı
- [x] Toplantı içi yetkilendirme (Oda sahibi, yetkili, katılımcı...)
- [x] Bütün (yetkisiz) kullanıcıların kamera ve mikrofon yetkilerini alma/verme
- [x] Toplantı içi genel sohbet ekranı
- [x] Katılımcılar listesi
- [x] İstenilen kullanıcının kamera, mikrofon ve ekran paylaşımı yetkisini alma/verme
- [x] Kullanıcıların rollerini değiştirme
- [x] Uygulama kullanıcılarının profillerini görüntüleme ve güncellemesi

Projenin istemci **repo**suna [gitmek için tıklayınız.](https://gitlab.com/saidtaylann/kegi-frontend)

## Sunucuda Hangi Araçlar Kullanıldı?

1. NestJS (ExpressJs)
2. PostgreSQL
3. Redis: Online kullanıcı ve toplantıları kaydeder
4. RabbitMQ: Mail bilgilerini Go mail servisne aktarır
5. Go(lang): Mail Servisi (hazır ama kullanılmıyor)
6. AWS Lambda: Mail servisi (Go yerine aktif olarak kullanılıyor)
7. AWS SQS: Mail bilgilerimi Lambda Function'a aktarır.
8. AWS S3: Kullanıcı avatar ve oda fotoğraflarını depolar.
9. Socket.io: Client ile iletişim kurar.

> **Uygulama birden çok bileşen ve servisten oluştuğu için uygulamayı indirip başlatmak zor olacaktır. Bu yüzden nasıl kurulduğu anlatılmamıştır**

## Projeden Kareler

1. Toplantı Ekranı </br>
   <img src="./images/main.png" width=50%>

2. Ekran Paylaşma </br>
   <img src="./images/sharingScreen.png" width=50%>

3. Katılımcılar </br>
   <img src="./images/contextMenu.png" width=50%>

4. Sohbet </br>
   <img src="./images/chat.png" width=50%>

5. Mikrofon ve Kamera Kapatma </br>
   <img src="./images/closeCam&Mic.png" width=50%>

6. Toplantıdan Ayrılma </br>
   <img src="./images/leave.png" width=50%>
