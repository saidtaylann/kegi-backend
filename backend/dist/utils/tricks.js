"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.erhand = void 0;
const erhand = (promise, improved) => {
    return promise
        .then((data) => [data, null])
        .catch((err) => {
        if (improved) {
            Object.assign(err, improved);
        }
        return [err, null];
    });
};
exports.erhand = erhand;
//# sourceMappingURL=tricks.js.map