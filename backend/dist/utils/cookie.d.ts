import { Response } from "express";
export declare const createHttpOnlyStrictCookie: (res: Response, key: string, value: any, maxAge?: number, expires?: Date) => Promise<void>;
