"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.genRandomShortCode = exports.genRandomDashedString = exports.genRandomString = void 0;
const uuid_1 = require("uuid");
const genRandomString = (size) => {
    return (0, uuid_1.v4)();
};
exports.genRandomString = genRandomString;
const genRandomDashedString = (length) => {
    let str = "";
    const chars = "abcdefghijklmoprstuvyzwxq";
    const charLen = chars.length;
    for (let i = 1; i <= length; i++) {
        if (i === 5 || i === 10)
            str += "-";
        else
            str += chars.charAt(Math.floor(Math.random() * charLen));
    }
    return str;
};
exports.genRandomDashedString = genRandomDashedString;
const genRandomShortCode = (length) => {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    const charactersLength = characters.length;
    let counter = 0;
    while (counter < length) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
        counter += 1;
    }
    return result;
};
exports.genRandomShortCode = genRandomShortCode;
//# sourceMappingURL=random.js.map