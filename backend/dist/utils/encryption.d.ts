import { Error as Err } from "src/types/error";
export declare const hashPassword: (password: string) => Promise<string | Err>;
export declare const comparePasswords: (password: string, hash: string) => Err | Promise<boolean>;
