export declare const genRandomString: (size: number) => string;
export declare const genRandomDashedString: (length: number) => string;
export declare const genRandomShortCode: (length: number) => string;
