"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createIncomingFile = exports.deleteFile = void 0;
const fs_1 = __importDefault(require("fs"));
const deleteFile = (path) => {
    try {
        fs_1.default.rm(path, () => {
        });
        return true;
    }
    catch (err) {
        throw err;
    }
};
exports.deleteFile = deleteFile;
const createIncomingFile = (file, dir, fileName) => {
    return file.mv(dir + fileName, async () => { });
};
exports.createIncomingFile = createIncomingFile;
//# sourceMappingURL=files.js.map