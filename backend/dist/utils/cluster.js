"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cluster_1 = __importDefault(require("cluster"));
const os_1 = require("os");
const cCPUs = (0, os_1.cpus)().length;
const clusterify = (appFunc, startPort) => {
    if (cluster_1.default.isPrimary) {
        for (let i = 0; i < cCPUs; i++) {
            cluster_1.default.fork();
        }
        cluster_1.default.on('online', function (worker) {
        });
        cluster_1.default.on('exit', function (worker, code, signal) {
        });
    }
    else {
        appFunc(startPort + cluster_1.default.worker.id - 1);
    }
};
exports.default = clusterify;
//# sourceMappingURL=cluster.js.map