"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.comparePasswords = exports.hashPassword = void 0;
const bcrypt_1 = __importDefault(require("bcrypt"));
const config_json_1 = __importDefault(require("../config.json"));
const error_1 = require("../types/error");
const hashPassword = async (password) => {
    try {
        const round = config_json_1.default.ENCRYPTION.BCRYPT_SALT_ROUND;
        return bcrypt_1.default.hash(password, round);
    }
    catch (err) {
        return new error_1.Error({ message: "an error ocured while hashing password", cause: err });
    }
};
exports.hashPassword = hashPassword;
const comparePasswords = (password, hash) => {
    try {
        return bcrypt_1.default.compare(password, hash);
    }
    catch (err) {
        return new error_1.Error({ message: "an error occured while comparing password", cause: err });
    }
};
exports.comparePasswords = comparePasswords;
//# sourceMappingURL=encryption.js.map