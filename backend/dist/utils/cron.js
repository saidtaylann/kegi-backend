"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cronJob = void 0;
const cron_1 = require("cron");
const cronJob = (job, minute) => {
    return new cron_1.CronJob(`*/${minute} * * * *`, () => job, null, true, "Europe/Istanbul");
};
exports.cronJob = cronJob;
//# sourceMappingURL=cron.js.map