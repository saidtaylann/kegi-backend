export declare const deleteFile: (path: string) => boolean;
export declare const createIncomingFile: (file: any, dir: string, fileName: string) => any;
