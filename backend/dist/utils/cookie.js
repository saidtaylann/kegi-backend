"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createHttpOnlyStrictCookie = void 0;
const createHttpOnlyStrictCookie = async (res, key, value, maxAge, expires) => {
    res.cookie(key, value, Object.assign(Object.assign({ httpOnly: true, sameSite: "strict", domain: '', secure: true }, (maxAge && { maxAge: maxAge * 1000 * 60 })), (expires && { expires: expires })));
};
exports.createHttpOnlyStrictCookie = createHttpOnlyStrictCookie;
//# sourceMappingURL=cookie.js.map