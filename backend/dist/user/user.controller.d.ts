/// <reference types="multer" />
import { CreateUserDto, UpdateUsersDto } from './dto/users.dto';
import { UserService } from './user.service';
import { IResponseCreatedUser } from 'src/types/responses';
import { ExtReq } from 'src/types/common';
import { Response } from "express";
export declare class UserController {
    private readonly UserService;
    private readonly knex;
    private onlineUsers;
    constructor(UserService: UserService);
    handleCreateUser(body: CreateUserDto, req: ExtReq, res: Response): Promise<IResponseCreatedUser>;
    handleUpdateUser(body: UpdateUsersDto, req: ExtReq): Promise<import("../types/user").IUser>;
    handleUpdateAvatar(req: ExtReq, avatar: Express.Multer.File): Promise<{
        avatar: {
            success: boolean;
        };
    }>;
    handleDeleteAvatar(req: ExtReq): Promise<boolean>;
    handleGetUserById(req: ExtReq, id: number): Promise<void>;
    handleGetUser(req: ExtReq): Promise<import("../types/user").IUser>;
    handleDeleteUser(req: ExtReq, id: number): Promise<import("../types/user").IUser>;
}
