import { CreateUserDto } from './dto/users.dto';
import type { Knex } from "knex";
import { IUserLogin, IUser, IUserDevice } from 'src/types/user';
import { Error as Err } from "src/types/error";
export declare class UserModel {
    private knex;
    private userReturningFields;
    private userLoginsReturningFields;
    private userDeviceReturningFields;
    constructor();
    selectUsersBy(trx: Knex.Transaction, data: {}): Promise<IUser[] | Err>;
    selectUserBy(trx: Knex.Transaction, data: {}): Promise<IUser | Err>;
    selectUserWithPassword(trx: Knex.Transaction, email: string): Promise<IUser | Err>;
    selectUserByVerificationtoken(trx: Knex.Transaction, token: string): Promise<IUser | Err>;
    create(trx: Knex.Transaction, dataToInsert: CreateUserDto, avatar: string): Promise<any>;
    update(trx: Knex.Transaction, id: number, dataToUpdate: {}): Promise<IUser | Err>;
    delete(trx: Knex.Transaction, id: number): Promise<any>;
    createUserLogin(trx: Knex.Transaction, data: Omit<IUserLogin, "updated_at">): Promise<Err | IUserLogin>;
    deleteUserLogin(trx: Knex.Transaction, lid: string): Promise<any>;
    markUserLoginAsLogedOutBy(trx: Knex.Transaction, by: {}): Promise<Err | any[]>;
    deleteUserLoginsBy(trx: Knex.Transaction, by: {}): Promise<Err | any[]>;
    updateUserLoginBy(trx: Knex.Transaction, lid: string, by: {}): Promise<any>;
    selectUserLoginBy(trx: Knex.Transaction, by: {}): Promise<Err | IUserLogin>;
    selectUsersLoginsBy(trx: Knex.Transaction, by: {}): Promise<Err | IUserLogin[]>;
    createUserDevice(trx: Knex.Transaction, data: IUserDevice): Promise<any>;
    deleteUserDevice(trx: Knex.Transaction, did: string): Promise<any>;
    updateUserDevice(trx: Knex.Transaction, did: string, updateData?: {
        platform?: string;
    }): Promise<any>;
    selectUserDevicesBy(trx: Knex.Transaction, by: {}): Promise<Err | IUserDevice[]>;
    selectUserDeviceBy(trx: Knex.Transaction, by: {}): Promise<Err | IUserDevice>;
}
