"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const encryption_1 = require("../utils/encryption");
const user_model_1 = require("./user.model");
const path_1 = __importDefault(require("path"));
const auth_service_1 = require("../auth/auth.service");
const redis_1 = __importDefault(require("../services/redis"));
const config_json_1 = __importDefault(require("../config.json"));
const s3_1 = require("../services/aws/s3");
const sqs_1 = require("../services/aws/sqs");
const error_1 = require("../types/error");
const database_1 = __importDefault(require("../database"));
const room_service_1 = require("../room/room.service");
let UserService = class UserService {
    constructor(UserModel, authService, roomService) {
        this.UserModel = UserModel;
        this.authService = authService;
        this.roomService = roomService;
        this.defaultAvatar = "default.png";
        this.avatarDir = `${config_json_1.default.DIRS.images}/avatar/`;
        this.knex = database_1.default.connection;
        this.onlineUsers = redis_1.default.getUserModule();
    }
    async getUsers(trx, cond) {
        const users = await this.UserModel.selectUsersBy(trx, cond);
        if (users instanceof error_1.Error) {
            users.code = 500;
            return users;
        }
        return users;
    }
    async getUserById(trx, id) {
        const user = await this.UserModel.selectUserBy(trx, { id });
        if (user instanceof error_1.Error) {
            user.code = 500;
            return user;
        }
        return user;
    }
    async getUserByEmail(trx, email) {
        const user = await this.UserModel.selectUserBy(trx, { email });
        if (user instanceof error_1.Error) {
            user.code = 500;
            return user;
        }
        return user;
    }
    async getUserByVerificationToken(trx, token) {
        const user = await this.UserModel.selectUserByVerificationtoken(trx, token);
        if (user instanceof error_1.Error) {
            user.code = 500;
            return user;
        }
        return user;
    }
    async createUser(trx, body, platform, browser) {
        let isUserExist = await this.UserModel.selectUserBy(trx, { email: body.email });
        if (isUserExist instanceof error_1.Error) {
            isUserExist.cause = isUserExist.message;
            isUserExist.message = "an error occured while querying whether user is exist";
            isUserExist.code = 500;
            return isUserExist;
        }
        else if (isUserExist) {
            return new error_1.Error({ message: "exist such a user with this email", code: 409 });
        }
        let { password } = body;
        const hashedPassword = await (0, encryption_1.hashPassword)(password);
        if (hashedPassword instanceof Error) {
            return new error_1.Error({ message: "an error occured while hashing password in phase creating user", code: 409 });
        }
        const newUser = await this.UserModel.create(trx, Object.assign(Object.assign({}, body), { password: hashedPassword }), this.defaultAvatar);
        if (newUser instanceof error_1.Error) {
            newUser.code = 500;
            return newUser;
        }
        else if (newUser) {
            const verificationToken = await this.authService.addEmailVerificationCode(trx, newUser.id);
            if (verificationToken instanceof error_1.Error) {
                verificationToken.code = 500;
                return verificationToken;
            }
            if (verificationToken) {
                const tmplData = { name: body.name, lastname: body.lastname, verificationToken, verificationLink: `${config_json_1.default.SERVER.allowedDomain}/verify/${verificationToken}` };
                const data = { to: [body.email], subject: "Kegi Mail Doğrulama! Son Bir Adım Kaldı.", templateData: tmplData };
                (0, sqs_1.sendEmail)("verification", data);
            }
            const loginData = await this.authService.login(trx, { email: body.email, password: body.password }, body.websocket, undefined, browser, platform);
            if (loginData) {
                return Object.assign(Object.assign({}, loginData), { UID: newUser.id });
            }
        }
        return new error_1.Error({ message: 'could not user created', code: 500 });
    }
    async updateUser(trx, id, body) {
        const updatedUser = await this.UserModel.update(trx, id, body);
        if (updatedUser instanceof error_1.Error) {
            updatedUser.code = 500;
            return updatedUser;
        }
        return updatedUser;
    }
    async deleteUser(trx, id) {
        const deletedUser = await this.UserModel.delete(trx, id);
        if (deletedUser instanceof error_1.Error) {
            deletedUser.code = 500;
            return deletedUser;
        }
        return deletedUser;
    }
    async updateAvatar(trx, uid, avatar) {
        const user = await this.getUserById(trx, uid);
        if (user instanceof error_1.Error) {
            user.cause = user.message;
            user.message = "an error occured while updating avatar";
            user.code = 500;
            return user;
        }
        const avatarName = this.setAvatarName(uid, avatar.originalname);
        if (user.avatar === this.defaultAvatar) {
            const isUploaded = await (0, s3_1.uploadS3File)(avatar, avatarName, this.avatarDir);
            if (isUploaded instanceof error_1.Error) {
                isUploaded.code = 500;
                return isUploaded;
            }
            const isUpdated = await this.UserModel.update(trx, uid, { avatar: avatarName });
            if (isUpdated instanceof error_1.Error) {
                isUpdated.cause = isUpdated.message;
                isUpdated.message = "could not update avatar";
                isUpdated.code = 500;
                return isUpdated;
            }
        }
        else {
            const isUpdated = await (0, s3_1.updateS3File)(avatar, avatarName, this.avatarDir);
            if (isUpdated instanceof error_1.Error) {
                isUpdated.cause = isUpdated.message;
                isUpdated.message = "could not update avatar";
                isUpdated.code = 500;
                return isUpdated;
            }
        }
        return { success: true };
    }
    async deleteAvatar(trx, uid) {
        const user = await this.getUserById(trx, uid);
        if (user instanceof error_1.Error) {
            user.cause = user.message;
            user.message = "could not delete user avatar";
            user.code = 500;
            return user;
        }
        const isUpdated = await this.UserModel.update(trx, uid, { avatar: this.defaultAvatar });
        if (isUpdated instanceof error_1.Error) {
            isUpdated.cause = isUpdated.message;
            isUpdated.message = "could not update user avatar";
            isUpdated.code = 500;
            return isUpdated;
        }
        const isDeleted = await (0, s3_1.deleteS3File)(user.avatar, this.avatarDir);
        if (isDeleted instanceof error_1.Error) {
            isDeleted.cause = isDeleted.message;
            isDeleted.message = "could not delete user avatar";
            isDeleted.code = 500;
            return isDeleted;
        }
        else if (isDeleted)
            return isUpdated.avatar;
        return "";
    }
    setAvatarName(uid, avatarname) {
        return `${uid}${path_1.default.extname(avatarname)}`;
    }
    async startSession(socket, registeredUser) {
        this.knex.transaction(async (trx) => {
            if (registeredUser) {
                const user = await this.UserModel.selectUserBy(trx, { id: registeredUser.id });
                if (user instanceof error_1.Error) {
                    user.cause = user.message;
                    user.message = "an error occured while getting user info";
                    return user;
                }
                else if (user) {
                    const login = await this.UserModel.selectUserLoginBy(trx, { id: registeredUser.LID });
                    if (login instanceof error_1.Error) {
                        login.cause = login.message;
                        login.message = "an error occured while getting login info of the user";
                        login.code = 500;
                        return login;
                    }
                    if (!login) {
                        return new error_1.Error({ message: 'invalid LID', code: 404 });
                    }
                    this.onlineUsers.set(socket.id, user.id);
                    const rooms = await this.roomService.getRoomsUserMembered(trx, registeredUser.id);
                    if (rooms instanceof error_1.Error) {
                        rooms.code = 500;
                        return rooms;
                    }
                    if (rooms.length > 0) {
                        rooms.forEach((r) => {
                            socket.join(r.code);
                        });
                    }
                    socket.emit("userSessionStarted", registeredUser.id);
                    this.onlineUsers.set(socket.id, user.id);
                }
            }
            else {
                socket.emit("userSessionStarted", -1);
            }
        });
    }
};
UserService = __decorate([
    (0, common_1.Injectable)(),
    __param(1, (0, common_1.Inject)((0, common_1.forwardRef)(() => auth_service_1.AuthService))),
    __param(2, (0, common_1.Inject)((0, common_1.forwardRef)(() => room_service_1.RoomService))),
    __metadata("design:paramtypes", [user_model_1.UserModel,
        auth_service_1.AuthService,
        room_service_1.RoomService])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map