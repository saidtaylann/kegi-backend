/// <reference types="multer" />
import { CreateUserDto, GetUsersDto, UpdateUsersDto } from "./dto/users.dto";
import { UserModel } from "./user.model";
import { AuthService } from "src/auth/auth.service";
import type { Knex } from "knex";
import { IUser } from "src/types/user";
import { Error as Err } from "src/types/error";
import type { Socket } from "socket.io";
import { RoomService } from "src/room/room.service";
export declare class UserService {
    private readonly UserModel;
    private readonly authService;
    private readonly roomService;
    private defaultAvatar;
    private avatarDir;
    private readonly onlineUsers;
    private readonly knex;
    constructor(UserModel: UserModel, authService: AuthService, roomService: RoomService);
    getUsers(trx: Knex.Transaction, cond: GetUsersDto): Promise<IUser[] | Err>;
    getUserById(trx: Knex.Transaction, id: number): Promise<IUser | Err>;
    getUserByEmail(trx: Knex.Transaction, email: string): Promise<IUser | Err>;
    getUserByVerificationToken(trx: Knex.Transaction, token: string): Promise<Err | IUser>;
    createUser(trx: Knex.Transaction, body: CreateUserDto, platform: string, browser: string): Promise<{
        DID: string;
        LID: any;
        accessToken: string;
        UID: number;
    } | Err>;
    updateUser(trx: Knex.Transaction, id: number, body: UpdateUsersDto): Promise<IUser | Err>;
    deleteUser(trx: Knex.Transaction, id: number): Promise<IUser | Err>;
    updateAvatar(trx: Knex.Transaction, uid: number, avatar: Express.Multer.File): Promise<{
        success: boolean;
    } | Err>;
    deleteAvatar(trx: Knex.Transaction, uid: number): Promise<string | Err>;
    setAvatarName(uid: number, avatarname: any): string;
    startSession(socket: Socket, registeredUser?: {
        id: number;
        LID: string;
    }): Promise<void>;
}
