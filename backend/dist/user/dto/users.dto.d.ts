export declare class CreateUserDto {
    name: string;
    lastname: string;
    email: string;
    password: string;
    username: string;
    type: string;
    role: number;
    websocket: string;
}
export declare class GetUsersDto {
    name?: string;
    lastName?: string;
    username?: string;
    verified?: boolean;
    type?: string;
    role?: number;
}
export declare class UpdateUsersDto {
    name?: string;
    lastname?: string;
    email?: string;
    password?: string;
    username?: string;
    type?: string;
    role?: number;
}
export declare class LoginDto {
    email: string;
    password: string;
}
