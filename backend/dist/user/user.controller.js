"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const common_1 = require("@nestjs/common");
const decorators_1 = require("@nestjs/common/decorators");
const exceptions_1 = require("@nestjs/common/exceptions");
const users_dto_1 = require("./dto/users.dto");
const user_service_1 = require("./user.service");
const database_1 = __importDefault(require("../database"));
const roles_decorator_1 = require("../auth/decorators/roles.decorator");
const roles_enum_1 = require("../enums/roles.enum");
const platform_express_1 = require("@nestjs/platform-express");
const error_1 = require("../types/error");
const roles_guard_1 = require("../auth/guards/roles.guard");
const redis_1 = __importDefault(require("../services/redis"));
let UserController = class UserController {
    constructor(UserService) {
        this.UserService = UserService;
        this.knex = database_1.default.connection;
        this.onlineUsers = redis_1.default.getUserModule();
    }
    async handleCreateUser(body, req, res) {
        return this.knex.transaction(async (trx) => {
            const resp = await this.UserService.createUser(trx, body, req.useragent.platform, req.useragent.browser);
            if (resp instanceof error_1.Error) {
                throw new exceptions_1.HttpException(resp.message, resp.code);
            }
            else {
                return { UID: resp.UID, accessToken: resp.accessToken, LID: resp.LID, DID: resp.DID };
            }
        });
    }
    async handleUpdateUser(body, req) {
        return this.knex.transaction(async (trx) => {
            const updatedUser = await this.UserService.updateUser(trx, req.user.id, body);
            if (updatedUser instanceof error_1.Error) {
                throw new exceptions_1.HttpException(updatedUser.message, updatedUser.code);
            }
            return updatedUser;
        });
    }
    async handleUpdateAvatar(req, avatar) {
        return this.knex.transaction(async (trx) => {
            const updated = await this.UserService.updateAvatar(trx, req.user.id, avatar);
            if (updated instanceof error_1.Error) {
                throw new exceptions_1.HttpException(updated.message, updated.code);
            }
            return { avatar: updated };
        });
    }
    async handleDeleteAvatar(req) {
        try {
            return this.knex.transaction(async (trx) => {
                await this.UserService.deleteAvatar(trx, req.user.id);
                return true;
            });
        }
        catch (err) {
            throw new exceptions_1.HttpException(err, 500);
        }
    }
    async handleGetUserById(req, id) {
        if (req.user.id === id) {
            return this.knex.transaction(async (trx) => {
                const user = await this.UserService.getUserById(trx, id);
                if (user instanceof error_1.Error) {
                    throw new exceptions_1.HttpException(user.message, user.code);
                }
            });
        }
        throw new exceptions_1.HttpException("you can just only get your info", 401);
    }
    async handleGetUser(req) {
        return this.knex.transaction(async (trx) => {
            const user = await this.UserService.getUserById(trx, req.user.id);
            if (user instanceof error_1.Error) {
                throw new exceptions_1.HttpException(user.message, user.code);
            }
            return user;
        });
    }
    async handleDeleteUser(req, id) {
        return this.knex.transaction(async (trx) => {
            const user = await this.UserService.deleteUser(trx, req.user.id);
            if (user instanceof error_1.Error) {
                throw new exceptions_1.HttpException(user.message, user.code);
            }
            return user;
        });
    }
};
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, decorators_1.Req)()),
    __param(2, (0, common_1.Res)({ passthrough: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [users_dto_1.CreateUserDto, Object, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "handleCreateUser", null);
__decorate([
    (0, common_1.Put)(),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, decorators_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [users_dto_1.UpdateUsersDto, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "handleUpdateUser", null);
__decorate([
    (0, common_1.Post)("avatar"),
    (0, decorators_1.UseInterceptors)((0, platform_express_1.FileInterceptor)('avatar')),
    __param(0, (0, decorators_1.Req)()),
    __param(1, (0, decorators_1.UploadedFile)(new common_1.ParseFilePipe({
        validators: [
            new common_1.FileTypeValidator({ fileType: 'image/jpeg' || "image/png" }),
        ],
    }))),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "handleUpdateAvatar", null);
__decorate([
    (0, common_1.Delete)("avatar"),
    __param(0, (0, decorators_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "handleDeleteAvatar", null);
__decorate([
    (0, common_1.Get)(":id"),
    __param(0, (0, decorators_1.Req)()),
    __param(1, (0, decorators_1.Param)('id', new common_1.ParseIntPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Number]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "handleGetUserById", null);
__decorate([
    (0, common_1.Get)(),
    (0, decorators_1.UseGuards)(roles_guard_1.RolesGuard),
    (0, roles_decorator_1.Roles)(roles_enum_1.ERole.USER, roles_enum_1.ERole.ADMIN, roles_enum_1.ERole.SUPERADMIN),
    __param(0, (0, decorators_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "handleGetUser", null);
__decorate([
    (0, common_1.Delete)(":id"),
    __param(0, (0, decorators_1.Req)()),
    __param(1, (0, decorators_1.Param)('id', new common_1.ParseIntPipe())),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Number]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "handleDeleteUser", null);
UserController = __decorate([
    (0, common_1.Controller)('users'),
    __metadata("design:paramtypes", [user_service_1.UserService])
], UserController);
exports.UserController = UserController;
//# sourceMappingURL=user.controller.js.map