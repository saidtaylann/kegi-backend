"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserModel = void 0;
const common_1 = require("@nestjs/common");
const error_1 = require("../types/error");
const database_1 = __importDefault(require("../database"));
const moment_1 = __importDefault(require("moment"));
let UserModel = class UserModel {
    constructor() {
        this.knex = database_1.default.connection;
        this.userReturningFields = ['u.id', 'u.name', 'u.lastname', 'u.avatar', 'u.username', 'u.email', 'u.role', 'u.verified', 'u.type'];
        this.userLoginsReturningFields = ['ul.id', 'ul.refresh_token', 'ul.user_id', 'ul.updated_at', 'ul.created_at'];
        this.userDeviceReturningFields = ['ud.id', 'ud.platform', 'ud.user_id'];
    }
    async selectUsersBy(trx, data) {
        try {
            const selectedUsers = await this.knex("users as u").where(data).whereNull("deleted_at").select(this.userReturningFields).transacting(trx);
            return selectedUsers;
        }
        catch (err) {
            return new error_1.Error({ message: "could not get users", cause: err });
        }
    }
    async selectUserBy(trx, data) {
        try {
            const selectedUser = await this.knex("users as u").where(data).whereNull("deleted_at").first().select(this.userReturningFields).transacting(trx);
            return selectedUser;
        }
        catch (err) {
            return new error_1.Error({ message: "could not get user", cause: err });
        }
    }
    async selectUserWithPassword(trx, email) {
        try {
            const user = await this.knex("users as u").where('email', email).whereNull('deleted_at').first().select([...this.userReturningFields, 'u.password']).transacting(trx);
            return user;
        }
        catch (err) {
            return new error_1.Error({ message: "could not get user", cause: err });
        }
    }
    async selectUserByVerificationtoken(trx, token) {
        try {
            return this.knex("email_verification_token as vt").where({ token }).first().transacting(trx)
                .innerJoin("users as u", "vt.token", '=', "u.id")
                .select(this.userReturningFields).first();
        }
        catch (err) {
            return new error_1.Error({ message: 'an error occured while getting user', cause: err });
        }
    }
    async create(trx, dataToInsert, avatar) {
        try {
            const { websocket } = dataToInsert, user = __rest(dataToInsert, ["websocket"]);
            const newUser = await this.knex("users").insert(Object.assign(Object.assign({}, user), { avatar })).transacting(trx).returning("id");
            return newUser ? newUser[0] : undefined;
        }
        catch (err) {
            return new error_1.Error({ message: "could not create user", cause: err });
        }
    }
    async update(trx, id, dataToUpdate) {
        try {
            const updatedUser = await this.knex("users as u").where({ "id": id }).whereNull('deleted_at').update(dataToUpdate).transacting(trx).returning(this.userReturningFields);
            return updatedUser ? updatedUser[0] : undefined;
        }
        catch (err) {
            return new error_1.Error({ message: "could not update user", cause: err });
        }
    }
    async delete(trx, id) {
        try {
            const deletedUser = await this.knex("users as u").where("id", id).whereNull('deleted_at').update({}).returning(this.userReturningFields).transacting(trx);
            return deletedUser ? deletedUser[0] : undefined;
        }
        catch (err) {
            return new error_1.Error({ message: "could not delete user", cause: err });
        }
    }
    async createUserLogin(trx, data) {
        try {
            const newLogin = await this.knex("user_logins as ul").insert(data).transacting(trx).returning(this.userLoginsReturningFields);
            return newLogin ? newLogin[0] : undefined;
        }
        catch (err) {
            return new error_1.Error({ message: "could not create user login", cause: err });
        }
    }
    async deleteUserLogin(trx, lid) {
        try {
            const login = await this.knex("user_logins as ul").where({ 'id': lid }).delete().transacting(trx).returning("*");
            return login ? login[0] : undefined;
        }
        catch (err) {
            return new error_1.Error({ message: "could not delete user login", cause: err });
        }
    }
    async markUserLoginAsLogedOutBy(trx, by) {
        try {
            const login = await this.knex("user_logins as ul").where(by).update({ 'deleted_at': (0, moment_1.default)().format() }).transacting(trx).returning(this.userLoginsReturningFields);
            return login;
        }
        catch (err) {
            return new error_1.Error({ message: "could not logout", cause: err });
        }
    }
    async deleteUserLoginsBy(trx, by) {
        try {
            const login = await this.knex("user_logins as ul").where(by).delete().transacting(trx).returning("*");
            return login;
        }
        catch (err) {
            return new error_1.Error({ message: "could not delete user logins", cause: err });
        }
    }
    async updateUserLoginBy(trx, lid, by) {
        try {
            const login = await this.knex("user_logins as ul").where({ 'id': lid }).update(by).transacting(trx).returning("*");
            return login ? login[0] : undefined;
        }
        catch (err) {
            return new error_1.Error({ message: "could not update user login", cause: err });
        }
    }
    async selectUserLoginBy(trx, by) {
        try {
            const login = await this.knex("user_logins as ul").where(by).whereNull('ul.deleted_at').select(this.userLoginsReturningFields).first().transacting(trx);
            return login;
        }
        catch (err) {
            return new error_1.Error({ message: "could not get user login", cause: err });
        }
    }
    async selectUsersLoginsBy(trx, by) {
        try {
            const logins = await this.knex("user_logins as ul").where(by).whereNull('deleted_at').select(this.userLoginsReturningFields).transacting(trx);
            return logins;
        }
        catch (err) {
            return new error_1.Error({ message: "could not get user logins", cause: err });
        }
    }
    async createUserDevice(trx, data) {
        try {
            const newDevice = await this.knex("user_devices as ud").insert(data).transacting(trx).returning("*");
            return newDevice ? newDevice[0] : undefined;
        }
        catch (err) {
            return new error_1.Error({ message: "could not get users", cause: err });
        }
    }
    async deleteUserDevice(trx, did) {
        try {
            const deletedDevice = await this.knex("user_devices as ud").where({ 'id': did }).del().transacting(trx).returning("*");
            return deletedDevice ? deletedDevice[0] : undefined;
        }
        catch (err) {
            return new error_1.Error({ message: "could not delete user device", cause: err });
        }
    }
    async updateUserDevice(trx, did, updateData) {
        try {
            const updatedDevice = await this.knex("user_devices as ud").where({ 'id': did, }).update(Object.assign({}, updateData)).transacting(trx).returning("*");
            return updatedDevice ? updatedDevice[0] : undefined;
        }
        catch (err) {
            return new error_1.Error({ message: "could not update user device", cause: err });
        }
    }
    async selectUserDevicesBy(trx, by) {
        try {
            const userDevices = await this.knex("user_devices as ud").where({}).select(this.userDeviceReturningFields).transacting(trx);
            return userDevices;
        }
        catch (err) {
            return new error_1.Error({ message: "could not get user devices", cause: err });
        }
    }
    async selectUserDeviceBy(trx, by) {
        try {
            const userDevices = await this.knex("user_devices as ud").where({}).first().select(this.userDeviceReturningFields).transacting(trx);
            return userDevices;
        }
        catch (err) {
            return new error_1.Error({ message: "could not get user device", cause: err });
        }
    }
};
UserModel = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [])
], UserModel);
exports.UserModel = UserModel;
//# sourceMappingURL=user.model.js.map