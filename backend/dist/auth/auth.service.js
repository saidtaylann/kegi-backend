"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const common_1 = require("@nestjs/common");
const uuid_1 = require("uuid");
const encryption_1 = require("../utils/encryption");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const redis_1 = __importDefault(require("../services/redis"));
const user_model_1 = require("../user/user.model");
const random_1 = require("../utils/random");
const config_json_1 = __importDefault(require("../config.json"));
const auth_model_1 = require("./auth.model");
const error_1 = require("../types/error");
let AuthService = class AuthService {
    constructor(UserModel, authModel) {
        this.UserModel = UserModel;
        this.authModel = authModel;
        this.onlineUsers = redis_1.default.getUserModule();
    }
    async comparePasswords(password, hash) {
        const isSame = await (0, encryption_1.comparePasswords)(password, hash);
        return isSame;
    }
    createRefreshToken() {
        try {
            return (0, uuid_1.v4)();
        }
        catch (err) {
            return new error_1.Error({ message: 'could not create refresh token', cause: err });
        }
    }
    signJWT(payload, expire) {
        try {
            return jsonwebtoken_1.default.sign(payload, process.env.JWT_SECRET, { expiresIn: expire + "m" });
        }
        catch (err) {
            return new error_1.Error({ message: 'could not sign jwt', cause: err });
        }
    }
    decodeJWT(token) {
        try {
            return jsonwebtoken_1.default.decode(token);
        }
        catch (err) {
            return new error_1.Error({ message: 'could not decode jwt', cause: err });
        }
    }
    verifyJWT(token, config) {
        try {
            const decoded = jsonwebtoken_1.default.verify(token, process.env.JWT_SECRET, config);
            return decoded;
        }
        catch (err) {
            if (err.name === "TokenExpiredError")
                return new error_1.Error({ message: err.message, cause: err });
            else if (err.name === "JsonWebTokenError")
                return new error_1.Error({ message: err.message, cause: err });
        }
    }
    async login(trx, data, websocket, did, browser, platform) {
        let device;
        const user = await this.UserModel.selectUserWithPassword(trx, data.email);
        if (user instanceof error_1.Error) {
            user.cause = user.message;
            user.message = "an error ocured while logging in";
            user.code = 500;
            return user;
        }
        if (user) {
            const arePasswordsSame = await this.comparePasswords(data.password, user.password);
            if (arePasswordsSame instanceof error_1.Error) {
                arePasswordsSame.cause = arePasswordsSame.message;
                arePasswordsSame.message = "an error occured while logging in";
                arePasswordsSame.code = 500;
                return arePasswordsSame;
            }
            delete user.password;
            if (arePasswordsSame) {
                if (did) {
                    device = await this.UserModel.selectUserDeviceBy(trx, { id: did });
                    if (device instanceof error_1.Error) {
                        device.code = 500;
                        return device;
                    }
                    if (!device) {
                        device = await this.UserModel.createUserDevice(trx, { id: (0, random_1.genRandomString)(128), user_id: user.id, platform, browser });
                        if (device instanceof error_1.Error) {
                            device.code = 500;
                            return device;
                        }
                    }
                }
                else {
                    device = await this.UserModel.createUserDevice(trx, { id: (0, random_1.genRandomString)(128), user_id: user.id, platform, browser });
                    if (device instanceof error_1.Error) {
                        device.code = 500;
                        return device;
                    }
                }
                const signPayload = {
                    id: user.id,
                    role: user.role
                };
                const accessToken = this.signJWT(signPayload, config_json_1.default.AUTH.accessTokenExpirationMinutes);
                if (accessToken instanceof error_1.Error) {
                    accessToken.cause = accessToken.message;
                    accessToken.message = "an error occured creating access token";
                    accessToken.code = 500;
                    return accessToken;
                }
                const refreshToken = this.createRefreshToken();
                if (refreshToken instanceof error_1.Error) {
                    refreshToken.code = 500;
                    return refreshToken;
                }
                const delLogin = await this.UserModel.markUserLoginAsLogedOutBy(trx, { user_id: user.id, device_id: device.id });
                if (delLogin instanceof error_1.Error) {
                    delLogin.cause = delLogin.message;
                    delLogin.message = "an error occured while logging out other logs";
                    delLogin.code = 500;
                    return delLogin;
                }
                const loginedUser = await this.UserModel.createUserLogin(trx, {
                    id: (0, random_1.genRandomString)(128),
                    user_id: user.id,
                    refresh_token: refreshToken,
                    device_id: device.id,
                });
                if (loginedUser instanceof error_1.Error) {
                    loginedUser.code = 500;
                    return loginedUser;
                }
                if (loginedUser) {
                    this.onlineUsers.set(websocket, user.id);
                    return {
                        user,
                        DID: device.id,
                        LID: loginedUser.id,
                        accessToken
                    };
                }
            }
        }
        return new error_1.Error({ message: 'username and password are wrong', code: 401 });
    }
    async logout(trx, lid, websocket) {
        const logout = await this.UserModel.markUserLoginAsLogedOutBy(trx, { id: lid });
        if (logout instanceof error_1.Error) {
            logout.code = 500;
            return logout;
        }
        this.onlineUsers.del(websocket);
        return logout.length !== 0 ? { success: true } : { success: false };
    }
    async logoutFromAllDevices(trx, uid, websocket) {
        const logsout = await this.UserModel.markUserLoginAsLogedOutBy(trx, { user_id: uid });
        if (logsout instanceof error_1.Error) {
            logsout.code = 500;
            return logsout;
        }
        this.onlineUsers.del(websocket);
        return logsout.length !== 0 ? { success: true } : { success: false };
    }
    async refresh(trx, uid, urole, lid) {
        const login = (await this.UserModel.markUserLoginAsLogedOutBy(trx, { id: lid }))[0];
        if (login instanceof error_1.Error) {
            login.cause = login.message;
            login.message = "an error occured while getting new token";
            login.code = 500;
            return login;
        }
        if (login) {
            const currentDate = new Date();
            if (currentDate.setDate(login.updated_at.getDate() + config_json_1.default.AUTH.refreshTokenExirationDays) < new Date().getTime()) {
                const deleted = await this.UserModel.deleteUserLogin(trx, lid);
                if (deleted instanceof error_1.Error) {
                    deleted.cause = deleted.message;
                    deleted.message = "an error occured while getting new token";
                    deleted.code = 500;
                    return deleted;
                }
            }
            else {
                const signPayload = {
                    id: uid,
                    role: urole
                };
                const accessToken = this.signJWT(signPayload, config_json_1.default.AUTH.accessTokenExpirationMinutes);
                if (accessToken instanceof error_1.Error) {
                    accessToken.cause = accessToken.message;
                    accessToken.message = "an error occured while getting new token";
                    accessToken.code = 500;
                    return accessToken;
                }
                const newRefreshToken = this.createRefreshToken();
                if (newRefreshToken instanceof error_1.Error) {
                    newRefreshToken.cause = newRefreshToken.message;
                    newRefreshToken.message = "an error occured while getting new token";
                    newRefreshToken.code = 500;
                    return newRefreshToken;
                }
                const updatedLogin = await this.UserModel.updateUserLoginBy(trx, login.id, { refresh_token: newRefreshToken });
                if (updatedLogin instanceof error_1.Error) {
                    updatedLogin.cause = updatedLogin.message;
                    updatedLogin.message = "an error occured while getting new token";
                    updatedLogin.code = 500;
                    return updatedLogin;
                }
                return accessToken;
            }
        }
    }
    async verifyEmail(trx, token) {
        const isTokenExist = await this.authModel.selectVerificationTokenBy(trx, { token });
        if (isTokenExist instanceof error_1.Error) {
            isTokenExist.cause = isTokenExist.message;
            isTokenExist.message = "an error occured while verifying email";
            isTokenExist.code = 500;
            return isTokenExist;
        }
        if (isTokenExist) {
            const currentDate = new Date();
            if (currentDate.setDate(isTokenExist.created_at.getHours() + config_json_1.default.AUTH.verificationTokenExpirationHours) < new Date().getTime()) {
                return new error_1.Error({ message: 'TokenExpired', code: 401 });
            }
            const verified = await this.UserModel.update(trx, isTokenExist.user_id, { verified: true });
            if (verified instanceof error_1.Error) {
                verified.cause = verified.message;
                verified.message = "an error occured while verifying email";
                verified.code = 500;
                return verified;
            }
            const deleted = await this.authModel.deleteVerificationToken(trx, isTokenExist.user_id);
            if (deleted instanceof error_1.Error) {
                deleted.cause = deleted.message;
                deleted.message = "an error occured while verifying email";
                deleted.code = 500;
                return deleted;
            }
            return deleted ? true : false;
        }
        return new error_1.Error({ message: 'InvalidToken', code: 404 });
    }
    async addEmailVerificationCode(trx, uid) {
        const createdToken = await this.authModel.createVerificationToken(trx, uid);
        if (createdToken instanceof error_1.Error) {
            createdToken.code = 500;
            return createdToken;
        }
        else if (createdToken)
            return createdToken;
        return new error_1.Error({ message: 'verification code could not created', code: 500 });
    }
    async resendVerificationToken(trx, uid) {
        const newToken = await this.authModel.updateVerificationToken(trx, uid);
        if (newToken instanceof error_1.Error) {
            newToken.cause = newToken.message;
            newToken.message = "an error occured while resending new verification token";
            newToken.code = 500;
        }
        return newToken;
    }
};
AuthService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)((0, common_1.forwardRef)(() => user_model_1.UserModel))),
    __metadata("design:paramtypes", [user_model_1.UserModel, auth_model_1.AuthModel])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map