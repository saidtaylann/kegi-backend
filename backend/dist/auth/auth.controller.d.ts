import { AuthService } from './auth.service';
import { ExtReq } from 'src/types/common';
import { Response } from "express";
import { UserService } from 'src/user/user.service';
export declare class AuthController {
    private readonly authService;
    private readonly userService;
    private knex;
    constructor(authService: AuthService, userService: UserService);
    handleLogin(res: Response, req: ExtReq, body: {
        email: string;
        password: string;
        websocket: string;
    }): Promise<{
        user: any;
        DID: any;
        LID: any;
        accessToken: any;
    }>;
    handleLogout(body: {
        websocket: string;
    }, req: ExtReq, res: Response): Promise<{
        success: boolean;
    }>;
    handleLogoutFromAllDevices(body: {
        websocket: string;
    }, req: ExtReq, res: Response): Promise<{
        success: boolean;
    }>;
    handleRefreshToken(req: ExtReq, res: Response): Promise<{
        accessToken: string;
    }>;
    handleVerifyEmail(token: string): Promise<{
        success: boolean;
    }>;
    handleRefreshVerificationToken(req: ExtReq): Promise<{
        success: boolean;
    }>;
}
