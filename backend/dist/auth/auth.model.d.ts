import type { Knex } from "knex";
export declare class AuthModel {
    private knex;
    private verificationTokenFields;
    constructor();
    createVerificationToken(trx: Knex.Transaction, uid: number): Promise<any>;
    selectVerificationTokenBy(trx: Knex.Transaction, by: {
        user_id?: number;
        token?: string;
    }): Promise<any>;
    deleteVerificationToken(trx: Knex.Transaction, user_id: number): Promise<any>;
    updateVerificationToken(trx: Knex.Transaction, uid: number): Promise<any>;
}
