"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthModel = void 0;
const common_1 = require("@nestjs/common");
const moment_1 = __importDefault(require("moment"));
const database_1 = __importDefault(require("../database"));
const error_1 = require("../types/error");
const random_1 = require("../utils/random");
let AuthModel = class AuthModel {
    constructor() {
        this.knex = database_1.default.connection;
        this.verificationTokenFields = ['vt.user_id', 'vt.token', 'vt.created_at'];
    }
    async createVerificationToken(trx, uid) {
        try {
            const verificationToken = (0, random_1.genRandomShortCode)(6);
            const createdRows = await this.knex('email_verification_tokens as vt').where({ 'user_id': uid })
                .insert({ token: verificationToken, 'user_id': uid }).transacting(trx).onConflict("token").ignore();
            if (createdRows.rowCount === 0)
                await this.createVerificationToken(trx, uid);
            else
                return verificationToken;
            return createdRows ? createdRows[0] : undefined;
        }
        catch (err) {
            return new error_1.Error({ message: 'verification token could not updated', cause: err });
        }
    }
    async selectVerificationTokenBy(trx, by) {
        try {
            const incomingToken = await this.knex('email_verification_tokens as vt').where(by).first().transacting(trx).select(this.verificationTokenFields);
            return incomingToken;
        }
        catch (err) {
            return new error_1.Error({ message: 'verification token could not get', cause: err });
        }
    }
    async deleteVerificationToken(trx, user_id) {
        try {
            const deletedToken = await this.knex('email_verification_tokens as vt').where({ user_id }).del().transacting(trx).returning("*");
            return deletedToken ? deletedToken[0] : undefined;
        }
        catch (err) {
            return new error_1.Error({ message: 'verification token could not delete', cause: err });
        }
    }
    async updateVerificationToken(trx, uid) {
        try {
            const verificationToken = (0, random_1.genRandomShortCode)(6);
            const updatedToken = await this.knex('email_verification_tokens as vt').where({ 'vt.user_id': uid })
                .update({ token: verificationToken, created_at: (0, moment_1.default)().format() }).transacting(trx).onConflict("token").ignore();
            if (updatedToken.rowCount === 0)
                await this.updateVerificationToken(trx, uid);
            else
                return verificationToken;
            return updatedToken ? updatedToken[0] : undefined;
        }
        catch (err) {
            return new error_1.Error({ message: 'verification token could not updated', cause: err });
        }
    }
};
AuthModel = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [])
], AuthModel);
exports.AuthModel = AuthModel;
//# sourceMappingURL=auth.model.js.map