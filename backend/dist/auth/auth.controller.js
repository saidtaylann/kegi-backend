"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthController = void 0;
const common_1 = require("@nestjs/common");
const auth_service_1 = require("./auth.service");
const database_1 = __importDefault(require("../database"));
const error_1 = require("../types/error");
const sqs_1 = require("../services/aws/sqs");
const user_service_1 = require("../user/user.service");
const config_json_1 = __importDefault(require("../config.json"));
let AuthController = class AuthController {
    constructor(authService, userService) {
        this.authService = authService;
        this.userService = userService;
        this.knex = database_1.default.connection;
    }
    async handleLogin(res, req, body) {
        const DID = req.get("DID");
        return this.knex.transaction(async (trx) => {
            const loginedUser = await this.authService.login(trx, Object.assign({}, body), body.websocket, DID, req.useragent.browser, req.useragent.platform);
            if (loginedUser instanceof error_1.Error) {
                throw new common_1.HttpException(loginedUser.message, loginedUser.code);
            }
            return { user: loginedUser.user, DID: loginedUser.DID, LID: loginedUser.LID, accessToken: loginedUser.accessToken };
        });
    }
    async handleLogout(body, req, res) {
        const LID = req.get("LID");
        return this.knex.transaction(async (trx) => {
            const login = await this.authService.logout(trx, LID, body.websocket);
            if (login instanceof error_1.Error) {
                throw new common_1.HttpException(login.message, login.code);
            }
            return login ? { success: true } : { success: false };
        });
    }
    async handleLogoutFromAllDevices(body, req, res) {
        return this.knex.transaction(async (trx) => {
            const login = await this.authService.logoutFromAllDevices(trx, req.user.id, body.websocket);
            if (login instanceof error_1.Error) {
                throw new common_1.HttpException(login.message, login.code);
            }
            return login ? { success: true } : { success: false };
        });
    }
    async handleRefreshToken(req, res) {
        var _a;
        const LID = req.get("LID");
        const accessToken = (_a = req.get('authorization')) === null || _a === void 0 ? void 0 : _a.split("Bearer ");
        let payload;
        if (LID && accessToken.length === 2) {
            payload = this.authService.verifyJWT(accessToken[1], { ignoreExpiration: true });
            return this.knex.transaction(async (trx) => {
                const newAccessToken = await this.authService.refresh(trx, payload.id, payload.role, LID);
                if (newAccessToken instanceof error_1.Error) {
                    throw new common_1.HttpException(newAccessToken.message, newAccessToken.code);
                }
                return newAccessToken ? { accessToken: newAccessToken } : { accessToken: '' };
            });
        }
        if (payload instanceof error_1.Error) {
            throw new common_1.HttpException(payload.message, payload.code);
        }
    }
    async handleVerifyEmail(token) {
        return this.knex.transaction(async (trx) => {
            const verified = await this.authService.verifyEmail(trx, token);
            if (verified instanceof error_1.Error) {
                throw new common_1.HttpException(verified.message, verified.code);
            }
            return verified ? { success: true } : { success: false };
        });
    }
    async handleRefreshVerificationToken(req) {
        return this.knex.transaction(async (trx) => {
            const user = await this.userService.getUserById(trx, req.user.id);
            if (user instanceof error_1.Error) {
                throw new common_1.HttpException(user.message, 500);
            }
            else if (user) {
                const verified = await this.authService.resendVerificationToken(trx, req.user.id);
                if (verified instanceof error_1.Error) {
                    throw new common_1.HttpException(verified.message, verified.code);
                }
                else if (verified) {
                    const tmplData = { name: user.name, lastname: user.lastname, verificationToken: verified, verificationLink: `${config_json_1.default.SERVER.allowedDomain}/verify/${verified}` };
                    const data = { to: [user.email], subject: "Kegi Mail Doğrulama", templateData: tmplData };
                    (0, sqs_1.sendEmail)("re-verification", data);
                    return { success: true };
                }
                return { success: false };
            }
        });
    }
};
__decorate([
    (0, common_1.Post)("login"),
    __param(0, (0, common_1.Res)({ passthrough: true })),
    __param(1, (0, common_1.Req)()),
    __param(2, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "handleLogin", null);
__decorate([
    (0, common_1.Post)("logout"),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Req)()),
    __param(2, (0, common_1.Res)({ passthrough: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "handleLogout", null);
__decorate([
    (0, common_1.Post)("logout-all"),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Req)()),
    __param(2, (0, common_1.Res)({ passthrough: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "handleLogoutFromAllDevices", null);
__decorate([
    (0, common_1.Post)("refresh"),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Res)({ passthrough: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "handleRefreshToken", null);
__decorate([
    (0, common_1.Post)("verify-email/:token"),
    __param(0, (0, common_1.Param)('token')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "handleVerifyEmail", null);
__decorate([
    (0, common_1.Post)("refresh-verification"),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "handleRefreshVerificationToken", null);
AuthController = __decorate([
    (0, common_1.Controller)('auth'),
    __param(1, (0, common_1.Inject)((0, common_1.forwardRef)(() => user_service_1.UserService))),
    __metadata("design:paramtypes", [auth_service_1.AuthService, user_service_1.UserService])
], AuthController);
exports.AuthController = AuthController;
//# sourceMappingURL=auth.controller.js.map