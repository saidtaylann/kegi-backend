import { IJWT } from 'src/types/auth';
import jwt from "jsonwebtoken";
import { Knex } from 'knex';
import { UserModel } from 'src/user/user.model';
import { IUser } from 'src/types/user';
import { AuthModel } from './auth.model';
import { Error as Err } from 'src/types/error';
export declare class AuthService {
    private readonly UserModel;
    private readonly authModel;
    private readonly onlineUsers;
    constructor(UserModel: UserModel, authModel: AuthModel);
    comparePasswords(password: string, hash: string): Promise<boolean | Err>;
    createRefreshToken(): string | Err;
    signJWT(payload: IJWT, expire: string): string | Err;
    decodeJWT(token: string): string | Err | jwt.JwtPayload;
    verifyJWT(token: string, config?: {
        ignoreExpiration?: boolean;
    }): IJWT | Err;
    login(trx: Knex.Transaction, data: {
        email: string;
        password: string;
    }, websocket: string, did: string, browser: string, platform: string): Promise<Err | {
        user: IUser;
        DID: string;
        LID: string;
        accessToken: string;
    }>;
    logout(trx: Knex.Transaction, lid: string, websocket: string): Promise<Err | {
        success: boolean;
    }>;
    logoutFromAllDevices(trx: Knex.Transaction, uid: number, websocket: string): Promise<{
        success: boolean;
    } | Err>;
    refresh(trx: Knex.Transaction, uid: number, urole: number, lid: string): Promise<string | Err>;
    verifyEmail(trx: Knex.Transaction, token: string): Promise<boolean | Err>;
    addEmailVerificationCode(trx: Knex.Transaction, uid: number): Promise<any>;
    resendVerificationToken(trx: Knex.Transaction, uid: number): Promise<any>;
}
