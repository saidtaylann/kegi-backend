"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const swagger_1 = require("./services/swagger");
const services_1 = require("./services");
const cookie_parser_1 = __importDefault(require("cookie-parser"));
const config_json_1 = __importDefault(require("./config.json"));
const express_useragent_1 = __importDefault(require("express-useragent"));
const cluster_1 = __importDefault(require("./utils/cluster"));
async function bootstrap(APP_PORT) {
    console.log('APP_PORT', APP_PORT);
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    (0, swagger_1.swaggerInit)(app);
    app.enableCors({
        origin: config_json_1.default.SERVER.allowedDomain,
        methods: ['GET', 'OPTIONS', 'PUT', 'POST', 'DELETE'],
        credentials: true,
        allowedHeaders: ['content-type', 'lid', 'Authorization', 'authorization', 'withcredentials']
    });
    app.use((0, cookie_parser_1.default)());
    app.use(express_useragent_1.default.express());
    await (0, services_1.initServices)();
    await app.listen(APP_PORT);
}
(0, cluster_1.default)(bootstrap, 3000);
//# sourceMappingURL=main.js.map