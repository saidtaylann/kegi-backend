"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Error = void 0;
class Error {
    constructor(params) {
        this.name = params.name;
        this.message = params.message;
        this.cause = params.cause;
        this.code = params.code;
    }
}
exports.Error = Error;
//# sourceMappingURL=error.js.map