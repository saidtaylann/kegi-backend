export declare class Error {
    name?: string;
    message: string;
    cause?: any;
    code?: number;
    constructor(params: {
        message: string;
        cause?: any;
        code?: number;
        name?: string;
    });
}
