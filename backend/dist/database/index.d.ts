declare class Database {
    static readonly connection: import("knex").Knex<any, unknown[]>;
}
export default Database;
