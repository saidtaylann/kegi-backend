"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const knex_1 = __importDefault(require("knex"));
class Database {
}
Database.connection = (0, knex_1.default)({
    client: 'pg',
    connection: {
        host: "localhost",
        port: 5432,
        user: "admin",
        password: "password",
        database: "kegiNestDB",
        timezone: "+00:00"
    },
    searchPath: ['public', 'knex'],
});
exports.default = Database;
//# sourceMappingURL=index.js.map