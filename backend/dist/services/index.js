"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.initServices = void 0;
const redis_1 = __importDefault(require("./redis"));
const database_1 = __importDefault(require("../database"));
const initServices = async () => {
    try {
        new redis_1.default();
        new database_1.default();
    }
    catch (err) {
        console.log(err);
    }
};
exports.initServices = initServices;
//# sourceMappingURL=index.js.map