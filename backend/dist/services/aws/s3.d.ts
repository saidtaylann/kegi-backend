/// <reference types="express-serve-static-core" />
/// <reference types="cookie-parser" />
/// <reference types="express-useragent" />
/// <reference types="csurf" />
/// <reference types="express-fileupload" />
/// <reference types="multer" />
/// <reference types="passport" />
import { S3 } from 'aws-sdk';
import { Error as Err } from 'src/types/error';
export declare const uploadS3File: (file: Express.Multer.File, fileName: string, folder: string) => Promise<Err | S3.ManagedUpload.SendData>;
export declare const updateS3File: (file: Express.Multer.File, fileName: string, folder: string) => Promise<Err | import("aws-sdk/lib/request").PromiseResult<S3.PutObjectOutput, import("aws-sdk").AWSError>>;
export declare const deleteS3File: (fileName: string, folder: string) => Promise<Err | import("aws-sdk/lib/request").PromiseResult<S3.DeleteObjectOutput, import("aws-sdk").AWSError>>;
export declare const getFile: (fileName: string, folder: string) => Promise<Err | import("aws-sdk/lib/request").PromiseResult<S3.GetObjectOutput, import("aws-sdk").AWSError>>;
