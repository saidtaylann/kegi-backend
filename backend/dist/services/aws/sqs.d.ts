import { IMailDataToSend } from "src/types/services/aws/sqs";
type mailTypes = "verification" | "re-verification" | "forgottenPassword";
export declare const sendEmail: (type: mailTypes, data: IMailDataToSend) => Promise<void>;
export {};
