"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getFile = exports.deleteS3File = exports.updateS3File = exports.uploadS3File = void 0;
const aws_sdk_1 = require("aws-sdk");
const error_1 = require("../../types/error");
const uploadS3File = async (file, fileName, folder) => {
    aws_sdk_1.config.update({
        region: 'eu-central-1'
    });
    const s3 = new aws_sdk_1.S3({
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_ACCESS_SECRET_KEY
    });
    const params = {
        Bucket: process.env.AWS_S3_BUCKET_NAME,
        Key: folder + fileName,
        Body: file.buffer
    };
    try {
        return s3.upload(params).promise();
    }
    catch (err) {
        return new error_1.Error({ message: "an error occured while uploading the file", cause: err });
    }
};
exports.uploadS3File = uploadS3File;
const updateS3File = async (file, fileName, folder) => {
    aws_sdk_1.config.update({
        region: 'eu-central-1'
    });
    const s3 = new aws_sdk_1.S3({
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_ACCESS_SECRET_KEY
    });
    const params = {
        Bucket: process.env.AWS_S3_BUCKET_NAME,
        Key: folder + fileName,
        Body: file.buffer
    };
    try {
        return s3.putObject(params).promise();
    }
    catch (err) {
        return new error_1.Error({ message: "an error occured while uploading the file", cause: err });
    }
};
exports.updateS3File = updateS3File;
const deleteS3File = async (fileName, folder) => {
    aws_sdk_1.config.update({
        region: 'eu-central-1'
    });
    const s3 = new aws_sdk_1.S3({
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_ACCESS_SECRET_KEY
    });
    const params = {
        Bucket: process.env.AWS_S3_BUCKET_NAME,
        Key: folder + fileName,
    };
    try {
        return s3.deleteObject(params).promise();
    }
    catch (err) {
        return new error_1.Error({ message: "an error occured while uploading the file", cause: err });
    }
};
exports.deleteS3File = deleteS3File;
const getFile = async (fileName, folder) => {
    aws_sdk_1.config.update({
        region: 'eu-central-1'
    });
    const s3 = new aws_sdk_1.S3({
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_ACCESS_SECRET_KEY
    });
    const params = {
        Bucket: process.env.AWS_S3_BUCKET_NAME,
        Key: folder + fileName,
    };
    try {
        return s3.getObject(params).promise();
    }
    catch (err) {
        return new error_1.Error({ message: "an error occured while uploading the file", cause: err });
    }
};
exports.getFile = getFile;
//# sourceMappingURL=s3.js.map