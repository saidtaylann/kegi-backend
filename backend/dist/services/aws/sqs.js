"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sendEmail = void 0;
const aws_sdk_1 = require("aws-sdk");
const sendToQ = async (qUrl, messageBody, messageAttrs) => {
    aws_sdk_1.config.update({ region: 'REGION' });
    const sqs = new aws_sdk_1.SQS({ apiVersion: '2012-11-05' });
    const params = Object.assign(Object.assign({}, (messageAttrs && { MessageAttributes: messageAttrs })), { MessageBody: JSON.stringify(messageBody), QueueUrl: qUrl });
    try {
        await sqs.sendMessage(params).promise();
    }
    catch (err) {
        console.log('err', err);
        return err;
    }
};
const sendEmail = async (type, data) => {
    sendToQ(process.env.AWS_SQS_MAILQ_URL, data, {
        type: { DataType: "String", StringValue: type }
    });
};
exports.sendEmail = sendEmail;
//# sourceMappingURL=sqs.js.map