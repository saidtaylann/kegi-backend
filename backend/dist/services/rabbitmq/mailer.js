"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sendMail = exports.mailConfig = void 0;
const QInfo = {
    channel: null,
    queues: ["verification"],
    qPrefix: "mail",
    exchange: "mailer",
    exchangeType: "direct"
};
const mailConfig = async (connection) => {
    QInfo.channel = await connection.createChannel();
    QInfo.channel.assertExchange(QInfo.exchange, QInfo.exchangeType, {
        durable: true,
        autoDelete: false,
        internal: false,
    });
    QInfo.queues.forEach(async (q) => {
        await QInfo.channel.assertQueue(`${QInfo.qPrefix}.${q}`, {
            durable: true,
            autoDelete: false,
            exclusive: false,
        });
        await QInfo.channel.bindQueue(`${QInfo.qPrefix}.${q}`, QInfo.exchange, q);
    });
};
exports.mailConfig = mailConfig;
const sendMail = async (mailType, data) => {
    const pubOk = QInfo.channel.publish(QInfo.exchange, mailType, Buffer.from(JSON.stringify(data)), {
        persistent: true,
        contentType: "text/plain",
    });
    if (!pubOk) {
        throw "verification mail could not send";
    }
};
exports.sendMail = sendMail;
//# sourceMappingURL=mailer.js.map