import amqp from "amqplib";
import { IMailDataToSend } from "src/types/services/rabbitmq";
export declare const mailConfig: (connection: amqp.Connection) => Promise<void>;
export declare const sendMail: (mailType: string, data: IMailDataToSend) => Promise<void>;
