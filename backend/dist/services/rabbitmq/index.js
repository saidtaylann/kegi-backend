"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.initRabbitMQ = void 0;
const amqplib_1 = __importDefault(require("amqplib"));
const mailer_1 = require("./mailer");
const initRabbitMQ = async () => {
    const connection = await amqplib_1.default.connect(process.env.RABBITMQ_ADDR);
    await (0, mailer_1.mailConfig)(connection);
};
exports.initRabbitMQ = initRabbitMQ;
//# sourceMappingURL=index.js.map