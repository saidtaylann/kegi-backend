"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OnlineUser = void 0;
const error_1 = require("../../types/error");
class OnlineUser {
    constructor(redis) {
        this.redis = redis;
        this.groupName = "user";
        this.redis = redis;
    }
    async set(websocket, uid) {
        try {
            return await this.redis.set(`${this.groupName}:${websocket}`, uid);
        }
        catch (err) {
            return new error_1.Error({ message: 'an error occured while setting online user ', cause: err });
        }
    }
    async get(websocket) {
        try {
            return this.redis.get(`${this.groupName}:${websocket}`);
        }
        catch (err) {
            return new error_1.Error({ message: 'an error occured while getting online user ', cause: err });
        }
    }
    async del(websocket) {
        try {
            return this.redis.del(`${this.groupName}:${websocket}`);
        }
        catch (err) {
            return new error_1.Error({ message: 'an error occured while deleting online user ', cause: err });
        }
    }
}
exports.OnlineUser = OnlineUser;
//# sourceMappingURL=users.js.map