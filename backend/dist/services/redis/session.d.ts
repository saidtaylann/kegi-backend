import Redis from "ioredis";
import type { JoinSessionDto } from "src/ws/dto/room.dto";
export declare class RedisRoomSession {
    private readonly conn;
    constructor(conn: Redis);
    setParticipant(body: JoinSessionDto): Promise<number>;
    deleteParticipant(sessionId: number, participantWebsocketId: string): Promise<number>;
    getParticipant(sessionId: number, participantWebsocketId: string): Promise<Record<string, string>>;
    updateParticipant(sessionId: number, participantWebsocketId: string, field: string, value: any): Promise<"OK">;
    deleteParticipantField(sessionId: number, participantWebsocketId: string, field: string): Promise<number>;
    setSession(sessionId: number, session: any): Promise<void>;
    deleteSession(sessionId: number): Promise<void>;
    getSession(sessionId: number): Promise<Record<string, string>>;
    updateSession(sessionId: number, field: string, value: any): Promise<"OK">;
}
