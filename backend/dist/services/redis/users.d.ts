import type Redis from "ioredis";
import { Error as Err } from "src/types/error";
export declare class OnlineUser {
    private readonly redis;
    private readonly groupName;
    constructor(redis: Redis);
    set(websocket: string, uid: "" | number): Promise<Err | "OK">;
    get(websocket: string): Promise<string | Err>;
    del(websocket: string): Promise<number | Err>;
}
