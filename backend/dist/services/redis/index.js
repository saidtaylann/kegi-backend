"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const ioredis_1 = __importDefault(require("ioredis"));
const users_1 = require("./users");
const session_1 = require("./session");
class Redis {
    static getUserModule() {
        return this.users;
    }
    static getSessionModule() {
        return this.sessions;
    }
    static multi() {
        return this.conn.multi();
    }
}
_a = Redis;
Redis.conn = new ioredis_1.default(process.env.REDIS_ADDR, {
    username: "default",
    password: "my-top-secret",
    db: 0
});
Redis.users = new users_1.OnlineUser(_a.conn);
Redis.sessions = new session_1.RedisRoomSession(_a.conn);
exports.default = Redis;
//# sourceMappingURL=index.js.map