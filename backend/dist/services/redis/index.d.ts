import redis from "ioredis";
import { OnlineUser } from "./users";
import { RedisRoomSession } from "./session";
declare class Redis {
    static conn: redis;
    static users: OnlineUser;
    static sessions: RedisRoomSession;
    static getUserModule(): OnlineUser;
    static getSessionModule(): RedisRoomSession;
    static multi(): import("ioredis").ChainableCommander;
}
export default Redis;
