"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RedisRoomSession = void 0;
class RedisRoomSession {
    constructor(conn) {
        this.conn = conn;
    }
    async setParticipant(body) {
        return this.conn.hset(`session:${body.sessionId}:participantStreamID:${body.websocketId}`, body.participant);
    }
    async deleteParticipant(sessionId, participantWebsocketId) {
        return this.conn.del(`session:${sessionId}:participantStreamID:${participantWebsocketId}`);
    }
    async getParticipant(sessionId, participantWebsocketId) {
        return this.conn.hgetall(`session:${sessionId}:participantStreamID:${participantWebsocketId}`);
    }
    async updateParticipant(sessionId, participantWebsocketId, field, value) {
        return this.conn.hmset(`session:${sessionId}:participantStreamID:${participantWebsocketId}`, [field, value]);
    }
    async deleteParticipantField(sessionId, participantWebsocketId, field) {
        return this.conn.hdel(`session:${sessionId}:participantStreamID:${participantWebsocketId}`, field);
    }
    async setSession(sessionId, session) {
        await this.conn.hset(`session:${sessionId}`, session);
    }
    async deleteSession(sessionId) {
        await this.conn.del(`session:${sessionId}`);
    }
    async getSession(sessionId) {
        return this.conn.hgetall(`session:${sessionId}`);
    }
    async updateSession(sessionId, field, value) {
        return this.conn.hmset(`session:${sessionId}`, [field, value]);
    }
}
exports.RedisRoomSession = RedisRoomSession;
//# sourceMappingURL=session.js.map