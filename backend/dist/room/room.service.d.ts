import { RoomModel } from './room.model';
import type { Knex } from 'knex';
import { IParticipant, IRoom, IRoomDetail, IRoomSession, IRoomSessionDetail } from 'src/types/room';
import { CreateRoomDto, UpdateRoomDto } from "src/room/dto/room.dto";
import { UserService } from 'src/user/user.service';
import { Error as Err } from 'src/types/error';
import { Socket, Server } from 'socket.io';
import type { JoinSessionDto, StartSessionDto } from 'src/ws/dto/room.dto';
import { ExtSocket } from 'src/types/common';
export declare class RoomService {
    private readonly roomModel;
    private readonly userService;
    private redisSessions;
    private readonly knex;
    private readonly redis;
    private readonly roomImageDir;
    constructor(roomModel: RoomModel, userService: UserService);
    getRoomsBy(trx: Knex.Transaction, by: {}): Promise<IRoom[] | Err>;
    getRoomsUserMembered(trx: Knex.Transaction, uid: number): Promise<Err | IRoom[]>;
    getRoomByCode(trx: Knex.Transaction, code: string): Promise<IRoom | Err>;
    getRoomDetail(trx: Knex.Transaction, code: string): Promise<any>;
    createRoom(trx: Knex.Transaction, data: CreateRoomDto & {
        owner: number;
    }): Promise<string | Err>;
    updateRoom(trx: Knex.Transaction, code: string, dataToUpdate: UpdateRoomDto): Promise<IRoom | Err>;
    updateImage(trx: Knex.Transaction, room_code: string, image: any): Promise<true | Err>;
    deleteRoom(trx: Knex.Transaction, room_code: string): Promise<IRoom | Err>;
    getRoomSessions(trx: Knex.Transaction, room_code: string): Promise<Err | IRoomSession[]>;
    getRoomSessionDetail(trx: Knex.Transaction, session_id: number): Promise<any>;
    setRoomImageName(room_code: string, imageName: string): string;
    parseRoomDetail(room: Array<IRoomDetail>): any;
    parseRoomSessionDetail(room: Array<IRoomSessionDetail>): any;
    hasPermission(trx: Knex.Transaction, roomCodeOrSessionId: string | number, uid: number, roles: number[], forMember: boolean): Promise<Err | {
        role: number;
    }>;
    createSession(socket: Socket, room_code: string, uid: number, sessionPreferences: StartSessionDto): Promise<Err>;
    requestToJoinSession(socket: Socket, room_code: string, passcode?: string, uid?: number, display_name?: string): Promise<Err>;
    joinSession(socket: Socket, incomingData: JoinSessionDto): Promise<Err>;
    addParticipant(trx: Knex.Transaction, room_session_id: number, role: number, is_member?: boolean, websocket_id?: string, uid?: number, display_name?: string): Promise<Err | IParticipant>;
    removeParticipant(trx: Knex.Transaction, participant_id: number, sessionId: number, socketId: string, eventName: string): Promise<string | Err>;
    openRoom(socket: Socket, room_code: string, uid: number): Promise<any>;
    _endSession(trx: Knex.Transaction, sessionId: number, roomCode: string, eventName: string): Promise<any>;
    mute(server: Server, body: {
        socketId: string[];
        roomSessionId: number;
    }): Promise<void>;
    unmute(server: Server, body: {
        socketId: string[];
        roomSessionId: number;
    }): Promise<void>;
    openCam(server: Server, body: {
        socketId: string[];
        roomSessionId: number;
    }): Promise<void>;
    closeCam(server: Server, body: {
        socketId: string[];
        roomSessionId: number;
    }): Promise<void>;
    shareScreen(server: Server, body: {
        socketId: string;
        roomSessionId: number;
        streamId: string;
    }): Promise<void>;
    stopSharingScreen(server: Server, body: {
        socketId: string;
        roomSessionId: number;
    }): Promise<void>;
    leaveSession(server: Server, socket: ExtSocket, body: {
        participantId: number;
        socketId: string;
        roomSessionId: number;
        isClosingSession: boolean;
    }, userId?: number): Promise<boolean | Err>;
    endSession(server: Server, sessionId: number): Promise<Err>;
    switchMicAuthorization(server: Server, body: {
        roomSessionId: number;
        socketId: string[];
        isMicAllowed: boolean;
        all: boolean;
    }): Promise<void>;
    switchCamAuthorization(server: Server, body: {
        roomSessionId: number;
        socketId: string[];
        isCamAllowed: boolean;
        all: boolean;
    }): Promise<void>;
    switchSharingScreenAuthorization(server: Server, body: {
        roomSessionId: number;
        socketId: string[];
        isSharingScreenAllowed: boolean;
        all: boolean;
    }): Promise<void>;
    changeParticipantRole(server: Server, body: {
        roomSessionId: number;
        participantId: number[];
        newRole: number;
    }): Promise<void>;
}
