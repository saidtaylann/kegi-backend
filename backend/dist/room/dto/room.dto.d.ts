export declare class CreateRoomDto {
    passcode?: string;
    is_edu: boolean;
    title?: string;
    is_temp: boolean;
}
export declare class UpdateRoomDto {
    passcode?: string;
    isEdu?: boolean;
    title?: string;
    is_temp: boolean;
}
export declare class AddParticipantDto {
    user_id?: number;
    display_name?: string;
    room_session_id: number;
    role: number;
    is_member?: boolean;
    websocket_id?: string;
}
export declare class UpdateParticipantDto {
    role: number;
    is_member?: boolean;
}
export declare class CreateMemberDto {
    user_id: number;
    role: number;
    room_code: string;
}
export declare class UpdateMemberDto {
    role?: number;
}
