"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoomController = void 0;
const common_1 = require("@nestjs/common");
const room_service_1 = require("./room.service");
const database_1 = __importDefault(require("../database"));
const room_dto_1 = require("./dto/room.dto");
const platform_express_1 = require("@nestjs/platform-express");
const error_1 = require("../types/error");
const room_model_1 = require("./room.model");
const roles_enum_1 = require("../enums/roles.enum");
let RoomController = class RoomController {
    constructor(roomService, roomModel) {
        this.roomService = roomService;
        this.roomModel = roomModel;
        this.knex = database_1.default.connection;
    }
    async handleGetRoomsYouMembered(req) {
        return this.knex.transaction(async (trx) => {
            const rooms = await this.roomService.getRoomsUserMembered(trx, req.user.id);
            if (rooms instanceof error_1.Error) {
                throw new common_1.HttpException(rooms.message, rooms.code);
            }
            return rooms;
        });
    }
    async handleIsMemberOfRoom(code, req) {
        return this.knex.transaction(async (trx) => {
            const room = await this.roomModel.getRoomMember(trx, code, req.user.id);
            if (room instanceof error_1.Error) {
                room.cause = room.message;
                throw new common_1.HttpException(room.message, 500);
            }
            return room ? { status: true } : { status: false };
        });
    }
    async handleGetRoom(req, code) {
        return this.knex.transaction(async (trx) => {
            const room = await this.roomService.getRoomByCode(trx, code);
            if (room instanceof error_1.Error) {
                throw new common_1.HttpException(room.message, room.code);
            }
            return room;
        });
    }
    async handleCreateRoom(req, body) {
        return this.knex.transaction(async (trx) => {
            var _a;
            const newRoom = await this.roomService.createRoom(trx, Object.assign(Object.assign({}, body), { owner: (_a = req.user) === null || _a === void 0 ? void 0 : _a.id }));
            if (newRoom instanceof error_1.Error) {
                throw new common_1.HttpException(newRoom.message, newRoom.code);
            }
            return { room_code: newRoom };
        });
    }
    async handleDleteRoom(req, code) {
        return this.knex.transaction(async (trx) => {
            const hasPermission = await this.roomService.hasPermission(trx, code, req.user.id, [roles_enum_1.ERoomRole.OWNER], true);
            if (hasPermission instanceof error_1.Error) {
                throw new common_1.HttpException(hasPermission.message, hasPermission.code);
            }
            const deleted = this.roomService.deleteRoom(trx, code);
            if (deleted instanceof error_1.Error) {
                throw new common_1.HttpException(deleted.message, deleted.code);
            }
            return { success: true };
        });
    }
    async handleUpdateRoomImage(req, status, room_code, image) {
        try {
            return this.knex.transaction(async (trx) => {
                const hasPermission = await this.roomService.hasPermission(trx, room_code, req.user.id, [roles_enum_1.ERoomRole.OWNER], true);
                if (hasPermission instanceof error_1.Error) {
                    throw new common_1.HttpException(hasPermission.message, hasPermission.code);
                }
                const updatedAvatar = await this.roomService.updateImage(trx, room_code, image);
                if (updatedAvatar instanceof error_1.Error) {
                    throw new common_1.HttpException(updatedAvatar.message, updatedAvatar.code);
                }
                if (updatedAvatar)
                    return { success: true };
                return { success: false };
            });
        }
        catch (err) {
            throw new common_1.HttpException(err, 500);
        }
    }
};
__decorate([
    (0, common_1.Get)(""),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], RoomController.prototype, "handleGetRoomsYouMembered", null);
__decorate([
    (0, common_1.Get)("is-member/:code"),
    __param(0, (0, common_1.Param)('code')),
    __param(1, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], RoomController.prototype, "handleIsMemberOfRoom", null);
__decorate([
    (0, common_1.Get)(":code"),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Param)('code')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], RoomController.prototype, "handleGetRoom", null);
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, room_dto_1.CreateRoomDto]),
    __metadata("design:returntype", Promise)
], RoomController.prototype, "handleCreateRoom", null);
__decorate([
    (0, common_1.Delete)(":code"),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Param)('code')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String]),
    __metadata("design:returntype", Promise)
], RoomController.prototype, "handleDleteRoom", null);
__decorate([
    (0, common_1.Post)(":code/image"),
    (0, common_1.UseInterceptors)((0, platform_express_1.FileInterceptor)('image')),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Query)('status')),
    __param(2, (0, common_1.Param)('code')),
    __param(3, (0, common_1.UploadedFile)(new common_1.ParseFilePipe({
        validators: [
            new common_1.FileTypeValidator({ fileType: 'image/jpeg' || "image/png" }),
        ],
    }))),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String, String, Object]),
    __metadata("design:returntype", Promise)
], RoomController.prototype, "handleUpdateRoomImage", null);
RoomController = __decorate([
    (0, common_1.Controller)('rooms'),
    __metadata("design:paramtypes", [room_service_1.RoomService, room_model_1.RoomModel])
], RoomController);
exports.RoomController = RoomController;
//# sourceMappingURL=room.controller.js.map