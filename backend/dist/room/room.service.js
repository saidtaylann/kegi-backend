"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoomService = void 0;
const common_1 = require("@nestjs/common");
const room_model_1 = require("./room.model");
const database_1 = __importDefault(require("../database"));
const config_json_1 = __importDefault(require("../config.json"));
const path_1 = __importDefault(require("path"));
const user_service_1 = require("../user/user.service");
const error_1 = require("../types/error");
const s3_1 = require("../services/aws/s3");
const roles_enum_1 = require("../enums/roles.enum");
const moment_1 = __importDefault(require("moment"));
const redis_1 = __importDefault(require("../services/redis"));
let RoomService = class RoomService {
    constructor(roomModel, userService) {
        this.roomModel = roomModel;
        this.userService = userService;
        this.knex = database_1.default.connection;
        this.redis = redis_1.default;
        this.roomImageDir = `${config_json_1.default.DIRS.store}/${config_json_1.default.DIRS.images}/room/`;
        this.redisSessions = this.redis.getSessionModule();
    }
    async getRoomsBy(trx, by) {
        const rooms = await this.roomModel.getRoomsBy(trx, by);
        if (rooms instanceof error_1.Error) {
            rooms.code = 500;
            return rooms;
        }
        return rooms;
    }
    async getRoomsUserMembered(trx, uid) {
        const rooms = await this.roomModel.getRoomsUserMembered(trx, uid);
        if (rooms instanceof error_1.Error) {
            rooms.code = 500;
        }
        return rooms;
    }
    async getRoomByCode(trx, code) {
        const room = await this.roomModel.getRoomBy(trx, { code });
        if (room instanceof error_1.Error) {
            room.code = 500;
            return room;
        }
        return room;
    }
    async getRoomDetail(trx, code) {
        const room = await this.roomModel.getRoomDetail(trx, { code: code });
        if (room instanceof error_1.Error) {
            room.code = 500;
            return room;
        }
        if (room) {
            return this.parseRoomDetail(room);
        }
    }
    async createRoom(trx, data) {
        const user = await this.userService.getUserById(trx, data.owner);
        if (user instanceof error_1.Error) {
            user.cause = user.message;
            user.message = "an error occured while creating room";
            user.code = 500;
            return user;
        }
        else if (user) {
            if (!user.verified) {
                return new error_1.Error({ message: 'user is not verified', code: 401 });
            }
            const roomCode = await this.roomModel.createRoom(trx, data);
            if (roomCode instanceof error_1.Error) {
                roomCode.code = 500;
                return roomCode;
            }
            if (data.is_temp === false) {
                const newMember = await this.roomModel.createMember(trx, { user_id: user.id, room_code: roomCode, role: roles_enum_1.ERoomRole.OWNER });
                if (newMember instanceof error_1.Error) {
                    newMember.code = 500;
                    return newMember;
                }
            }
            return roomCode;
        }
    }
    async updateRoom(trx, code, dataToUpdate) {
        const room = await this.roomModel.updateRoom(trx, code, dataToUpdate);
        if (room instanceof error_1.Error) {
            room.code = 500;
            return room;
        }
        return room;
    }
    async updateImage(trx, room_code, image) {
        const room = await this.roomModel.getRoomBy(trx, { id: room_code });
        if (room instanceof error_1.Error) {
            room.cause = room.message;
            room.message = "an error occured while updating room image";
            room.code = 500;
            return room;
        }
        const imageName = this.setRoomImageName(room_code, image.name);
        if (!room.image) {
            const isUploaded = await (0, s3_1.uploadS3File)(image, this.setRoomImageName(room_code, image.name), this.roomImageDir);
            if (isUploaded instanceof error_1.Error) {
                isUploaded.cause = isUploaded.message;
                isUploaded.message = "an error occured while uploading the image";
                isUploaded.code = 500;
                return isUploaded;
            }
            const isUpdated = await this.roomModel.updateRoom(trx, room_code, { avatar: imageName });
            if (isUpdated instanceof error_1.Error) {
                isUpdated.cause = isUpdated.message;
                isUpdated.message = "an error occured while updating the image";
                isUpdated.code = 500;
                return isUpdated;
            }
        }
        else {
            const isUploaded = await (0, s3_1.updateS3File)(image, this.setRoomImageName(room_code, image.name), this.roomImageDir);
            if (isUploaded instanceof error_1.Error) {
                isUploaded.cause = isUploaded.message;
                isUploaded.message = "an error occured while uploading the image";
                isUploaded.code = 500;
                return isUploaded;
            }
            const isUpdated = await this.roomModel.updateRoom(trx, room_code, { image: imageName });
            if (isUpdated instanceof error_1.Error) {
                isUpdated.cause = isUpdated.message;
                isUpdated.message = "an error occured while updating the image";
                isUpdated.code = 500;
                return isUpdated;
            }
        }
        return true;
    }
    async deleteRoom(trx, room_code) {
        const room = await this.roomModel.deleteRoom(trx, room_code);
        if (room instanceof error_1.Error) {
            room.code = 500;
            return room;
        }
        return room;
    }
    async getRoomSessions(trx, room_code) {
        const sessions = await this.roomModel.getRoomSessions(trx, room_code, 50, "desc");
        if (sessions instanceof error_1.Error) {
            sessions.code = 500;
            return sessions;
        }
        return sessions;
    }
    async getRoomSessionDetail(trx, session_id) {
        const session = await this.roomModel.getRoomSessionDetail(trx, session_id);
        if (session instanceof error_1.Error) {
            session.code = 500;
            return session;
        }
        return this.parseRoomSessionDetail(session);
    }
    setRoomImageName(room_code, imageName) {
        return `${room_code}${path_1.default.extname(imageName)}`;
    }
    parseRoomDetail(room) {
        const parsedParticipants = {};
        parsedParticipants.code = room[0].code;
        parsedParticipants.room_title = room[0].room_title && room[0].room_title;
        parsedParticipants.passcode = room[0].passcode && room[0].passcode;
        parsedParticipants.image = room[0].image && room[0].image;
        parsedParticipants.is_edu = room[0].is_edu;
        parsedParticipants.is_temp = room[0].is_temp;
        parsedParticipants.room_created_at = room[0].room_created_at;
        parsedParticipants.owner_username = room[0].owner_username;
        parsedParticipants.owner_name = room[0].owner_name;
        parsedParticipants.owner_lastname = room[0].owner_lastname;
        parsedParticipants.owner_id = room[0].owner_id;
        parsedParticipants.is_open = room[0].is_open;
        parsedParticipants.members = [];
        room.forEach((r) => {
            parsedParticipants.members.push({
                id: r.member_id,
                name: r.member_name,
                lastname: r.member_lastname,
                username: r.member_username,
                email: r.member_email,
                avatar: r.member_avatar,
                joined_at: r.member_joined_at,
                participant_role: r.member_role,
            });
        });
        return parsedParticipants;
    }
    parseRoomSessionDetail(room) {
        const parsedParticipants = {};
        parsedParticipants.code = room[0].code;
        parsedParticipants.title = room[0].title && room[0].title;
        parsedParticipants.passcode = room[0].passcode && room[0].passcode;
        parsedParticipants.image = room[0].image && room[0].image;
        parsedParticipants.is_edu = room[0].is_edu;
        parsedParticipants.is_temp = room[0].is_temp;
        parsedParticipants.room_created_at = room[0].room_created_at;
        parsedParticipants.owner_username = room[0].owner_username;
        parsedParticipants.owner_name = room[0].owner_name;
        parsedParticipants.owner_id = room[0].owner_id;
        parsedParticipants.owner_lastname = room[0].owner_lastname;
        parsedParticipants.room_session_id = room[0].room_session_id;
        parsedParticipants.started_at = room[0].started_at;
        parsedParticipants.ended_at = room[0].ended_at;
        parsedParticipants.participants = [];
        room.forEach((rs) => {
            parsedParticipants.participants.push(Object.assign(Object.assign(Object.assign({ id: rs.participant_id }, (rs.participant_user_id && {
                user_id: rs.participant_user_id,
                name: rs.participant_name,
                lastname: rs.participant_lastname,
                username: rs.participant_username,
                email: rs.participant_email,
                avatar: rs.participant_avatar,
                is_member: rs.participant_is_member
            })), (rs.participant_display_name && {
                display_name: rs.participant_display_name,
            })), { websocket_id: rs.participant_websocket_id, joined_at: rs.participant_joined_at, left_at: rs.participant_left_at, role: rs.participant_role }));
        });
        return parsedParticipants;
    }
    async hasPermission(trx, roomCodeOrSessionId, uid, roles, forMember) {
        let isHost;
        if (forMember) {
            isHost = await this.roomModel.getMember(trx, roomCodeOrSessionId, uid);
        }
        else {
            isHost = await this.roomModel.getParticipant(trx, roomCodeOrSessionId);
        }
        if (isHost instanceof error_1.Error) {
            isHost.code = 500;
            return isHost;
        }
        if (roles.includes(isHost.role))
            return { role: isHost.role };
        return new error_1.Error({ message: 'not authorized', code: 401 });
    }
    async createSession(socket, room_code, uid, sessionPreferences) {
        return this.knex.transaction(async (trx) => {
            const isOwner = await this.roomModel.getRoomOnly(trx, room_code);
            if (isOwner instanceof error_1.Error) {
                isOwner.cause = isOwner.message;
                isOwner.message = "createSession";
                isOwner.code = 500;
                return isOwner;
            }
            if (isOwner.owner !== uid)
                return new error_1.Error({ message: 'createSession', code: 401 });
            const room = await this.roomModel.getRoomBy(trx, { code: room_code });
            if (room instanceof error_1.Error) {
                room.code = 500;
                room.cause = room.message;
                room.message = "roomCreateSession";
                return room;
            }
            const newSession = await this.roomModel.createRoomSession(trx, room_code);
            if (newSession instanceof error_1.Error) {
                newSession.code = 500;
                return newSession;
            }
            const participant = await this.addParticipant(trx, newSession, roles_enum_1.ERoomRole.OWNER, false, socket.id, uid);
            if (participant instanceof error_1.Error)
                return participant;
            const sessionDetail = await this.getRoomSessionDetail(trx, newSession);
            if (sessionDetail instanceof error_1.Error) {
                sessionDetail.code = 500;
                sessionDetail.cause = sessionDetail.message;
                sessionDetail.message = "createRoomSession";
                return sessionDetail;
            }
            Object.assign(sessionDetail, {
                is_mic_authorized: sessionPreferences.isMicAuthorized,
                is_cam_authorized: sessionPreferences.isCamAuthorized,
                is_sharing_screen_authorized: sessionPreferences.isSharingScreenAuthorized
            });
            await this.redisSessions.setSession(newSession, sessionPreferences);
            if (participant) {
                socket.join(`${newSession}`);
                socket.emit("roomSessionCreated", sessionDetail);
            }
        });
    }
    async requestToJoinSession(socket, room_code, passcode, uid, display_name) {
        const eventName = "requestToJoinSession";
        return this.knex.transaction(async (trx) => {
            const room = await this.roomModel.getRoomBy(trx, { code: room_code });
            if (room instanceof error_1.Error) {
                room.cause = room.message;
                room.code = 500;
                room.message = eventName;
                return room;
            }
            else if (room) {
                const roomSession = await this.roomModel.getLastGoingSession(trx, { room_code });
                if (roomSession instanceof error_1.Error) {
                    roomSession.cause = roomSession.message;
                    roomSession.message = eventName;
                    roomSession.code = 500;
                    return room;
                }
                if (room.is_open) {
                    let participant;
                    if (room.is_temp === false) {
                        if (roomSession) {
                            if (uid && !display_name) {
                                const isMember = await this.roomModel.getMember(trx, room.code, uid);
                                if (isMember instanceof error_1.Error) {
                                    isMember.cause = isMember.message;
                                    isMember.code = 500;
                                    isMember.message = eventName;
                                    return isMember;
                                }
                                else if (!isMember) {
                                    if (passcode !== room.passcode)
                                        return new error_1.Error({ message: 'invalidPasscode' });
                                    participant = await this.addParticipant(trx, roomSession.id, roles_enum_1.ERoomRole.PARTICIPANT, false, socket.id, uid);
                                    if (participant instanceof error_1.Error) {
                                        participant.message = eventName;
                                        return participant;
                                    }
                                }
                                else {
                                    participant = await this.addParticipant(trx, roomSession.id, isMember.role, true, socket.id);
                                    if (participant instanceof error_1.Error) {
                                        participant.message = eventName;
                                        return participant;
                                    }
                                }
                            }
                            else {
                                if (passcode !== room.passcode)
                                    return new error_1.Error({ message: 'invalidPasscode' });
                                participant = await this.addParticipant(trx, roomSession.id, 0, false, socket.id, undefined, display_name);
                                if (participant instanceof error_1.Error) {
                                    participant.message = eventName;
                                    return participant;
                                }
                            }
                            socket.emit("joinSessionRequestAccepted", { room_session_id: roomSession.id, code: room.code });
                        }
                    }
                    else if (room.is_temp === true) {
                        if (uid && !display_name) {
                            if (passcode !== room.passcode)
                                return new error_1.Error({ message: 'invalidPasscode' });
                            participant = await this.addParticipant(trx, roomSession.id, roles_enum_1.ERoomRole.PARTICIPANT, false, socket.id, uid);
                            if (participant instanceof error_1.Error) {
                                participant.message = eventName;
                                return participant;
                            }
                        }
                        else {
                            if (room.passcode && passcode !== room.passcode)
                                return new error_1.Error({ message: 'invalidPasscode' });
                            participant = await this.addParticipant(trx, roomSession.id, roles_enum_1.ERoomRole.PARTICIPANT, false, socket.id, undefined, display_name);
                            if (participant instanceof error_1.Error) {
                                participant.message = eventName;
                                return participant;
                            }
                        }
                        socket.emit("joinSessionRequestAccepted", { room_session_id: roomSession.id, code: room.code });
                    }
                }
                return new error_1.Error({ message: 'the room is closed', code: 401 });
            }
        });
    }
    async joinSession(socket, incomingData) {
        const eventName = "joinSession";
        const sessionPreferences = await this.redisSessions.getSession(incomingData.sessionId);
        await this.redisSessions.setParticipant(incomingData);
        return this.knex.transaction(async (trx) => {
            var _a;
            const sessionDetail = await this.getRoomSessionDetail(trx, incomingData.sessionId);
            if (sessionDetail instanceof error_1.Error) {
                sessionDetail.code = 500;
                sessionDetail.cause = sessionDetail.message;
                sessionDetail.message = eventName;
                return sessionDetail;
            }
            sessionDetail.is_mic_authorized = sessionPreferences.isMicAuthorized == "true" ? true : false;
            sessionDetail.is_cam_authorized = sessionPreferences.isCamAuthorized == "true" ? true : false;
            sessionDetail.is_sharing_screen_authorized = sessionPreferences.isSharingScreenAuthorized ? true : false;
            let newParticipant;
            for (const p of sessionDetail.participants) {
                const participant = await this.redisSessions.getParticipant(incomingData.sessionId, p.websocket_id);
                const parsedParticipant = {
                    isMicOpen: true,
                    isCamOpen: true,
                    isMicAuthorized: true,
                    isCamAuthorized: true,
                    isSharingScreenAuthorized: true,
                };
                parsedParticipant.isMicOpen = participant.isMicOpen == "true" ? true : false;
                parsedParticipant.isCamOpen = participant.isCamOpen == "true" ? true : false;
                parsedParticipant.sharingScreen = (_a = participant.sharingScreen) !== null && _a !== void 0 ? _a : '';
                parsedParticipant.isMicAuthorized = participant.isMicAuthorized == "true" ? true : false;
                parsedParticipant.isCamAuthorized = participant.isCamAuthorized == "true" ? true : false;
                parsedParticipant.isSharingScreenAuthorized = participant.isSharingScreenAuthorized == "true" ? true : false;
                p.stream_id = participant.streamId;
                if (p.role === 0) {
                    if (parsedParticipant.isMicAuthorized) {
                        p.is_mic_open = parsedParticipant.isMicOpen;
                    }
                    else {
                        p.is_mic_open = false;
                    }
                    if (parsedParticipant.isCamAuthorized) {
                        p.is_cam_open = parsedParticipant.isCamOpen;
                    }
                    else {
                        p.is_cam_open = false;
                    }
                    if (parsedParticipant.isSharingScreenAuthorized) {
                        p.sharing_screen = parsedParticipant.sharingScreen;
                    }
                    else {
                        p.sharing_screen = '';
                    }
                }
                else {
                    p.is_mic_open = parsedParticipant.isMicOpen;
                    p.is_cam_open = parsedParticipant.isCamOpen;
                    p.sharing_screen = parsedParticipant.sharingScreen;
                }
                p.is_mic_authorized = parsedParticipant.isMicAuthorized && sessionDetail.is_mic_authorized;
                p.is_cam_authorized = parsedParticipant.isCamAuthorized && sessionDetail.is_cam_authorized;
                p.is_sharing_screen_authorized = parsedParticipant.isSharingScreenAuthorized && sessionDetail.is_sharing_screen_authorized;
                if (p.websocket_id === incomingData.websocketId) {
                    newParticipant = p;
                }
            }
            socket.join(`${incomingData.sessionId}`);
            socket.emit("joinedSession", sessionDetail);
            socket.in(`${incomingData.sessionId}`).emit("someoneJoinedSession", newParticipant);
        });
    }
    async addParticipant(trx, room_session_id, role, is_member, websocket_id, uid, display_name) {
        const participant = await this.roomModel.createParticipant(trx, {
            user_id: uid, room_session_id, role, is_member, display_name, websocket_id
        });
        if (participant instanceof error_1.Error) {
            participant.cause = participant.message;
            participant.code = 500;
            return participant;
        }
        else if (participant) {
            const loginMove = await this.roomModel.createParticipantLoginMove(trx, participant.id, { loged_in_at: (0, moment_1.default)().format() });
            if (loginMove instanceof error_1.Error) {
                loginMove.cause = loginMove.message;
                loginMove.code = 500;
                return loginMove;
            }
            return participant;
        }
    }
    async removeParticipant(trx, participant_id, sessionId, socketId, eventName) {
        const deletedParticipant = await this.roomModel.deleteParticipant(trx, participant_id);
        if (deletedParticipant instanceof error_1.Error) {
            deletedParticipant.cause = deletedParticipant.message;
            deletedParticipant.message = eventName;
            deletedParticipant.code = 500;
            return deletedParticipant;
        }
        else if (deletedParticipant) {
            const loginMove = await this.roomModel.createParticipantLoginMove(trx, participant_id, { loged_out_at: (0, moment_1.default)().format() });
            if (loginMove instanceof error_1.Error) {
                loginMove.cause = loginMove.message;
                deletedParticipant.message = eventName;
                loginMove.code = 500;
                return loginMove;
            }
        }
        const streamIdOfParticipant = (await this.redisSessions.getParticipant(sessionId, socketId)).streamId;
        await this.redisSessions.deleteParticipant(sessionId, socketId);
        return streamIdOfParticipant;
    }
    async openRoom(socket, room_code, uid) {
        return this.knex.transaction(async (trx) => {
            const memberPermission = await this.hasPermission(trx, room_code, uid, [2, 3], true);
            if (memberPermission instanceof error_1.Error) {
                memberPermission.cause = memberPermission.message;
                memberPermission.message = "openRoom";
                return memberPermission;
            }
            else if (memberPermission) {
                const session = await this.roomModel.createRoomSession(trx, room_code);
                if (session instanceof error_1.Error) {
                    session.code = 500;
                    session.cause = session.message;
                    session.message = "openRoom";
                    return session;
                }
                else if (session) {
                    const room = await this.roomModel.updateRoom(trx, room_code, { is_open: true });
                    if (room instanceof error_1.Error) {
                        room.code = 500;
                        room.cause = room.message;
                        room.message = "openRoom";
                        return room;
                    }
                    else if (room) {
                        const participant = await this.addParticipant(trx, session.id, memberPermission.role, true, socket.id, uid);
                        if (participant instanceof error_1.Error) {
                            participant.message = "openRoom";
                            return participant;
                        }
                        const sessionDetail = await this.roomModel.getRoomSessionDetail(trx, session.id);
                        if (sessionDetail instanceof error_1.Error) {
                            session.code = 500;
                            session.cause = session.message;
                            session.message = "openRoom";
                            return session;
                        }
                        if (sessionDetail) {
                            socket.join(session);
                            socket.emit("roomOpened", sessionDetail);
                        }
                    }
                }
            }
        });
    }
    async _endSession(trx, sessionId, roomCode, eventName) {
        const closedSession = this.roomModel.updateRoomSession(trx, sessionId, { ended_at: (0, moment_1.default)().format() });
        if (closedSession instanceof error_1.Error) {
            closedSession.cause = closedSession.message;
            closedSession.message = eventName;
            closedSession.code = 500;
            return closedSession;
        }
        const room = await this.roomModel.updateRoom(trx, roomCode, { is_open: true });
        if (room instanceof error_1.Error) {
            room.cause = room.message;
            room.message = eventName;
            room.code = 500;
            return room;
        }
        await this.redisSessions.deleteSession(sessionId);
    }
    async mute(server, body) {
        for (const socket in body.socketId) {
            await this.redisSessions.updateParticipant(body.roomSessionId, socket, "isMicOn", false);
        }
        server.in(`${body.roomSessionId}`).emit("muted", body.socketId);
    }
    async unmute(server, body) {
        for (const socket in body.socketId) {
            await this.redisSessions.updateParticipant(body.roomSessionId, socket, "isMicOn", true);
        }
        server.in(`${body.roomSessionId}`).emit("unmuted", body.socketId);
    }
    async openCam(server, body) {
        for (const socket in body.socketId) {
            await this.redisSessions.updateParticipant(body.roomSessionId, socket, "isCamOpen", true);
        }
        server.to(`${body.roomSessionId}`).emit("turnedOnCam", body.socketId);
    }
    async closeCam(server, body) {
        for (const socket in body.socketId) {
            await this.redisSessions.updateParticipant(body.roomSessionId, socket, "isCamOpen", false);
        }
        server.in(`${body.roomSessionId}`).emit("turnedOffCam", body.socketId);
    }
    async shareScreen(server, body) {
        await this.redisSessions.updateParticipant(body.roomSessionId, body.socketId, "sharingScreen", body.streamId);
        server.in(`${body.roomSessionId}`).emit("sharedScreen", { socketId: body.socketId, streamId: body.streamId });
    }
    async stopSharingScreen(server, body) {
        await this.redisSessions.deleteParticipantField(body.roomSessionId, body.socketId, "sharingScreen");
        server.in(`${body.roomSessionId}`).emit("stoppedSharingScreen", body.socketId);
    }
    async leaveSession(server, socket, body, userId) {
        const eventName = "leaveSession";
        return this.knex.transaction(async (trx) => {
            const roomSession = await this.roomModel.getRoomSession(trx, body.roomSessionId);
            if (roomSession instanceof error_1.Error) {
                roomSession.cause = roomSession.message;
                roomSession.message = eventName;
                roomSession.code = 500;
            }
            if (userId) {
                if (body.isClosingSession) {
                    const canUserCloseSession = await this.hasPermission(trx, body.roomSessionId, userId, [1, 2, 3], false);
                    if (canUserCloseSession instanceof error_1.Error) {
                        canUserCloseSession.cause = canUserCloseSession.message;
                        canUserCloseSession.message = eventName;
                        return canUserCloseSession;
                    }
                    const participantsOfRoom = await this.roomModel.getParticipants(trx, { room_session_id: body.roomSessionId });
                    if (participantsOfRoom instanceof error_1.Error) {
                        participantsOfRoom.cause = participantsOfRoom.message;
                        participantsOfRoom.message = eventName;
                        participantsOfRoom.code = 500;
                        return participantsOfRoom;
                    }
                    for (const p of participantsOfRoom) {
                        const deletedParticipant = await this.removeParticipant(trx, p.id, body.roomSessionId, body.socketId, eventName);
                        if (deletedParticipant instanceof error_1.Error) {
                            deletedParticipant.cause = deletedParticipant.message;
                            deletedParticipant.message = eventName;
                            deletedParticipant.code = 500;
                            return deletedParticipant;
                        }
                        server.in(`${body.roomSessionId}`).emit("sessionEnded");
                        server.sockets.sockets.get(`${p.websocket_id}`).leave(`${body.roomSessionId}`);
                    }
                    const endedSession = await this._endSession(trx, body.roomSessionId, roomSession.room_code, eventName);
                    if (endedSession instanceof error_1.Error)
                        return endedSession;
                    socket.leave(`${body.roomSessionId}`);
                }
                else {
                    const participant = await this.roomModel.getParticipant(trx, body.roomSessionId);
                    if (participant instanceof error_1.Error) {
                        participant.cause = participant.message;
                        participant.message = eventName;
                        participant.code = 500;
                        return participant;
                    }
                    if (!participant)
                        return new error_1.Error({ message: 'participant not found', code: 404 });
                    const deletedParticipant = await this.removeParticipant(trx, body.participantId, body.roomSessionId, body.socketId, eventName);
                    if (deletedParticipant instanceof error_1.Error) {
                        deletedParticipant.cause = deletedParticipant.message;
                        deletedParticipant.message = eventName;
                        deletedParticipant.code = 500;
                        return deletedParticipant;
                    }
                    if (server.sockets.adapter.rooms.get(`${body.roomSessionId}`).size === 1) {
                        const endedSession = await this._endSession(trx, body.roomSessionId, roomSession.room_code, eventName);
                        if (endedSession instanceof error_1.Error) {
                            return endedSession;
                        }
                        return server.in(`${body.roomSessionId}`).emit("sessionEnded");
                    }
                    if ([1, 2, 3].includes(participant.role)) {
                        const authorizedParticipants = await this.roomModel.getParticipantsByRange(trx, { room_session_id: body.roomSessionId }, 'p.role', [1, 2, 3]);
                        if (authorizedParticipants instanceof error_1.Error) {
                            authorizedParticipants.cause = authorizedParticipants.message;
                            authorizedParticipants.message = eventName;
                            authorizedParticipants.code = 500;
                            return authorizedParticipants;
                        }
                        if (authorizedParticipants.length === 0) {
                            const participantToAuthorize = await this.roomModel.getParticipantsByOrder(trx, body.roomSessionId, "p.created_at", "asc", 1);
                            if (participantToAuthorize instanceof error_1.Error) {
                                participantToAuthorize.cause = participantToAuthorize.message;
                                participantToAuthorize.message = eventName;
                                participantToAuthorize.code = 500;
                                return participantToAuthorize;
                            }
                            const newAuthorizedParticipant = await this.roomModel.updateParticipant(trx, participantToAuthorize[0].id, { role: 1 });
                            if (newAuthorizedParticipant instanceof error_1.Error) {
                                newAuthorizedParticipant.cause = newAuthorizedParticipant.message;
                                newAuthorizedParticipant.message = eventName;
                                newAuthorizedParticipant.code = 500;
                                return newAuthorizedParticipant;
                            }
                            socket.emit("leftSession");
                            socket.in(`${body.roomSessionId}`).emit("someoneLeftSession", { streamId: deletedParticipant, websocketId: body.socketId });
                            server.in(`${body.roomSessionId}`).emit("changedParticipantRole", { participantId: [participantToAuthorize[0].id], newRole: newAuthorizedParticipant.role });
                        }
                    }
                    socket.leave(`${body.roomSessionId}`);
                }
            }
            else {
                const deletedParticipant = await this.removeParticipant(trx, body.participantId, body.roomSessionId, body.socketId, eventName);
                if (deletedParticipant instanceof error_1.Error) {
                    deletedParticipant.cause = deletedParticipant.message;
                    deletedParticipant.message = eventName;
                    deletedParticipant.code = 500;
                    return deletedParticipant;
                }
                socket.emit("leftSession");
                socket.in(`${body.roomSessionId}`).emit("someoneLeftSession", body.socketId);
                if (server.sockets.adapter.rooms.get(`${body.roomSessionId}`).size === 1) {
                    const endedSession = await this._endSession(trx, body.roomSessionId, roomSession.room_code, eventName);
                    if (endedSession instanceof error_1.Error) {
                        return endedSession;
                    }
                    server.in(`${body.roomSessionId}`).emit("sessionEnded");
                }
                socket.leave(`${body.roomSessionId}`);
            }
        });
    }
    async endSession(server, sessionId) {
        const eventName = "endSession";
        return this.knex.transaction(async (trx) => {
            const session = await this.roomModel.getRoomSession(trx, sessionId);
            const participants = await this.roomModel.getParticipants(trx, { room_session_id: sessionId });
            if (participants instanceof error_1.Error) {
                participants.cause = participants.message;
                participants.message = eventName;
                participants.code = 500;
                return participants;
            }
            for (const p of participants) {
                const removedParticipant = await this.removeParticipant(trx, p.id, sessionId, p.websocket_id, eventName);
                if (removedParticipant instanceof error_1.Error)
                    return removedParticipant;
            }
            const endedSession = await this._endSession(trx, sessionId, session.room_code, eventName);
            if (endedSession instanceof error_1.Error)
                return endedSession;
            server.in(`${sessionId}`).emit("sessionEnded");
        });
    }
    async switchMicAuthorization(server, body) {
        for (const socket of body.socketId) {
            await this.redisSessions.updateParticipant(body.roomSessionId, socket, "isMicAuthorized", body.isMicAllowed);
            if (!body.isMicAllowed) {
                await this.redisSessions.updateParticipant(body.roomSessionId, socket, "isMicOpened", false);
            }
        }
        if (body.all) {
            await this.redisSessions.updateSession(body.roomSessionId, "isMicAuthorized", body.isMicAllowed);
        }
        server.in(`${body.roomSessionId}`).emit("switchedMicAuthorization", { isMicAllowed: body.isMicAllowed, sockets: body.socketId, all: body.all });
    }
    async switchCamAuthorization(server, body) {
        for (const socket of body.socketId) {
            await this.redisSessions.updateParticipant(body.roomSessionId, socket, "isCamAuthorized", body.isCamAllowed);
            if (!body.isCamAllowed) {
                await this.redisSessions.updateParticipant(body.roomSessionId, socket, "isCamOpen", false);
            }
        }
        if (body.all) {
            await this.redisSessions.updateSession(body.roomSessionId, "isCamAuthorized", body.isCamAllowed);
        }
        server.in(`${body.roomSessionId}`).emit("switchedCamAuthorization", { isCamAllowed: body.isCamAllowed, sockets: body.socketId });
    }
    async switchSharingScreenAuthorization(server, body) {
        for (const socket of body.socketId) {
            await this.redisSessions.updateParticipant(body.roomSessionId, socket, "isSharingScreenAuthorized", body.isSharingScreenAllowed);
            if (!body.isSharingScreenAllowed) {
                await this.redisSessions.updateParticipant(body.roomSessionId, socket, "sharingScreen", "");
            }
        }
        if (body.all) {
            await this.redisSessions.updateSession(body.roomSessionId, "isSharingScreenAuthorized", body.isSharingScreenAllowed);
        }
        await this.redisSessions.updateSession(body.roomSessionId, "isSharingScreenAuthorized", body.isSharingScreenAllowed);
        server.in(`${body.roomSessionId}`).emit("switchedSharingScreenAuthorization", { isSharingScreenAllowed: body.isSharingScreenAllowed, sockets: body.socketId });
    }
    async changeParticipantRole(server, body) {
        const eventName = "changeParticipantRole";
        await this.knex.transaction(async (trx) => {
            for (const pId of body.participantId) {
                const updatedParticipant = await this.roomModel.updateParticipant(trx, pId, { role: body.newRole });
                if (updatedParticipant instanceof error_1.Error) {
                    updatedParticipant.cause = updatedParticipant.message;
                    updatedParticipant.message = eventName;
                    updatedParticipant.code = 500;
                    return updatedParticipant;
                }
            }
        });
        server.in(`${body.roomSessionId}`).emit("changedParticipantRole", { participantId: body.participantId, newRole: body.newRole });
    }
};
RoomService = __decorate([
    (0, common_1.Injectable)(),
    __param(1, (0, common_1.Inject)((0, common_1.forwardRef)(() => user_service_1.UserService))),
    __metadata("design:paramtypes", [room_model_1.RoomModel, user_service_1.UserService])
], RoomService);
exports.RoomService = RoomService;
//# sourceMappingURL=room.service.js.map