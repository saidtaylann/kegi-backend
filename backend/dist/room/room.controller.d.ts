/// <reference types="multer" />
import { ExtReq } from 'src/types/common';
import { RoomService } from './room.service';
import { CreateRoomDto } from './dto/room.dto';
import { RoomModel } from './room.model';
export declare class RoomController {
    private readonly roomService;
    private readonly roomModel;
    private readonly knex;
    constructor(roomService: RoomService, roomModel: RoomModel);
    handleGetRoomsYouMembered(req: ExtReq): Promise<import("../types/room").IRoom[]>;
    handleIsMemberOfRoom(code: string, req: ExtReq): Promise<{
        status: boolean;
    }>;
    handleGetRoom(req: ExtReq, code: string): Promise<import("../types/room").IRoom>;
    handleCreateRoom(req: ExtReq, body: CreateRoomDto): Promise<{
        room_code: string;
    }>;
    handleDleteRoom(req: ExtReq, code: string): Promise<{
        success: boolean;
    }>;
    handleUpdateRoomImage(req: ExtReq, status: "new" | "update", room_code: string, image: Express.Multer.File): Promise<{
        success: boolean;
    }>;
}
