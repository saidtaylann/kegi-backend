"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ERoomRole = exports.ERole = void 0;
var ERole;
(function (ERole) {
    ERole[ERole["SUPERADMIN"] = 2] = "SUPERADMIN";
    ERole[ERole["ADMIN"] = 1] = "ADMIN";
    ERole[ERole["USER"] = 0] = "USER";
})(ERole = exports.ERole || (exports.ERole = {}));
var ERoomRole;
(function (ERoomRole) {
    ERoomRole[ERoomRole["OWNER"] = 3] = "OWNER";
    ERoomRole[ERoomRole["SUPERHOST"] = 2] = "SUPERHOST";
    ERoomRole[ERoomRole["HOST"] = 1] = "HOST";
    ERoomRole[ERoomRole["PARTICIPANT"] = 0] = "PARTICIPANT";
})(ERoomRole = exports.ERoomRole || (exports.ERoomRole = {}));
//# sourceMappingURL=roles.enum.js.map