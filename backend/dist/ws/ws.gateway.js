"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WsGateway = void 0;
const websockets_1 = require("@nestjs/websockets");
const socket_io_1 = require("socket.io");
const ws_service_1 = require("./ws.service");
const config_json_1 = __importDefault(require("../config.json"));
const common_1 = require("@nestjs/common");
const ws_guard_1 = require("./ws.guard");
const room_service_1 = require("../room/room.service");
const room_dto_1 = require("./dto/room.dto");
const user_service_1 = require("../user/user.service");
const error_1 = require("../types/error");
let WsGateway = class WsGateway {
    constructor(wsService, roomService, userService) {
        this.wsService = wsService;
        this.roomService = roomService;
        this.userService = userService;
    }
    async listenToStartSession(body, socket) {
        const LID = body.LID;
        if (socket.request.user && LID) {
            await this.userService.startSession(socket, { id: socket.request.user.id, LID });
        }
        else {
            await this.userService.startSession(socket);
        }
    }
    async listenToOpenRoom(body, socket) {
        const err = await this.roomService.openRoom(socket, body.roomCode, socket.request.user.id);
        if (err) {
            socket.emit("room_error", Object.assign({ message: err.message }, (err.code && { code: err.code })));
        }
    }
    async listenToCreateSession(body, socket) {
        const err = await this.roomService.createSession(socket, body.roomCode, socket.request.user.id, body.sessionPreferences);
        if (err instanceof error_1.Error) {
            socket.emit("room_error", Object.assign({ message: err.message }, (err.code && { code: err.code })));
        }
    }
    listenCloseRoomOrClass(body, socket) {
    }
    async listenToRequestToJoinSession(body, socket) {
        var _a;
        let err;
        if ((_a = socket.request.user) === null || _a === void 0 ? void 0 : _a.id) {
            err = await this.roomService.requestToJoinSession(socket, body.roomCode, body.passcode, socket.request.user.id);
        }
        else {
            err = await this.roomService.requestToJoinSession(socket, body.roomCode, body.passcode, undefined, body.displayName);
        }
        if (err) {
            socket.emit("room_error", Object.assign({ message: err.message }, (err.code && { code: err.code })));
        }
    }
    async listenToJoinSession(body, socket) {
        var _a;
        let err;
        if ((_a = socket.request.user) === null || _a === void 0 ? void 0 : _a.id) {
            err = await this.roomService.joinSession(socket, body);
        }
        else {
            err = await this.roomService.joinSession(socket, body);
        }
        if (err) {
            socket.emit("room_error", Object.assign({ message: err.message }, (err.code && { code: err.code })));
        }
    }
    async listenToLeaveSession(body, socket) {
        var _a;
        let err;
        if ((_a = socket.request.user) === null || _a === void 0 ? void 0 : _a.id) {
            err = await this.roomService.leaveSession(this.server, socket, body, socket.request.user.id);
        }
        else {
            err = await this.roomService.leaveSession(this.server, socket, body);
        }
        if (err) {
            socket.emit("room_error", Object.assign({ message: err.message }, (err.code && { code: err.code })));
        }
    }
    listenToMuteParticipant(body) {
        this.roomService.mute(this.server, body);
    }
    listenToUnmuteParticipant(body) {
        this.roomService.unmute(this.server, body);
    }
    listenToCamTurnOffParticipant(body) {
        this.roomService.closeCam(this.server, body);
    }
    async listenToCamTurOnParticipant(body) {
        await this.roomService.openCam(this.server, body);
    }
    async listenToShareScreen(body) {
        await this.roomService.shareScreen(this.server, body);
    }
    async listenToStopSharingScreen(body) {
        await this.roomService.stopSharingScreen(this.server, body);
    }
    async listenToEndSession(body) {
        await this.roomService.endSession(this.server, body.roomSessionId);
    }
    async listenToSwitchMicAuthorization(body) {
        await this.roomService.switchMicAuthorization(this.server, body);
    }
    async listenToSwitchCamAuthorization(body) {
        await this.roomService.switchCamAuthorization(this.server, body);
    }
    async listenToSwitchSharingScreenAuthorization(body) {
        await this.roomService.switchSharingScreenAuthorization(this.server, body);
    }
    async listenToChangeSessionRole(body) {
        await this.roomService.changeParticipantRole(this.server, body);
    }
};
__decorate([
    (0, websockets_1.WebSocketServer)(),
    __metadata("design:type", socket_io_1.Server)
], WsGateway.prototype, "server", void 0);
__decorate([
    (0, common_1.UseGuards)(ws_guard_1.WsOptionalJWTAuthGuard),
    (0, websockets_1.SubscribeMessage)('startUserSession'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, websockets_1.ConnectedSocket)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], WsGateway.prototype, "listenToStartSession", null);
__decorate([
    (0, common_1.UseGuards)(ws_guard_1.WsJwtAuthGuard),
    (0, websockets_1.SubscribeMessage)('openRoom'),
    __param(0, (0, websockets_1.MessageBody)()),
    __param(1, (0, websockets_1.ConnectedSocket)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], WsGateway.prototype, "listenToOpenRoom", null);
__decorate([
    (0, common_1.UseGuards)(ws_guard_1.WsJwtAuthGuard),
    (0, websockets_1.SubscribeMessage)('createRoomSession'),
    __param(0, (0, websockets_1.MessageBody)()),
    __param(1, (0, websockets_1.ConnectedSocket)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], WsGateway.prototype, "listenToCreateSession", null);
__decorate([
    (0, common_1.UseGuards)(ws_guard_1.WsJwtAuthGuard),
    (0, websockets_1.SubscribeMessage)('closeRoom'),
    __param(0, (0, websockets_1.MessageBody)()),
    __param(1, (0, websockets_1.ConnectedSocket)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], WsGateway.prototype, "listenCloseRoomOrClass", null);
__decorate([
    (0, common_1.UseGuards)(ws_guard_1.WsOptionalJWTAuthGuard),
    (0, websockets_1.SubscribeMessage)('requestToJoinSession'),
    __param(0, (0, websockets_1.MessageBody)()),
    __param(1, (0, websockets_1.ConnectedSocket)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [room_dto_1.RequestToJoinSessionDto, Object]),
    __metadata("design:returntype", Promise)
], WsGateway.prototype, "listenToRequestToJoinSession", null);
__decorate([
    (0, common_1.UseGuards)(ws_guard_1.WsOptionalJWTAuthGuard),
    (0, websockets_1.SubscribeMessage)('joinSession'),
    __param(0, (0, websockets_1.MessageBody)()),
    __param(1, (0, websockets_1.ConnectedSocket)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [room_dto_1.JoinSessionDto, Object]),
    __metadata("design:returntype", Promise)
], WsGateway.prototype, "listenToJoinSession", null);
__decorate([
    (0, common_1.UseGuards)(ws_guard_1.WsOptionalJWTAuthGuard),
    (0, websockets_1.SubscribeMessage)('leaveSession'),
    __param(0, (0, websockets_1.MessageBody)()),
    __param(1, (0, websockets_1.ConnectedSocket)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], WsGateway.prototype, "listenToLeaveSession", null);
__decorate([
    (0, common_1.UseGuards)(ws_guard_1.WsOptionalJWTAuthGuard),
    (0, websockets_1.SubscribeMessage)('mute'),
    __param(0, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], WsGateway.prototype, "listenToMuteParticipant", null);
__decorate([
    (0, common_1.UseGuards)(ws_guard_1.WsOptionalJWTAuthGuard),
    (0, websockets_1.SubscribeMessage)('unmute'),
    __param(0, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], WsGateway.prototype, "listenToUnmuteParticipant", null);
__decorate([
    (0, common_1.UseGuards)(ws_guard_1.WsOptionalJWTAuthGuard),
    (0, websockets_1.SubscribeMessage)('turnOffCam'),
    __param(0, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], WsGateway.prototype, "listenToCamTurnOffParticipant", null);
__decorate([
    (0, common_1.UseGuards)(ws_guard_1.WsOptionalJWTAuthGuard),
    (0, websockets_1.SubscribeMessage)('turnOnCam'),
    __param(0, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], WsGateway.prototype, "listenToCamTurOnParticipant", null);
__decorate([
    (0, common_1.UseGuards)(ws_guard_1.WsOptionalJWTAuthGuard),
    (0, websockets_1.SubscribeMessage)('shareScreen'),
    __param(0, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], WsGateway.prototype, "listenToShareScreen", null);
__decorate([
    (0, websockets_1.SubscribeMessage)("stopSharingScreen"),
    __param(0, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], WsGateway.prototype, "listenToStopSharingScreen", null);
__decorate([
    __param(0, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], WsGateway.prototype, "listenToEndSession", null);
__decorate([
    (0, websockets_1.SubscribeMessage)("switchMicAuthorization"),
    __param(0, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], WsGateway.prototype, "listenToSwitchMicAuthorization", null);
__decorate([
    (0, websockets_1.SubscribeMessage)("switchCamAuthorization"),
    __param(0, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], WsGateway.prototype, "listenToSwitchCamAuthorization", null);
__decorate([
    (0, websockets_1.SubscribeMessage)("switchSharingScreenAuthorization"),
    __param(0, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], WsGateway.prototype, "listenToSwitchSharingScreenAuthorization", null);
__decorate([
    (0, websockets_1.SubscribeMessage)("changeParticipantRole"),
    __param(0, (0, websockets_1.MessageBody)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], WsGateway.prototype, "listenToChangeSessionRole", null);
WsGateway = __decorate([
    (0, websockets_1.WebSocketGateway)({
        cors: {
            allowedHeaders: ['content-type', 'lid', 'accessToken', 'withcredentials'],
            origin: config_json_1.default.SERVER.allowedDomain,
            credentials: true,
            methods: ['GET', 'OPTIONS', 'PUT', 'POST', 'DELETE'],
        },
    }),
    __metadata("design:paramtypes", [ws_service_1.WsService, room_service_1.RoomService, user_service_1.UserService])
], WsGateway);
exports.WsGateway = WsGateway;
//# sourceMappingURL=ws.gateway.js.map