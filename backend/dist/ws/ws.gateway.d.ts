import { Server } from 'socket.io';
import { WsService } from './ws.service';
import { ExtSocket } from 'src/types/common';
import { RoomService } from 'src/room/room.service';
import { JoinSessionDto, RequestToJoinSessionDto, StartSessionDto } from './dto/room.dto';
import { UserService } from 'src/user/user.service';
export declare class WsGateway {
    private readonly wsService;
    private readonly roomService;
    private readonly userService;
    server: Server;
    constructor(wsService: WsService, roomService: RoomService, userService: UserService);
    listenToStartSession(body: any, socket: ExtSocket): Promise<void>;
    listenToOpenRoom(body: {
        roomCode: string;
    }, socket: ExtSocket): Promise<void>;
    listenToCreateSession(body: {
        roomCode: string;
        sessionPreferences: StartSessionDto;
    }, socket: ExtSocket): Promise<void>;
    listenCloseRoomOrClass(body: {
        userSessionId: string;
        roomCode: string;
    }, socket: ExtSocket): void;
    listenToRequestToJoinSession(body: RequestToJoinSessionDto, socket: ExtSocket): Promise<void>;
    listenToJoinSession(body: JoinSessionDto, socket: ExtSocket): Promise<void>;
    listenToLeaveSession(body: any, socket: ExtSocket): Promise<void>;
    listenToMuteParticipant(body: any): void;
    listenToUnmuteParticipant(body: any): void;
    listenToCamTurnOffParticipant(body: any): void;
    listenToCamTurOnParticipant(body: any): Promise<void>;
    listenToShareScreen(body: any): Promise<void>;
    listenToStopSharingScreen(body: any): Promise<void>;
    listenToEndSession(body: any): Promise<void>;
    listenToSwitchMicAuthorization(body: any): Promise<void>;
    listenToSwitchCamAuthorization(body: any): Promise<void>;
    listenToSwitchSharingScreenAuthorization(body: any): Promise<void>;
    listenToChangeSessionRole(body: any): Promise<void>;
}
