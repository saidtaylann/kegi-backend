import { ExecutionContext } from '@nestjs/common';
import { WsService } from './ws.service';
export declare class WsJwtAuthGuard {
    private readonly wsService;
    constructor(wsService: WsService);
    canActivate(context: ExecutionContext): Promise<boolean>;
}
export declare class WsOptionalJWTAuthGuard {
    private readonly wsService;
    constructor(wsService: WsService);
    canActivate(context: ExecutionContext): Promise<boolean>;
}
