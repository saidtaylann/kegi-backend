"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.WsOptionalJWTAuthGuard = exports.WsJwtAuthGuard = void 0;
const common_1 = require("@nestjs/common");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const ws_service_1 = require("./ws.service");
let WsJwtAuthGuard = class WsJwtAuthGuard {
    constructor(wsService) {
        this.wsService = wsService;
    }
    async canActivate(context) {
        const socket = context.switchToWs();
        const body = socket.getData();
        const client = socket.getClient();
        const token = body.accessToken;
        if (token) {
            const payload = jsonwebtoken_1.default.verify(token, process.env.JWT_SECRET);
            if (!payload) {
                return false;
            }
            client.request.user = payload;
            return true;
        }
        return false;
    }
};
WsJwtAuthGuard = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [ws_service_1.WsService])
], WsJwtAuthGuard);
exports.WsJwtAuthGuard = WsJwtAuthGuard;
let WsOptionalJWTAuthGuard = class WsOptionalJWTAuthGuard {
    constructor(wsService) {
        this.wsService = wsService;
    }
    async canActivate(context) {
        const socket = context.switchToWs();
        const body = socket.getData();
        const client = socket.getClient();
        const token = body.accessToken;
        if (!token)
            return true;
        const payload = jsonwebtoken_1.default.verify(token, process.env.JWT_SECRET);
        if (!payload) {
            return false;
        }
        client.request.user = payload;
        return true;
    }
};
WsOptionalJWTAuthGuard = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [ws_service_1.WsService])
], WsOptionalJWTAuthGuard);
exports.WsOptionalJWTAuthGuard = WsOptionalJWTAuthGuard;
//# sourceMappingURL=ws.guard.js.map