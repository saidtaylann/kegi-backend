import { RoomModel } from 'src/room/room.model';
export declare class WsService {
    private readonly roomModel;
    private readonly knex;
    private onlineUsers;
    constructor(roomModel: RoomModel);
    getCookie(client: any, name: string): any;
}
