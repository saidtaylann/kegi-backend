export declare class JoinSessionDto {
    sessionId: number;
    websocketId: string;
    participant: {
        streamId: string;
        isMicOpen: boolean;
        isCamOpen: boolean;
        isMicAuthorized: boolean;
        isCamAuthorized: boolean;
    };
}
export declare class RequestToJoinSessionDto {
    roomCode: string;
    passcode?: string;
    displayName?: string;
}
export declare class StartSessionDto {
    isMicAuthorized: boolean;
    isCamAuthorized: boolean;
    isSharingScreenAuthorized: boolean;
}
