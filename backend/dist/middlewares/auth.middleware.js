"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OptionalJWTMiddleware = exports.JWTMiddleware = void 0;
const common_1 = require("@nestjs/common");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
let JWTMiddleware = class JWTMiddleware {
    use(req, res, next) {
        var _a;
        let accessToken;
        accessToken = (_a = req.get("authorization")) === null || _a === void 0 ? void 0 : _a.split("Bearer ");
        if (accessToken.length === 2) {
            return jsonwebtoken_1.default.verify(accessToken[1], process.env.JWT_SECRET, async (err, user) => {
                if (err) {
                    if (err.name === 'TokenExpiredError') {
                        throw new common_1.HttpException('TokenExpired', 401);
                    }
                    throw new common_1.HttpException("invalid token", 401);
                }
                req.user = user;
                next();
            });
        }
        throw new common_1.HttpException('Invalid token', 401);
    }
};
JWTMiddleware = __decorate([
    (0, common_1.Injectable)()
], JWTMiddleware);
exports.JWTMiddleware = JWTMiddleware;
let OptionalJWTMiddleware = class OptionalJWTMiddleware {
    use(req, res, next) {
        var _a;
        let accessToken;
        accessToken = (_a = req.get("authorization")) === null || _a === void 0 ? void 0 : _a.split("Bearer ");
        if (accessToken.length !== 2)
            next();
        else {
            return jsonwebtoken_1.default.verify(accessToken[1], process.env.JWT_SECRET, async (err, user) => {
                if (err) {
                    if (err.name === 'TokenExpiredError') {
                        throw new common_1.HttpException('TokenExpired', 401);
                    }
                    throw new common_1.HttpException('Invalid token', 401);
                }
                req.user = user;
                next();
            });
        }
    }
};
OptionalJWTMiddleware = __decorate([
    (0, common_1.Injectable)()
], OptionalJWTMiddleware);
exports.OptionalJWTMiddleware = OptionalJWTMiddleware;
//# sourceMappingURL=auth.middleware.js.map