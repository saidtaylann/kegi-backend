"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.optionalJwtMiddlewareRoutes = exports.jwtMiddlewareRoutes = void 0;
const common_1 = require("@nestjs/common");
exports.jwtMiddlewareRoutes = [
    { path: '/users', method: common_1.RequestMethod.GET },
    { path: '/users', method: common_1.RequestMethod.PUT },
    { path: '/users/avatar', method: common_1.RequestMethod.POST },
    { path: '/users/avatar', method: common_1.RequestMethod.DELETE },
    { path: '/users/filter', method: common_1.RequestMethod.POST },
    { path: '/users/:id', method: common_1.RequestMethod.GET },
    { path: '/users/:id', method: common_1.RequestMethod.DELETE },
    { path: '/auth/logout', method: common_1.RequestMethod.POST },
    { path: '/auth/logout-all', method: common_1.RequestMethod.POST },
    { path: '/auth/verify-email/:token', method: common_1.RequestMethod.POST },
    { path: '/auth/refresh-verification', method: common_1.RequestMethod.POST },
    { path: '/rooms', method: common_1.RequestMethod.GET },
    { path: '/rooms/is-member/:code', method: common_1.RequestMethod.GET },
    { path: '/rooms', method: common_1.RequestMethod.POST },
    { path: '/rooms/:code/image', method: common_1.RequestMethod.POST },
];
exports.optionalJwtMiddlewareRoutes = [
    { path: '/rooms/:code', method: common_1.RequestMethod.GET },
];
//# sourceMappingURL=routeList.js.map