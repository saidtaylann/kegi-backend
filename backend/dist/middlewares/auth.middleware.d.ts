import { NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
export declare class JWTMiddleware implements NestMiddleware {
    use(req: Request, res: Response, next: NextFunction): void;
}
export declare class OptionalJWTMiddleware implements NestMiddleware {
    use(req: Request, res: Response, next: NextFunction): void;
}
