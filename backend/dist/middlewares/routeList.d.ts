import { RequestMethod } from "@nestjs/common";
export declare const jwtMiddlewareRoutes: {
    path: string;
    method: RequestMethod;
}[];
export declare const optionalJwtMiddlewareRoutes: {
    path: string;
    method: RequestMethod;
}[];
